<?php
/*
Template Name: Blank Template (No header nor footer)
*/
get_header(); //the_post();
$theid = (isset($larch_uc_id)) ? $larch_uc_id : get_the_ID();
$post = get_post($theid);
if (class_exists('Ultimate_VC_Addons')) {
	if(stripos($post->post_content, 'font_call:')){
		preg_match_all('/font_call:(.*?)"/',$post->post_content, $display);
		enquque_ultimate_google_fonts_optimzed($display[1]);
	}
	larch_google_fonts_scripts();
}
?>
<div class="page-template page-template-template-blank page-template-template-blank-php <?php echo esc_attr("the-id-is-$theid"); ?>">
	<div class="fullwindow_content container">
		<div class="tb-row">
			<div class="tb-cell">
				<?php 
					if (is_preview()){
						wp_reset_postdata();
						the_content();
					} else {
						if (function_exists('wpb_js_remove_wpautop') == true)
							echo wpb_js_remove_wpautop($post->post_content);
						else echo wp_kses_post($post->post_content); 
						/* custom element css */
						$shortcodes_custom_css = get_post_meta( $theid, '_wpb_shortcodes_custom_css', true );
						if ( ! empty( $shortcodes_custom_css ) ) {
							larch_set_custom_inline_css($shortcodes_custom_css);
						}
					}
				?>
			</div>
		</div>
	</div>
</div>
</div><!-- fechar div main -->
<?php wp_footer(); ?>
<div id="templatepath" class="larch_helper_div"><?php  echo get_template_directory_uri()."/"; ?></div>
<div id="homeURL" class="larch_helper_div"><?php echo esc_url(home_url('/')); ?>/</div>
<div id="styleColor" class="larch_helper_div"><?php $larch_styleColor = "#".get_option("larch"."_style_color"); echo esc_html($larch_styleColor);?></div>	
<div id="headerStyleType" class="larch_helper_div"><?php $larch_headerStyleType = get_option("larch"."_header_style_type"); echo esc_html($larch_headerStyleType); ?></div>
<div class="larch_helper_div" id="reading_option"><?php 
	$larch_reading_option = get_option('larch_blog_reading_type');
	if ($larch_reading_option == "scrollauto"){
		if (wp_is_mobile())
			$larch_reading_option = "scroll";
	}
	echo esc_html($larch_reading_option); 
?></div>
<?php
	$larch_color_code = get_option("larch_style_color");
?>
<div class="larch_helper_div" id="larch_no_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'No more posts to load.', get_option('larch_no_more_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_no_more_posts_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_load_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'Load More Posts', get_option('larch_load_more_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_load_more_posts_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_loading_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'Loading posts.', get_option('larch_loading_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_loading_posts_text'));
	}
?></div><div class="larch_helper_div" id="larch_links_color_hover"><?php echo str_replace("__USE_THEME_MAIN_COLOR__", $larch_color_code, get_option("larch_links_color_hover")); ?></div>
<div class="larch_helper_div" id="larch_enable_images_magnifier"><?php echo get_option('larch_enable_images_magnifier'); ?></div>
<div class="larch_helper_div" id="larch_thumbnails_hover_option"><?php echo get_option('larch_thumbnails_hover_option'); ?></div>
<div class="larch_helper_div" id="larch_menu_color">#<?php echo get_option("larch"."_menu_color"); ?></div>
<div class="larch_helper_div" id="larch_fixed_menu"><?php echo get_option("larch"."_fixed_menu"); ?></div>
<div class="larch_helper_div" id="larch_thumbnails_effect"><?php if (get_option("larch"."_animate_thumbnails") == "on") echo get_option("larch_thumbnails_effect"); else echo "none"; ?></div>
<div class="larch_helper_div" id="permalink_structure"><?php echo get_option('permalink_structure'); ?></div>
<div class="larch_helper_div" id="headerstyle3_menucolor">#<?php echo get_option("larch"."_menu_color"); ?></div>
<div class="larch_helper_div" id="disable_responsive_layout"><?php echo get_option('larch_disable_responsive'); ?></div>
<div class="larch_helper_div" id="filters-dropdown-sort"><?php esc_html_e('Sort Gallery','larch'); ?></div>
<div class="larch_helper_div" id="searcheverything"><?php echo get_option("larch"."_enable_search_everything"); ?></div>
<div class="larch_helper_div" id="larch_header_shrink"><?php if (get_option('larch_fixed_menu') == 'on'){if (get_option('larch_header_after_scroll') == 'on'){if (get_option('larch_header_shrink_effect') == 'on'){echo "yes";} else echo "no";}} ?></div>
<div class="larch_helper_div" id="larch_header_after_scroll"><?php if (get_option('larch_fixed_menu') == 'on'){if (get_option('larch_header_after_scroll') == 'on'){echo "yes";} else echo "no";} ?></div>
<div class="larch_helper_div" id="larch_grayscale_effect"><?php echo get_option("larch"."_enable_grayscale"); ?></div>
<div class="larch_helper_div" id="larch_enable_ajax_search"><?php echo get_option("larch"."_enable_ajax_search"); ?></div>
<div class="larch_helper_div" id="larch_menu_add_border"><?php echo get_option("larch"."_menu_add_border"); ?></div>
<div class="larch_helper_div" id="larch_newsletter_input_text"><?php
	if (function_exists('icl_t')){
		echo esc_html(icl_t( 'larch', 'Enter your email here', get_option('larch_newsletter_input_text')));
	} else {
		echo esc_html(get_option('larch_newsletter_input_text'));
	}
?></div>
<?php
	if (function_exists('icl_t')){
		?>
		<div class="larch_helper_div" id="larch_wpml_current_lang"><?php echo ICL_LANGUAGE_CODE; ?></div>
		<?php
	}
?>
</div>
