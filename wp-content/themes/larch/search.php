<?php
/**
 * @package WordPress
 * @subpackage Larch
**/

get_header(); larch_print_menu(); $larch_color_code = get_option("larch_style_color");

	/* pagetitle options related. */
	$type = get_option("larch_header_type");
	$thecolor = larch_hex2rgb(str_replace("__USE_THEME_MAIN_COLOR__", $larch_color_code, get_option("larch_header_color"))); 
	$opacity = intval(str_replace("%","",get_option("larch_header_opacity")))/100;
	$color = "rgba(".$thecolor[0].",".$thecolor[1].",".$thecolor[2].",".$opacity.")";
	$image = get_option("larch_header_image"); 
	$background_position = get_option("larch_pagetitle_background_position");
	$pattern = is_string(get_option("larch_header_pattern")) ? LARCH_PATTERNS_URL.get_option("larch_header_pattern") : ""; 
	$custompattern = get_option("larch_header_custom_pattern"); 
	$margintop = get_option("larch_header_text_margin_top");	
	$banner = get_option("larch_banner_slider");
	$showtitle = get_option("larch_hide_pagetitle") == "on" ? true : false;
	$showsectitle = get_option("larch_hide_sec_pagetitle") == "on" ? true : false;
	$tcolor = get_option("larch".'_header_text_color');
	$tsize = intval(str_replace(" ", "", get_option("larch".'_header_text_size')),10)."px";
	$tfont = get_option("larch".'_header_text_font');
	$stcolor = get_option("larch".'_secondary_title_text_color');
	$stsize = intval(str_replace(" ", "", get_option("larch".'_secondary_title_text_size')),10)."px";
	$stfont = get_option("larch".'_secondary_title_font');
	$stmargin = intval(str_replace(" ", "", get_option("larch".'_header_sec_text_margin_top')),10)."px";
	$originalalign = get_option("larch_header_text_alignment");
	$pt_parallax = get_option("larch_pagetitle_image_parallax") == "on" ? true : false;
	$pt_overlay = get_option("larch_pagetitle_image_overlay") == "on" ? true : false;
	$pt_overlay_type = get_option("larch_pagetitle_overlay_type");
	$pt_overlay_the_color = larch_hex2rgb(str_replace("__USE_THEME_MAIN_COLOR__", $larch_color_code, get_option("larch_pagetitle_overlay_color")));
	$pt_overlay_pattern = (is_string(get_option("larch_pagetitle_overlay_pattern"))) ? LARCH_PATTERNS_URL.get_option("larch_pagetitle_overlay_pattern") : "";
	$pt_overlay_opacity = intval(str_replace("%","",get_option("larch_pagetitle_overlay_opacity")))/100;
	$pt_overlay_color = "rgba(".$pt_overlay_the_color[0].",".$pt_overlay_the_color[1].",".$pt_overlay_the_color[2].",".$pt_overlay_opacity.")";
	$breadcrumbs = get_option("larch_breadcrumbs");
	$breadcrumbs_margintop = get_option('larch_breadcrumbs_text_margin_top');
	$pagetitlepadding = get_option('larch_page_title_padding');
	$height = "auto";
	$sidebar_scheme = get_option('larch_search_archive_sidebar', 'right');
	$sidebar = get_option('larch_search_sidebars_available');
	$textalign = $originalalign;
	if ($originalalign == "titlesleftcrumbsright") $textalign = "left";
	if ($originalalign == "titlesrightcrumbsleft") $textalign = "right";

	$larch_import_fonts[] = $tfont;
	$principalfont = explode("|",$tfont);
	$principalfont[0] = $principalfont[0]."', 'Arial', 'sans-serif";
	if (!isset($principalfont[1])) $principalfont[1] = "";
		
	$larch_import_fonts[] = $stfont;
	$secondaryfont = explode("|",$stfont);
	$secondaryfont[0] = $secondaryfont[0]."', 'Arial', 'sans-serif";
	if (!isset($secondaryfont[1])) $secondaryfont[1] = "";

	/* endof pagetitle options stuff. */
	
	/* search code related. counters and stuff. */
	$larch_reading_option = get_option('larch_blog_reading_type');
	$larch_more = 0;

	$orderby="";
	$category="";
	$nposts = "";

	$pag = 1;
	$pag = $wp_query->query_vars['paged'];
	if (!is_numeric($pag)) $pag = 1;
 
	
	$se = get_option("larch_enable_search_everything");
	
	if ($se == "on"){
		$args = array(
			'showposts' => get_option('posts_per_page'),
			'post_status' => 'publish',
			'paged' => $pag,
			's' => esc_html($_GET['s'])
		);
	    
	    $larch_the_query = new WP_Query( $args );
	    
	    $args2 = array(
			'showposts' => 0,
			'post_status' => 'publish',
			's' => esc_html($_GET['s'])
		);
		
		$counter = new WP_Query($args2);
		
	} else {
		$args = array(
			'showposts' => get_option('posts_per_page'),
			'post_status' => 'publish',
			'paged' => $pag,
			'post_type' => 'post',
			's' => esc_html($_GET['s'])
		);
			
	    $larch_the_query = new WP_Query( $args );
	    
	    $args2 = array(
			'showposts' => -1,
			'post_status' => 'publish',
			'post_type' => 'post',
			's' => esc_html($_GET['s'])
		);
		
		$counter = new WP_Query($args2);
	}
	/* endof search stuff. */
	
	if ($type != "without"){
		
		$ptitleaux = $bcaux = "";
		if ($originalalign == "titlesleftcrumbsright" || $originalalign == "titlesrightcrumbsleft"){
    		$ptitleaux .= "max-width: 50%;";
    		$bcaux .= "max-width: 50%;";
    		if ($originalalign == "titlesleftcrumbsright"){
				$ptitleaux .= "float:left;";
				$bcaux .= "float:right;";
			} else {
				$ptitleaux .= "float:right;";
				$bcaux .= "float:left;";
			}
		}
		$bcaux .= "margin-top:".intval($breadcrumbs_margintop,10)."px;";
		switch($originalalign){
			case "left": case "titlesrightcrumbsleft":
				$bcaux .= "text-align: left;";
			break;
			case "right": case "titlesleftcrumbsright":
				$bcaux .= "text-align:right;";
			break;
			case "center": 
				$bcaux .= "text-align:center;";
			break;
		}
		?>
		<div class="fullwidth-container <?php if ($type == "pattern") echo "bg-pattern"; ?> <?php if ($pt_parallax) echo "parallax"; ?><?php if (($type == "image" || $type == "pattern") && get_option('larch_enable_grayscale') == 'on') echo " larch_grayscale "; ?>" <?php if ($pt_parallax) echo 'data-stellar-ratio="0.5"'; ?> style="
	    	<?php 
		 		if ($height != "") echo "height: ". esc_html($height) . ";";
				if ($type == "none") echo "background: none;"; 
				if ($type == "color") echo "background: " . esc_html($color) . ";";
				if ($type == "image") echo "background: url(" . esc_url($image) . ") no-repeat; background-size: 100% auto;";  
	 			if ($type == "pattern") echo "background: url('" . esc_url($pattern) . "') 0 0 repeat;";
	    	?>" <?php if ($type == "image" && !$pt_parallax) echo ' data-background-alignment="'. esc_attr( $background_position ) .'" '; ?>>
	    	<?php
		    	if ($type == "image" && $pt_overlay){
			    	echo '<div class="pagetitle_overlay" style="'; 
			    	if ($pt_overlay_type == "color") echo 'background-color:'.esc_html($pt_overlay_color);
			    	else echo 'background:url('.esc_url($pt_overlay_pattern).') repeat;opacity:'.esc_html($pt_overlay_opacity).';';
			    	echo '"></div>';
		    	}
		    	if ($type === "banner"){
			    	?> 
			    	<div class="revBanner">
				    	<?php
					    	if (substr($banner, 0, 10) === "revSlider_"){
								if (!function_exists('putRevSlider')){
									echo esc_html__('Please install the missing plugin - Revolution Slider.', 'larch');
								} else {
									putRevSlider(substr($banner, 10));
								}
							} 
							if (substr($banner, 0, 13) === "masterSlider_"){
								if (!function_exists('masterslider')){
									echo esc_html__('Please install the missing plugin - MasterSlider.', 'larch');
								} else {
									echo do_shortcode( '[masterslider alias="'.substr($banner, 13).'"]' );
								}
							}
							if (substr($banner, 0, 12) === "layerSlider_"){
								if (!function_exists('layerslider')){
									echo esc_html__('Please install the missing plugin - LayerSlider.', 'larch');
								} else {
									echo do_shortcode( '[layerslider id="'.substr($banner, 12).'"]' );
								}
							}
				    	?>
				    </div> 
				    <?php
		    	} else {
		    	?>
				<div class="container <?php echo esc_attr($originalalign); ?>" style="padding:<?php echo esc_attr($pagetitlepadding); ?> 15px;">
					<div class="pageTitle" style="text-align:<?php echo esc_attr($textalign); ?>;<?php echo esc_attr($ptitleaux); ?>">
					<?php
						if ($showtitle){
							?>
							<h1 class="page_title" style="<?php echo esc_attr("color: #$tcolor; font-size: $tsize; font-family: '{$principalfont[0]}', sans-serif;font-weight: {$principalfont[1]}; ");?><?php if ($margintop != "") echo esc_attr("margin-top: ".intval($margintop,10)."px;"); ?>">
								<?php
									if (function_exists('icl_t')){
										echo wp_kses_post($counter->post_count . " " . sprintf(esc_html__("%s", "larch"), icl_t( 'larch', 'Search results for', get_option('larch_search_results_text'))) . " &#8216;" . $_GET['s'] ."&#8217;");
									} else {
										echo wp_kses_post($counter->post_count . " " . sprintf(esc_html__("%s", "larch"), get_option("larch_search_results_text")) . " &#8216;" . $_GET['s'] ."&#8217;");
									}
								?>
							</h1>
							<?php
						}
		    			if ($showsectitle){
			    			if (is_string(get_option("larch_search_secondary_title")) && get_option("larch_search_secondary_title") != ""){
						    	?>
							    <h2 class="secondaryTitle" style="<?php echo esc_attr("color: #$stcolor; font-size: $stsize; line-height: $stsize; font-family: '{$secondaryfont[0]}'; font-weight:{$secondaryfont[1]}; margin-top:{$stmargin};");?>">
							    	<?php echo wp_kses_post(get_option("larch_search_secondary_title")); ?>
							    </h2>
					    		<?php
					    	}
		    			}
		    		?>
		    		</div>
		    		
				</div>
		<?php }
		?>
		</div> <!-- end of fullwidth section -->
		<?php 
	}
	
	if (!$sidebar) $sidebar = "defaultblogsidebar";
	switch ($sidebar_scheme){
		case "none":
			?>
			<div class="master_container" style="width: 100%;float: left;background-color: white;">
				<div class="container">
					<section class="page_content">
						<?php larch_print_search_results(); ?>
					</section>
				</div>
			</div>
			<?php
		break;
		case "left":
			?>
			<div class="master_container" style="width: 100%;float: left;background-color: white;">
				<div class="container">
					<section class="page_content left sidebar col-xs-12 col-md-3">
						<?php 
						if ($sidebar === "defaultblogsidebar"){
							get_sidebar();
						} else {
							if ( function_exists('dynamic_sidebar')) { 
								ob_start();
							    do_shortcode(dynamic_sidebar($sidebar));
							    $html = ob_get_contents();
							    ob_end_clean();
							    $html = wp_kses_no_null( $html, array( 'slash_zero' => 'keep' ) );
								$html = wp_kses_normalize_entities($html);
								echo wp_kses_hook($html, 'post', array()); 
							}
						}
						wp_reset_postdata();
						?>
					</section>
					<section class="page_content right col-xs-12 col-md-9">
						<?php larch_print_search_results(); ?>
					</section>
				</div>
			</div>
			<?php
		break;
		case "right":
			?>
			<div class="master_container" style="width: 100%;float: left;background-color: white;">
				<div class="container">
					<section class="page_content left col-xs-12 col-md-9">
						<?php larch_print_search_results(); ?>
					</section>
					<section class="page_content right sidebar col-xs-12 col-md-3">
						<?php 
						wp_reset_postdata();
						if ($sidebar === "defaultblogsidebar"){
							get_sidebar();
						} else {
							if ( function_exists('dynamic_sidebar')) { 
								ob_start();
							    do_shortcode(dynamic_sidebar($sidebar));
							    $html = ob_get_contents();
							    ob_end_clean();
							    $html = wp_kses_no_null( $html, array( 'slash_zero' => 'keep' ) );
								$html = wp_kses_normalize_entities($html);
								echo wp_kses_hook($html, 'post', array());
							}
						}
						?>
					</section>
				</div>
			</div>
			<?php
		break;
		default:
			?>
			<div class="master_container" style="width: 100%;float: left;background-color: white;">
				<div class="container">
					<section class="page_content">
						<?php larch_print_search_results(); ?>
					</section>
				</div>
			</div>
			<?php
		break;
	}
	
	function larch_print_search_results(){
		global $larch_the_query;
		if ($larch_the_query->have_posts()){
			
			$larch_animate_results = get_option( 'larch_animate_results_on_scroll', 'on' ) == 'on' ? true : false;
			$larch_layout_is_masonry = get_option( 'larch_search_results_layout', 'normal' ) == 'masonry' || get_option( 'larch_search_results_layout', 'normal' ) == 'masonry_grid' ? true : false;
			$larch_show_featured_image = get_option( 'larch_show_results_featured_image', 'on' ) == 'on' ? true : false;
			$larch_inline_script = "";
		?> 
		
		<div class="post-listing <?php  if ($larch_layout_is_masonry) echo " journal isotope not-ready "; if ($larch_animate_results) echo " larch-animate-results "; ?>" <?php if ($larch_layout_is_masonry) echo ' data-columns="4" data-gutter-space="60" '; ?>>
			<?php			    
			    while ( $larch_the_query->have_posts() ) : 
						
			    	$larch_the_query->the_post();
		    		global $larch_more;
			    		$larch_more = 0;
			    	$post_classes = array();
			    	if ($larch_animate_results) $post_classes[] = "not-in-view";
					if ($larch_layout_is_masonry){
						$post_classes[] = "journal-post isotope-item";
					}
					?>
			    	
			    	<article id="post-<?php esc_attr(the_ID()); ?>" <?php post_class($post_classes); ?>>
				    	
				    	<?php
					    	if ($larch_layout_is_masonry){
						    	?>
						<div class="result_type_tag"><?php echo get_post_type( get_the_ID() ); ?></div>
						    	<?php
					    	}
				    	?>
				    	
				    	<?php
					    	if ($larch_show_featured_image && wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())) != false){
						    	?>
						    <div class="featured-image-box">

								<div class="featured-image">
									
									<a href="<?php echo esc_url(get_the_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
									<img alt="<?php echo esc_attr(get_the_title()); ?>" src="<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()))); ?>" title="<?php echo esc_attr(get_the_title()); ?>"/>
									<span class="post_overlay">
											<i class="fa fa-plus" aria-hidden="true"></i>
										</span>
									</a>
								</div>
								
							</div>
						    	<?php
					    	}
				    	?>
				    	
				    	<div class="the_title"><h2><a href="<?php esc_url(the_permalink()); ?>"><?php wp_kses_post(the_title()); ?></a></h2></div>
					<?php
						if (get_option("larch_display_metas") == "on"){
							
							if (!$larch_layout_is_masonry){
								?>
								<div class="metas-container">
								<?php
					    			$metas = explode(",", get_option("larch_metas_to_display"));
					    			if (!empty($metas)){
						    			$firstMeta = true;
						    			foreach ($metas as $meta){
							    			switch ($meta){
								    			case "date": 
								    				if (!$firstMeta){
										    			echo '<p class="metas-sep">|</p>';
									    			} else {
										    			$firstMeta = false;
									    			}
								    				?>
								    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
								    				<?php
								    			break;
								    			case "author":
								    				if (!$firstMeta){
										    			echo '<p class="metas-sep">|</p>';
									    			} else {
										    			$firstMeta = false;
									    			}
								    				?>
								    				<p data-rel="metas-author"><?php
									    				if (function_exists('icl_t')){
										    				printf(esc_html__("%s","larch"), icl_t( 'larch', 'by', get_option('larch_by_text')));
									    				} else {
										    				printf(esc_html__("%s","larch"), get_option("larch_by_text"));
									    				}
								    				?>: <a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"> <?php  esc_html(the_author_meta('nickname')); ?></a></p>
								    				<?php
								    			break;
								    			case "tags":
								    				$posttags = get_the_tags();
													if ($posttags) {
														if (!$firstMeta){
											    			echo '<p class="metas-sep">|</p>';
										    			} else {
											    			$firstMeta = false;
										    			}
														$first = true;
														echo '<p data-rel="metas-tags">';
														if (function_exists('icl_t')){
										    				printf(esc_html__("%s","larch"), icl_t( 'larch', 'by', get_option('larch_tags_text')));
									    				} else {
										    				printf(esc_html__("%s","larch"), get_option("larch_tags_text"));
									    				}
														echo ': ';
														foreach($posttags as $tag) {
															if ($tag->name != "uncategorized"){
																if ($first){
																	echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																	$first = false;
																} else {
																	echo "<span>, </span><a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																}
															}
													  	}
													  	echo '</p>';
													}
								    			break;
								    			case "categories":
								    				$postcats = get_the_category();
													if ($postcats) {
														if (!$firstMeta){
											    			echo '<p class="metas-sep">|</p>';
										    			} else {
											    			$firstMeta = false;
										    			}
														$first = true;
														echo '<p data-rel="metas-categories">';
														if (function_exists('icl_t')){
										    				printf(esc_html__("%s","larch"), icl_t( 'larch', 'by', get_option('larch_categories_text')));
									    				} else {
										    				printf(esc_html__("%s","larch"), get_option("larch_categories_text"));
									    				}
														echo ': ';
														foreach($postcats as $cat) {
															if ($cat->name != "uncategorized"){
																if ($first){
																	echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																	$first = false;
																} else {
																	echo "<span>, </span><a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																}	
															}
													  	}
													  	echo '</p>';
													}
								    			break;
								    			case "comments":
								    				if (!$firstMeta){
										    			echo '<p class="metas-sep">|</p>';
									    			} else {
										    			$firstMeta = false;
									    			}
								    				echo '<p data-rel="metas-comments">';
								    				printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'larch' ), number_format_i18n( get_comments_number() ) );
								    				echo '</p>';
								    			break;
								    			case "customtext":
								    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
										    			echo '<p class="metas-sep">|</p>';
										    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
									    			} else {
										    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
											    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
											    			$firstMeta = false;
										    			}
									    			}
								    			break;
							    			}
						    			}
					    			}
				    			?>
				    		</div>
								<?php
							}
								
								if ($larch_layout_is_masonry){
									?>
							<div class="results_excerpt">
								<?php
									$content = get_the_content('...',true);
									$pos=strpos($content, '<!--more-->');
									if ($pos){
										$text = explode('<!--more-->', $content);
										$text = $text[0];
										$text = preg_replace( '/\[[^\]]+\]/', '', $text );
										$text = apply_filters('the_content', $text);
										$text = str_replace(']]>', ']]&gt;', $text);
										echo wp_kses_post($text);
									} else {
										$text = preg_replace( '/\[[^\]]+\]/', '', $content );
										$text = apply_filters('the_content', $text);
										$text = str_replace(']]>', ']]&gt;', $text);
										$text = wp_trim_words( $text, 13 );
										echo apply_filters('wp_trim_excerpt', $text );
									}
								?>
							</div>
									<?php
								}
						}
				    ?> 
			    		
			    		<?php
			    		if (!is_sticky()){
				    		if (!$larch_layout_is_masonry){
					    		?>
					    	<div class="des-sc-dots-divider"></div>
					    		<?php
				    		}
			    		}
			    		?>
						
				    </article> <!-- end of post -->
				    	
			    <?php endwhile; ?>
			    		
	    	</div> <!-- end of post-listing -->
					
			<div class="navigation">
				<?php
					$larch_reading_option = get_option('larch_blog_reading_type');
					if ($larch_reading_option != "paged" && $larch_reading_option != "dropdown"){ 
						$larch_the_query = new WP_Query();
						if (function_exists('icl_t')){
							next_posts_link('<div class="next-posts">&laquo; ' . sprintf(esc_html__("%s", "larch"), icl_t( 'larch', 'Previous results', get_option('larch_previous_results'))).'</div>', $larch_the_query->max_num_pages);
							previous_posts_link('<div class="prev-posts">'.sprintf(esc_html__("%s", "larch"), icl_t( 'larch', 'Next results', get_option('larch_next_results'))) . ' &raquo;</div>', $larch_the_query->max_num_pages);
						} else {
							next_posts_link('<div class="next-posts">&laquo; ' . sprintf(esc_html__("%s", "larch"), get_option("larch_previous_results")).'</div>', $larch_the_query->max_num_pages);
							previous_posts_link('<div class="prev-posts">'.sprintf(esc_html__("%s", "larch"), get_option("larch_next_results")) . ' &raquo;</div>', $larch_the_query->max_num_pages);
						}
					} else { 
						larch_wp_pagenavi();
					}
				?>
			</div>

		<?php  
			
			if ($larch_layout_is_masonry){
				$larch_inline_script .= '
					"use strict";
					var forceGutter = 50; // change to false to return to the normal behavior.
					(function(e){"use strict";e.Isotope.prototype._getMasonryGutterColumns=function(){var e=this.options.masonry&&this.options.masonry.gutterWidth||0;var t=this.element.width();this.masonry.columnWidth=this.options.masonry&&this.options.masonry.columnWidth||this.$filteredAtoms.outerWidth(true)||t;this.masonry.columnWidth+=e;this.masonry.cols=Math.floor((t+e)/this.masonry.columnWidth);this.masonry.cols=Math.max(this.masonry.cols,1)};e.Isotope.prototype._masonryReset=function(){this.masonry={};this._getMasonryGutterColumns();var e=this.masonry.cols;this.masonry.colYs=[];while(e--){this.masonry.colYs.push(0)}};e.Isotope.prototype._masonryResizeChanged=function(){var e=this.masonry.cols;this._getMasonryGutterColumns();return this.masonry.cols!==e};e(document).ready(function(){"use strict";var t=e(".journal");var n=0;var r=0;var i=function(){var e=parseInt(t.data("columns"));var i=t.data("gutterSpace");var s=t.width();var o=0;if(isNaN(i)){i=.02}else if(i>.05||i<0){i=.02}if(s<568){e=1}else if(s<768){e-=2}else if(s<991){e-=1;if(e<2){e=2}}if(e<1){e=1}r=forceGutter!=false ? forceGutter : Math.floor(s*i);var u=r*(e-1);var a=s-u;n=Math.floor(a/e);o=r;if(1==e){o=20}t.children(".journal-post").css({width:n+"px",marginBottom:o+"px"})};i();window.iso = t.isotope({itemSelector:".journal-post",resizable:false,masonry:{columnWidth:n,gutterWidth:r}});    t.imagesLoaded(function(){i(); t.isotope({itemSelector:".journal-post",resizable:true,masonry:{columnWidth:n,gutterWidth:r}})});e(window).smartresize(function(){i();t.isotope({masonry:{columnWidth:n,gutterWidth:r}})});var s=e(".wc-shortcodes-filtering .wc-shortcodes-term");s.on("click",function(i){i.preventDefault();s.removeClass("wc-shortcodes-term-active");e(this).addClass("wc-shortcodes-term-active");var o=e(this).attr("data-filter"); t.isotope({filter:o,masonry:{columnWidth:n,gutterWidth:r} });return false})})})(jQuery);
					
					jQuery(window).load(function(){ jQuery(window).resize(); jQuery(".post-listing.larch-animate-results").removeClass("not-ready"); });
				';
			}
			
			if ($larch_animate_results){
				$larch_inline_script .= '
					jQuery(document).ready(function(){
						"use strict";
						jQuery(".not-in-view").waypoint({
							handler: function(event, direction){
								var element = this.element ? this.element : this;
								jQuery(element).removeClass("not-in-view");
							},
							offset: "80%"
						});
					});
				';
			}
			
			wp_add_inline_script( 'larch-global', $larch_inline_script, 'after' );
			
		}  else { ?>
	
		<div class="post-listing">
			<div class="pageTitle">
				<h2 class="hsearchtitle"><?php
					if (function_exists('icl_t')){
						echo sprintf(esc_html__("%s", "larch"), icl_t( 'larch', 'No results found.', get_option('larch_no_results_text')));
					} else {
						echo sprintf(esc_html__("%s", "larch"), get_option("larch_no_results_text"));
					}
				?></h2>
				<p class="titleSep"></p>
			</div>
		</div>
		
		
	<?php }
	}
	?>
	
<?php get_footer(); ?>