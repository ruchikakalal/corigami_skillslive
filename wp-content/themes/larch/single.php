<?php
/**
 * @package WordPress
 * @subpackage Larch
 */

get_header(); larch_print_menu(); ?>
	
	<?php 
		if (have_posts()) {
			the_post(); 
			$larch_type = get_post_type();
			$larch_portfolio_permalink = get_option("larch_portfolio_permalink");
			
			switch ($larch_type){
				case "post":
					get_template_part('post-single', 'single');
				break;
				case $larch_portfolio_permalink:
					get_template_part('proj-single', 'single');
				break;
				default:
					the_content();
				break;
			}
		}
	?>
	
<?php get_footer(); ?>