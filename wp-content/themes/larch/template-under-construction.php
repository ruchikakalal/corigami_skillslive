<?php
/*
Template Name: Under Construction Template
*/
get_header(); 
$theid = (isset($larch_uc_id)) ? $larch_uc_id : get_the_ID();
$post = get_post($theid);
if (class_exists('Ultimate_VC_Addons')) {
	if(stripos($post->post_content, 'font_call:')){
		preg_match_all('/font_call:(.*?)"/',$post->post_content, $display);
		$larch_import_fonts = array_unique( $display[1] );
		enquque_ultimate_google_fonts_optimzed($larch_import_fonts);
		
		$standardfonts = array('Arial','Arial Black','Helvetica','Helvetica Neue','Courier New','Georgia','Impact','Lucida Sans Unicode','Times New Roman', 'Trebuchet MS','Verdana','');
		$importfonts = "";
	
		foreach ($larch_import_fonts as $font){
			if (!in_array($font,$standardfonts)){
				$font = str_replace(" ","+",str_replace("|",":",$font));
				if ($importfonts=="") $importfonts .= $font;
				else {
					if (strpos($importfonts, $font) === false)
						$importfonts .= "|{$font}";
				}
			}
		}
	
		if ($importfonts!="") {
			$larch_import_fonts = $importfonts;
			larch_set_import_fonts($larch_import_fonts);
			larch_google_fonts_scripts();
		}
	}
}
?>

	<div class="fullwindow_rev">
		<?php
		$daslider = get_post_meta($theid, 'underconstruction_rev_value', true);
		if ($daslider){
			if (substr($daslider, 0, 10) === "revSlider_"){
				if (!function_exists('putRevSlider')){
					echo esc_html__('Please install the missing plugin - Revolution Slider.', 'larch');
				} else {
					putRevSlider(substr($daslider, 10));
				}
			}
			if (substr($daslider, 0, 13) === "masterSlider_"){
				if (!function_exists('masterslider')){
					echo esc_html__('Please install the missing plugin - MasterSlider.', 'larch');
				} else {
					echo do_shortcode( '[masterslider alias="'.substr($daslider, 13).'"]' );
				}
			}
			if (substr($daslider, 0, 12) === "layerSlider_"){
				if (!function_exists('layerslider')){
					echo esc_html__('Please install the missing plugin - LayerSlider.', 'larch');
				} else {
					echo do_shortcode( '[layerslider id="'.substr($daslider, 12).'"]' );
				}
			}
		} else {
			echo "You need to create a Revolution, Master or Layer Slider instance and then choose it in this Page Options.";
		}
		?>
	</div>
	<div class="underconstructo fullwindow_content container">
		<div class="tb-row">
			<div class="tb-cell">
				<?php 
					echo do_shortcode($post->post_content);
					/* custom element css */
					$shortcodes_custom_css = get_post_meta( $theid, '_wpb_shortcodes_custom_css', true );
					if ( ! empty( $shortcodes_custom_css ) ) {
						larch_set_custom_inline_css($shortcodes_custom_css);
					}
				?>
			</div>
		</div>
	</div>
	<?php wp_footer(); ?>
</div> <!-- END OF MAIN -->
<div id="templatepath" class="larch_helper_div"><?php echo esc_url(get_template_directory_uri())."/"; ?></div>
<div id="homeURL" class="larch_helper_div"><?php echo esc_url(home_url('/')); ?>/</div>
<div id="styleColor" class="larch_helper_div"><?php $larch_styleColor = "#".get_option("larch_style_color"); echo esc_html($larch_styleColor);?></div>		
<div id="headerStyleType" class="larch_helper_div"><?php $larch_headerStyleType = get_option("larch_header_style_type"); echo esc_html($larch_headerStyleType); ?></div>
<div class="larch_helper_div" id="reading_option"><?php 
	$larch_reading_option = get_option('larch_blog_reading_type');
	if ($larch_reading_option == "scrollauto"){
		if (wp_is_mobile())
			$larch_reading_option = "scroll";
	}
	echo esc_html($larch_reading_option); 
?></div>
<?php
	$larch_color_code = get_option("larch_style_color");
?>
<div class="larch_helper_div" id="larch_no_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'No more posts to load.', get_option('larch_no_more_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_no_more_posts_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_load_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'Load More Posts', get_option('larch_load_more_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_load_more_posts_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_loading_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'Loading posts.', get_option('larch_loading_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_loading_posts_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_links_color_hover"><?php echo esc_html(str_replace("__USE_THEME_MAIN_COLOR__", $larch_color_code, get_option("larch_links_color_hover"))); ?></div>
<div class="larch_helper_div" id="larch_enable_images_magnifier"><?php echo esc_html(get_option('larch_enable_images_magnifier')); ?></div>
<div class="larch_helper_div" id="larch_thumbnails_hover_option"><?php echo esc_html(get_option('larch_thumbnails_hover_option')); ?></div>
<div class="larch_helper_div" id="larch_menu_color">#<?php echo esc_html(get_option("larch_menu_color")); ?></div>
<div class="larch_helper_div" id="larch_fixed_menu"><?php echo esc_html(get_option("larch_fixed_menu")); ?></div>
<div class="larch_helper_div" id="larch_thumbnails_effect"><?php if (get_option("larch_animate_thumbnails") == "on") echo esc_html(get_option("larch_thumbnails_effect")); else echo "none"; ?></div>
<div class="larch_helper_div loadinger">
	<img alt="<?php esc_attr_e('loading','larch'); ?>" src="<?php echo esc_url(get_template_directory_uri()). '/images/ajx_loading.gif' ?>">
</div>
<div class="larch_helper_div" id="permalink_structure"><?php echo esc_html(get_option('permalink_structure')); ?></div>
<div class="larch_helper_div" id="headerstyle3_menucolor">#<?php echo esc_html(get_option("larch_menu_color")); ?></div>
<div class="larch_helper_div" id="disable_responsive_layout"><?php echo esc_html(get_option('larch_disable_responsive')); ?></div>
<div class="larch_helper_div" id="filters-dropdown-sort"><?php esc_html_e('Sort Gallery','larch'); ?></div>
<div class="larch_helper_div" id="searcheverything"><?php echo esc_html(get_option("larch_enable_search_everything")); ?></div>
<div class="larch_helper_div" id="larch_header_shrink"><?php if (get_option('larch_fixed_menu') == 'on'){if (get_option('larch_header_after_scroll') == 'on'){if (get_option('larch_header_shrink_effect') == 'on'){echo "yes";} else echo "no";}} ?></div>
<div class="larch_helper_div" id="larch_header_after_scroll"><?php if (get_option('larch_fixed_menu') == 'on'){if (get_option('larch_header_after_scroll') == 'on'){echo "yes";} else echo "no";} ?></div>
<div class="larch_helper_div" id="larch_grayscale_effect"><?php echo esc_html(get_option("larch_enable_grayscale")); ?></div>
<div class="larch_helper_div" id="larch_enable_ajax_search"><?php echo esc_html(get_option("larch_enable_ajax_search")); ?></div>
<div class="larch_helper_div" id="larch_menu_add_border"><?php echo esc_html(get_option("larch_menu_add_border")); ?></div>
<div class="larch_helper_div" id="larch_newsletter_input_text"><?php
	if (function_exists('icl_t')){
		echo esc_html(icl_t( 'larch', 'Enter your email here', get_option('larch_newsletter_input_text')));
	} else {
		echo esc_html(get_option('larch_newsletter_input_text'));
	}
?></div>
<?php
	if (function_exists('icl_t')){
		?>
		<div class="larch_helper_div" id="larch_wpml_current_lang"><?php echo ICL_LANGUAGE_CODE; ?></div>
		<?php
	}
?>