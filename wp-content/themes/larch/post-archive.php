<?php
/**
 * @package WordPress
 * @subpackage Larch
 */
get_header(); larch_print_menu();

	$larch_reading_option = get_option("larch_blog_reading_type");
	$larch_more = 0;
	$larch_color_code = get_option("larch_style_color");

	$type = get_option("larch_header_type");
	$thecolor = larch_hex2rgb(str_replace("__USE_THEME_MAIN_COLOR__", $larch_color_code, get_option("larch_header_color"))); 
	$opacity = intval(str_replace("%","",get_option("larch_header_opacity")),10)/100;
	$color = "rgba(".$thecolor[0].",".$thecolor[1].",".$thecolor[2].",".$opacity.")";
	$image = get_option("larch_header_image"); 
	$background_position = get_option("larch_pagetitle_background_position");
	$pattern = is_string(get_option("larch_header_pattern")) ? LARCH_PATTERNS_URL.get_option("larch_header_pattern") : ""; 
	$custompattern = get_option("larch_header_custom_pattern"); 
	$height = get_option("larch_header_height"); 
	$margintop = get_option("larch_header_text_margin_top");	
	$banner = get_option("larch_banner_slider");
	$showtitle = get_option("larch_hide_pagetitle") == "on" ? true : false;
	$showsectitle = get_option("larch_hide_sec_pagetitle") == "on" ? true : false;
	$tcolor = get_option("larch".'_header_text_color');
	$tsize = intval(str_replace(" ", "", get_option("larch".'_header_text_size')),10)."px";
	$tfont = get_option("larch".'_header_text_font');
	$stcolor = get_option("larch".'_secondary_title_text_color');
	$stsize = intval(str_replace(" ", "", get_option("larch".'_secondary_title_text_size')),10)."px";
	$stfont = get_option("larch".'_secondary_title_font');
	$stmargin = intval(str_replace(" ", "", get_option("larch".'_header_sec_text_margin_top')),10)."px";
	$originalalign = get_option("larch_header_text_alignment");
	$pt_parallax = get_option("larch_pagetitle_image_parallax") == "on" ? true : false;
	$pt_overlay = get_option("larch_pagetitle_image_overlay") == "on" ? true : false;
	$pt_overlay_type = get_option("larch_pagetitle_overlay_type");
	$pt_overlay_the_color = larch_hex2rgb(str_replace("__USE_THEME_MAIN_COLOR__", $larch_color_code, get_option("larch_pagetitle_overlay_color")));
	$pt_overlay_pattern = (is_string(get_option("larch_pagetitle_overlay_pattern"))) ? LARCH_PATTERNS_URL.get_option("larch_pagetitle_overlay_pattern") : "";
	$pt_overlay_opacity = intval(str_replace("%","",get_option("larch_pagetitle_overlay_opacity")))/100;
	$pt_overlay_color = "rgba(".$pt_overlay_the_color[0].",".$pt_overlay_the_color[1].",".$pt_overlay_the_color[2].",".$pt_overlay_opacity.")";
	$breadcrumbs = get_option("larch_breadcrumbs");
	$breadcrumbs_margintop = get_option('larch_breadcrumbs_text_margin_top');
	$pagetitlepadding = get_option('larch_page_title_padding');

	$textalign = $originalalign;
	if ($originalalign == "titlesleftcrumbsright") $textalign = "left";
	if ($originalalign == "titlesrightcrumbsleft") $textalign = "right";

	$larch_import_fonts[] = $tfont;
	$principalfont = explode("|",$tfont);
	$principalfont[0] = $principalfont[0]."', 'Arial', 'sans-serif";
	if (!isset($principalfont[1])) $principalfont[1] = "";
		
	$larch_import_fonts[] = $stfont;
	$secondaryfont = explode("|",$stfont);
	$secondaryfont[0] = $secondaryfont[0]."', 'Arial', 'sans-serif";
	if (!isset($secondaryfont[1])) $secondaryfont[1] = "";
	
	larch_set_import_fonts($larch_import_fonts);
	
	if ($type != "without"){
		
		$ptitleaux = $bcaux = "";
		if ($originalalign == "titlesleftcrumbsright" || $originalalign == "titlesrightcrumbsleft"){
    		$ptitleaux .= "max-width: 50%;";
    		$bcaux .= "max-width: 50%;";
    		if ($originalalign == "titlesleftcrumbsright"){
				$ptitleaux .= "float:left;";
				$bcaux .= "float:right;";
			} else {
				$ptitleaux .= "float:right;";
				$bcaux .= "float:left;";
			}
		}
		$bcaux .= "margin-top:".intval($breadcrumbs_margintop,10)."px;";
		switch($originalalign){
			case "left": case "titlesrightcrumbsleft":
				$bcaux .= "text-align: left;";
			break;
			case "right": case "titlesleftcrumbsright":
				$bcaux .= "text-align:right;";
			break;
			case "center": 
				$bcaux .= "text-align:center;";
			break;
		}
		?>
		<div class="fullwidth-container <?php if ($type == "pattern") echo "bg-pattern"; ?> <?php if ($pt_parallax) echo "parallax"; ?><?php if (($type == "image" || $type == "pattern") && get_option('larch_enable_grayscale') == 'on') echo " larch_grayscale "; ?>" <?php if ($pt_parallax) echo 'data-stellar-ratio="0.5"'; ?> style="
	    	<?php 
		 		if ($height != "") echo "height: ". esc_html($height) . ";";
				if ($type == "none") echo "background: none;"; 
				if ($type == "color") echo "background: " . esc_html($color) . ";";
				if ($type == "image") echo "background: url(" . esc_url($image) . ") no-repeat; background-size: 100% auto;";  
	 			if ($type == "pattern") echo "background: url('" . esc_url($pattern) . "') 0 0 repeat;";
	    	?>" <?php if ($type == "image" && !$pt_parallax) echo ' data-background-alignment="'. esc_attr( $background_position ) .'" '; ?>>
	    	<?php
		    	if ($type == "image" && $pt_overlay){
			    	echo '<div class="pagetitle_overlay" style="'; 
			    	if ($pt_overlay_type == "color") echo 'background-color:'.esc_html($pt_overlay_color);
			    	else echo 'background:url('.esc_url($pt_overlay_pattern).') repeat;opacity:'.esc_html($pt_overlay_opacity).';';
			    	echo '"></div>';
		    	}
		    	if ($type === "banner"){
			    	?> 
			    	<div class="revBanner">
				    	<?php
					    	if (substr($banner, 0, 10) === "revSlider_"){
								if (!function_exists('putRevSlider')){
									echo esc_html__('Please install the missing plugin - Revolution Slider.', 'larch');
								} else {
									putRevSlider(substr($banner, 10));
								}
							} 
							if (substr($banner, 0, 13) === "masterSlider_"){
								if (!function_exists('masterslider')){
									echo esc_html__('Please install the missing plugin - MasterSlider.', 'larch');
								} else {
									echo do_shortcode( '[masterslider alias="'.substr($banner, 13).'"]' );
								}
							}
							if (substr($banner, 0, 12) === "layerSlider_"){
								if (!function_exists('layerslider')){
									echo esc_html__('Please install the missing plugin - LayerSlider.', 'larch');
								} else {
									echo do_shortcode( '[layerslider id="'.substr($banner, 12).'"]' );
								}
							}
				    	?>
				    </div> 
				    <?php
		    	} else {
		    	?>
				<div class="container <?php echo esc_attr($originalalign); ?>" style="padding:<?php echo esc_attr($pagetitlepadding); ?> 15px;">
					<div class="pageTitle" style="text-align:<?php echo esc_attr($textalign); ?>;<?php echo esc_attr($ptitleaux); ?>">
					<?php
						if ($showtitle){
							?>
							<h1 class="page_title" style="<?php echo esc_attr("color: #$tcolor; font-size: $tsize; font-family: '{$principalfont[0]}', sans-serif;font-weight: {$principalfont[1]}; ");?><?php if ($margintop != "") echo esc_attr("margin-top: ".intval($margintop,10)."px;"); ?>">
								<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
								<?php 
									/* If this is a category archive */ if (is_category()) { 
									esc_html_e('Archive for the','larch'); echo "<br/>&#8216;"; single_cat_title(); echo "&#8217; "; esc_html_e('Category','larch'); 
									
									/* If this is a tag archive */ } elseif( is_tag() ) { 
									esc_html_e('Posts Tagged','larch'); echo "<br/>&#8216;"; single_tag_title(); echo "&#8217;";
									
									/* If this is a daily archive */ } elseif (is_day()) {
									esc_html_e('Archive for ','larch'); echo get_the_date('F jS, Y'); 
					
									/* If this is a monthly archive */ } elseif (is_month()) { 
									esc_html_e('Archive for ','larch'); echo get_the_date('F, Y'); 
					
									/* If this is a yearly archive */ } elseif (is_year()) {
									esc_html_e('Archive for ','larch'); echo get_the_date('Y'); 
					
									/* If this is an author archive */ } elseif (is_author()) {
									esc_html_e('Author Archive','larch');
									
									/* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged']))
									esc_html_e('Blog Archives','larch');
								?>
							</h1>
							<?php
						}
		    			if ($showsectitle){
			    			if (is_string(get_option('larch_archive_secondary_title')) && get_option('larch_archive_secondary_title') != ""){
						    	?>
							    <h2 class="secondaryTitle" style="<?php echo esc_attr("color: #$stcolor; font-size: $stsize; line-height: $stsize; font-family: '{$secondaryfont[0]}'; font-weight: {$secondaryfont[1]}; margin-top:{$stmargin};");?>">
							    	<?php echo wp_kses_post(get_option("larch_archive_secondary_title")); ?>
							    </h2>
					    		<?php
					    	}
		    			}
		    		?>
		    		</div>
		    		<?php
		    		if ($breadcrumbs == "on"){
			    		?>
			    		<div class="larch_breadcrumbs" style="<?php echo esc_attr($bcaux); ?>">
							<?php larch_the_breadcrumb(); ?>
			    		</div>
			    		<?php
					}
					?>
				</div>
		<?php }
		?>
		</div> <!-- end of fullwidth section -->
		<?php	
	}
	
	$sidebar = get_option("larch_blog_archive_sidebars_available");
	$template = get_option("larch_blog_archive_style");
	$pagetemplate = "page-template-blog-template";
	if ($template == "masonry") $pagetemplate = "page-template-blog-masonry-template";
	if ($template == "masonry_grid") $pagetemplate = "page-template-blog-masonry-grid-template";
	switch (get_option("larch_blog_archive_sidebar")){
		case "none":
			?>
			<div class="blog-default wideblog <?php echo esc_attr( $$pagetemplate ); ?>">
				<div class="master_container container">
					
					<section class="page_content col-xs-12 col-md-12">
						<div class="blog-default-bg <?php echo esc_attr( get_option("larch_blog_archive_style") ); ?>">
							<?php larch_print_blog_archive(); ?>
						</div>
					</section>
				</div>
			</div>
			<?php
		break;
		case "left":
			?>
			<div class="blog-default <?php echo esc_attr( $$pagetemplate ); ?>">
				<div class="master_container container">
					<section class="page_content left sidebar col-xs-12 col-md-3">
						
						<div class="blog-sidebar-bg">
							<?php 
							if ($sidebar === "defaultblogsidebar" || !$sidebar){
								get_sidebar();
							} else {
								if ( function_exists('dynamic_sidebar')) { 
									ob_start();
								    do_shortcode(dynamic_sidebar($sidebar));
								    $html = ob_get_contents();
								    ob_end_clean();
								    $html = wp_kses_no_null( $html, array( 'slash_zero' => 'keep' ) );
									$html = wp_kses_normalize_entities($html);
									echo wp_kses_hook($html, 'post', array());
								}	
							}
							?>
						</div>
					</section>
					<section class="page_content right col-xs-12 col-md-9">
						<div class="blog-default-bg <?php echo esc_attr( get_option("larch_blog_archive_style") ); ?>">
							<?php larch_print_blog_archive(); ?>
						</div>
					</section>
				</div>
			</div>
			<?php
		break;
		case "right":
			?>
			
			<div class="blog-default <?php echo esc_attr( $$pagetemplate ); ?>">
				<div class="master_container container">
					<section class="page_content left col-xs-12 col-md-9">
						<div class="blog-default-bg <?php echo esc_attr( get_option("larch_blog_archive_style") ); ?>">
							<?php larch_print_blog_archive(); ?>
						</div>
					</section>
					<section class="page_content right sidebar col-xs-12 col-md-3">
						<div class="blog-sidebar-bg">
							<?php 
							if ($sidebar === "defaultblogsidebar" || !$sidebar ){
								get_sidebar();
							} else {
								if ( function_exists('dynamic_sidebar')) { 
									ob_start();
								    do_shortcode(dynamic_sidebar($sidebar));
								    $html = ob_get_contents();
								    ob_end_clean();
								    $html = wp_kses_no_null( $html, array( 'slash_zero' => 'keep' ) );
									$html = wp_kses_normalize_entities($html);
									echo wp_kses_hook($html, 'post', array());
								}
							}
							?>
						</div>
					</section>
				</div>
			</div>
			
			<?php
		break;
		default:
			?>
			<section class="page_content <?php echo esc_attr( $$pagetemplate ); ?>">
				<div class="container">
				<?php
					$thepost = get_post($i->object_id);
					echo apply_filters( 'the_content', $thepost->post_content );
				?>
				</div>
			</section>
			<?php
		break;
	}
	
		
	function larch_print_blog_archive(){
		
		global $larch_import_fonts;
		$larch_inline_script = "";
		$larch_animate_posts = get_option('larch_animate_posts_on_scroll', 'on') == 'on' ? true : false;
		if (have_posts()){
		
			if (get_option("larch_blog_archive_style") === "normal"){
				$larch_import_fonts[] = get_option('larch_blog_normal_title_font');
				$titlefont = explode("|",get_option('larch_blog_normal_title_font'));
				$titlefont[0] = $titlefont[0]."', 'Arial', 'sans-serif";
				if (!isset($titlefont[1])) $titlefont[1] = "";
				$titlecolor = intval(get_option('larch_blog_normal_title_color'),10);
				$titlesize = get_option('larch_blog_normal_title_size');
			?> 			
			<div class="post-listing<?php if ($larch_animate_posts) echo " larch-animate-posts "; ?>">
	    	
	    	<?php
		    
			    while (have_posts()){
				the_post();
			    $postid = get_the_ID();
			    $post_classes = array();
			    if ($larch_animate_posts) $post_classes[] = "not-in-view";
			    ?>
			    
			    <article id="post-<?php esc_attr(the_ID()); ?>" <?php post_class($post_classes); ?>>
				    
				    <?php
					    $posttype = get_post_meta(get_the_ID(), 'posttype_value', true);
					    $upper_fullwidth_media = get_post_meta( get_the_ID(), 'upper_fullwidth_media_value', true ) == 'yes' ? true : false;
					    $article_output = "";
					    $media = false;
					    switch($posttype){
				    		case "image":
				    		
				    			if (wp_get_attachment_url( get_post_thumbnail_id($postid))){
					    			$media = true;
					    			ob_start();
									$article_output .= '[vc_row][vc_column width="1/2"]';
								?>
									<div class="featured-image-box">
										<?php
											if (get_option('larch_show_author_avatar') == "on"){
												?>
										<div class="author-thumbnail">
											<?php echo "<a href='".esc_url(get_author_posts_url( get_the_author_meta('ID') ))."'>".get_avatar( get_the_author_meta('ID'), 100 )."</a>"; ?>
										</div>
												<?php
											}
										?>
										<div class="featured-image">
											
											<a href="<?php echo esc_url(get_the_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
											<img alt="<?php echo esc_attr(get_the_title()); ?>" src="<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id($postid))); ?>" title="<?php echo esc_attr(get_the_title()); ?>"/>
											<span class="post_overlay">
													<i class="fa fa-plus" aria-hidden="true"></i>
												</span>
											</a>
										</div>
										
									</div>
									<?php 
								}
				    			
				    		break;

				    		case "slider": 
				    			$media = true;
				    			ob_start();
								$article_output .= '[vc_row][vc_column width="1/2"]';
				    			$randClass = rand(0,1000);
								?>
									<div class="featured-image-box">
										<?php
											if (get_option('larch_show_author_avatar') == "on"){
												?>
										<div class="author-thumbnail">
											<?php echo "<a href='".esc_url(get_author_posts_url( get_the_author_meta('ID') ))."'>".get_avatar( get_the_author_meta('ID'), 100 )."</a>"; ?>
										</div>
												<?php
											}
										?>
										<div class="flexslider <?php echo esc_attr($posttype); ?>" id="<?php echo esc_attr($randClass); ?>">
											<ul class="slides">
												<?php
													$sliderData = get_post_meta($postid, "sliderImages_value", true);
													$slide = explode("|*|",$sliderData);
												    foreach ($slide as $s){
												    	if ($s != ""){
												    		$params = explode("|!|",$s);
												    		$attachment = get_post( $params[0] );
												    		$title = isset($attachment->post_excerpt) ? $attachment->post_excerpt : "";
												    		echo "<li>";
												    		echo "<img src='".esc_url($params[1])."' alt='' title='".esc_attr($title)."'>";
												    		echo "</li>";	
												    	}
												    }
												?>
											</ul>
										</div>
									</div>
								<?php
									$larch_inline_script = '
										jQuery(document).ready(function(){
											"use strict";
											jQuery("#'.$randClass.'.flexslider").flexslider({
												animation: "'. esc_js( get_option('larch_posts_flex_transition', 'fade') ) .'",
												slideshow: '. esc_js( (get_option('larch_posts_flex_autoplay','on') == 'on') ? "true" : "false" ) .',
												slideshowSpeed: '. esc_js( get_option('larch_posts_flex_slide_duration','5500') ) .',
												animationDuration: '. esc_js( get_option('larch_posts_flex_transition_duration','500') ) .',
												directionNav: '. esc_js( (get_option('larch_posts_flex_navigation','on') == 'on') ? "true" : "false" ) .',
												controlNav: '. esc_js( (get_option('larch_posts_flex_controls','on') == 'on') ? "true" : "false" ) .',
												smootheHeight:false,
												start: function(slider) {
												  slider.removeClass("loading").css("overflow","").find(".slides").css("max-height", "'.esc_js( intval(get_option('larch_posts_flex_height','400')) ).'px" );
												}
											});
										});
									';
									wp_add_inline_script('larch-global', $larch_inline_script, 'after');
				    		break;

				    		case "audio":
				    			$media = true;
						    	ob_start();
								$article_output .= '[vc_row][vc_column width="1/2"]';
				    			?>
								<div class="audioContainer">
									<?php
										if (get_post_meta($postid, 'audioSource_value', true) == 'embed') echo get_post_meta($postid, 'audioCode_value', true); 
										else {
											$audio = explode("|!|",get_post_meta($postid, 'audioMediaLibrary_value', true));
											if (isset($audio[1])) {
												$ext = explode(".",$audio[1]);
												if (isset($ext)) $ext = $ext[count($ext)-1];
												?>
												<audio controls="controls"><source type="audio/<?php echo esc_attr($ext); ?>" src="<?php echo esc_url($audio[1]); ?>"></audio>
												<?php
											}
										}
									?>
								</div>
								<?php
				    		break;
				    		
				    		case "video":
				    			$media = true;
						    	ob_start();
								$article_output .= '[vc_row][vc_column width="1/2"]';
				    			?>
				    			<div class="featured-image-box">
									<?php
										if (get_option('larch_show_author_avatar') == "on"){
											?>
									<div class="author-thumbnail">
										<?php echo "<a href='".esc_url(get_author_posts_url( get_the_author_meta('ID') ))."'>".get_avatar( get_the_author_meta('ID'), 100 )."</a>"; ?>
									</div>
											<?php
										}
									?>
					    			<div class="post-video <?php echo esc_attr(get_post_meta($postid, "videoSource_value", true)); ?>">
										<div class="video-thumb">
											<div class="video-wrapper vendor">
										<?php
											$videosType = get_post_meta($postid, "videoSource_value", true);
											if ($videosType != "embed"){
												$videos = get_post_meta($postid, "videoCode_value", true);
												$videos = preg_replace( '/\s+/', '', $videos );
												$vid = explode(",",$videos);
											}
											switch (get_post_meta($postid, "videoSource_value", true)){
												case "media":
													$video = explode("|!|",get_post_meta($postid, 'videoMediaLibrary_value', true));
													if (isset($video[1])) {
														$ext = explode(".",$video[1]);
														if (isset($ext)) $ext = $ext[count($ext)-1];
														?>
														<video controls="controls" class="video-controls"><source type="video/<?php echo esc_attr($ext); ?>" src="<?php echo esc_url($video[1]); ?>"></video>
														<?php
													}
												break;
												case "youtube":
													if (isset($vid[0])) echo "<iframe src='//www.youtube.com/embed/".esc_attr($vid[0])."' frameborder='0' allowfullscreen></iframe>";
													break;
												case "vimeo":
													if (isset($vid[0])) echo '<iframe src="https://player.vimeo.com/video/'.esc_attr($vid[0]).'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
													break;
											}						
										?>
											</div>
										</div>
									</div>
				    			</div>
								<?php
				    		break;
				    		
				    		case "gallery":
					    		$slider = get_post_meta($postid,'gallery_slider_value',true);
								if ($slider != '-1'){
									$media = true;
					    			ob_start();
									$article_output .= '[vc_row][vc_column width="1/2"]';
									if (substr($slider, 0, 10) === "revSlider_"){
										?>
										<div class="featured-image-box">
											<?php
												if (get_option('larch_show_author_avatar') == "on"){
													?>
											<div class="author-thumbnail">
												<?php echo "<a href='".esc_url(get_author_posts_url( get_the_author_meta('ID') ))."'>".get_avatar( get_the_author_meta('ID'), 100 )."</a>"; ?>
											</div>
													<?php
												}
											?>
											<div class="gallery_container">
												<?php
													if (!function_exists('putRevSlider')){
														echo esc_html__('Please install the missing plugin - Revolution Slider.', 'larch');
													} else {
														putRevSlider(substr($slider, 10));
													}
												?>
											</div>
										</div>
										<?php
									} else {
										?>
										<div class="featured-image-box">
											<?php
												if (get_option('larch_show_author_avatar') == "on"){
													?>
											<div class="author-thumbnail">
												<?php echo "<a href='".esc_url(get_author_posts_url( get_the_author_meta('ID') ))."'>".get_avatar( get_the_author_meta('ID'), 100 )."</a>"; ?>
											</div>
													<?php
												}
											?>
											<div class="gallery_container">
												<?php
													if (!function_exists('masterslider')){
														echo esc_html__('Please install the missing plugin - MasterSlider.', 'larch');
													} else {
														echo do_shortcode( '[masterslider alias="'.substr($slider, 13).'"]' );
													}
												?>
											</div>
										</div>
										<?php
									}
								}
				    		break;
				    		
				    		case "quote":
				    			?>
				    			<a href="<?php esc_url(the_permalink()); ?>">
					    			<div class="post-quote">
			                        	<blockquote><i class="fa fa-quote-left"></i> <?php echo wp_kses_post(get_post_meta($postid, 'quote_text_value', true)); ?> <i class="fa fa-quote-right"></i></blockquote>
			                        	<span class="author-quote">-- <?php echo wp_kses_post(get_post_meta($postid, 'quote_author_value', true)); ?> --</span>
			                        </div>
		                        </a>
				    			<?php
				    		break;

							case "link":
								?>
								<h2 class="post-title post-link">
									<?php
										$linkurl = get_post_meta($postid, 'link_url_value', true) != '' ? get_post_meta($postid, 'link_url_value', true) : get_permalink();
										$linktext = get_post_meta($postid, 'link_text_value', true) != '' ? get_post_meta($postid, 'link_text_value', true) : $linkurl;
									?>
									<a href="<?php echo esc_url($linkurl); ?>" target="_blank"><?php echo esc_html($linktext); ?></a>
		                        </h2>
								<?php
							break;
				    		
				    		case "text": default:
								if ($posttype != "text"){
					    			if (wp_get_attachment_url( get_post_thumbnail_id($postid))){
									?>
										<div class="featured-image-box">
											<?php
												if (get_option('larch_show_author_avatar') == "on"){
													?>
											<div class="author-thumbnail">
												<?php echo "<a href='".esc_url(get_author_posts_url( get_the_author_meta('ID') ))."'>".get_avatar( get_the_author_meta('ID'), 100 )."</a>"; ?>
											</div>
													<?php
												}
											?>
											<div class="featured-image">
												
												<a href="<?php echo esc_url(get_the_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
												<img alt="<?php echo esc_attr(get_the_title()); ?>" src="<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id($postid))); ?>" title="<?php echo esc_attr(get_the_title()); ?>"/>
												<span class="post_overlay">
														<i class="fa fa-plus" aria-hidden="true"></i>
													</span>
												</a>
											</div>
											
										</div>
										<?php 
									}
				    			}
				    		break;
			    		}
			    		
			    		if ($media && !$upper_fullwidth_media){
				    		$article_output .= ob_get_clean() . '[/vc_column][vc_column width="1/2"]';
				    		ob_start();
			    		}
			    		if ($media && $upper_fullwidth_media) echo '<div class="fullwidth_media_content"><div class="contentainer">';
			    		
			    		if (get_option("larch_display_metas") == "on"){
							?>
							<div class="metas-container">
								<?php
					    			$metas = explode(",", get_option("larch_metas_to_display"));
					    			if (!empty($metas)){
						    			$firstMeta = true;
						    			foreach ($metas as $meta){
							    			switch ($meta){
								    			case "date": 
								    				if (!$firstMeta){
										    			echo '<p class="metas-sep">|</p>';
									    			} else {
										    			$firstMeta = false;
									    			}
								    				?>
								    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
								    				<?php
								    			break;
								    			case "author":
								    				if (!$firstMeta){
										    			echo '<p class="metas-sep">|</p>';
									    			} else {
										    			$firstMeta = false;
									    			}
								    				?>
								    				<p data-rel="metas-author"><?php
									    				if (function_exists('icl_t')){
										    				printf(esc_html__("%s","larch"), icl_t( 'larch', 'by', get_option('larch_by_text')));
									    				} else {
										    				printf(esc_html__("%s","larch"), get_option("larch_by_text"));
									    				}
								    				?>: <a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"> <?php  esc_html(the_author_meta('nickname')); ?></a></p>
								    				<?php
								    			break;
								    			case "tags":
								    				$posttags = get_the_tags();
													if ($posttags) {
														if (!$firstMeta){
											    			echo '<p class="metas-sep">|</p>';
										    			} else {
											    			$firstMeta = false;
										    			}
														$first = true;
														echo '<p data-rel="metas-tags">';
														if (function_exists('icl_t')){
										    				printf(esc_html__("%s","larch"), icl_t( 'larch', 'by', get_option('larch_tags_text')));
									    				} else {
										    				printf(esc_html__("%s","larch"), get_option("larch_tags_text"));
									    				}
														echo ': ';
														foreach($posttags as $tag) {
															if ($tag->name != "uncategorized"){
																if ($first){
																	echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																	$first = false;
																} else {
																	echo "<span>, </span><a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																}
															}
													  	}
													  	echo '</p>';
													}
								    			break;
								    			case "categories":
								    				$postcats = get_the_category();
													if ($postcats) {
														if (!$firstMeta){
											    			echo '<p class="metas-sep">|</p>';
										    			} else {
											    			$firstMeta = false;
										    			}
														$first = true;
														echo '<p data-rel="metas-categories">';
														if (function_exists('icl_t')){
										    				printf(esc_html__("%s","larch"), icl_t( 'larch', 'by', get_option('larch_categories_text')));
									    				} else {
										    				printf(esc_html__("%s","larch"), get_option("larch_categories_text"));
									    				}
														echo ': ';
														foreach($postcats as $cat) {
															if ($cat->name != "uncategorized"){
																if ($first){
																	echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																	$first = false;
																} else {
																	echo "<span>, </span><a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																}	
															}
													  	}
													  	echo '</p>';
													}
								    			break;
								    			case "comments":
								    				if (!$firstMeta){
										    			echo '<p class="metas-sep">|</p>';
									    			} else {
										    			$firstMeta = false;
									    			}
								    				echo '<p data-rel="metas-comments">';
								    				printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'larch' ), number_format_i18n( get_comments_number() ) );
								    				echo '</p>';
								    			break;
								    			case "customtext":
								    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
										    			echo '<p class="metas-sep">|</p>';
										    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
									    			} else {
										    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
											    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
											    			$firstMeta = false;
										    			}
									    			}
								    			break;
							    			}
						    			}
					    			}
				    			?>
				    		</div>
							<?php
						}
				    ?>	
				    
				    
				    
				    <div class="the_title"><h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" ><a href="<?php esc_url(the_permalink()); ?>"><?php wp_kses_post(the_title()); ?></a></h2></div>
													
					<?php
						
				    if ($media && $upper_fullwidth_media) echo '</div></div>';
						    
			    		if ($posttype != "quote" && $posttype != "link" && !$upper_fullwidth_media){
				    		?>
				    		<div class="blog_excerpt">
						    	<?php 
							    	wp_kses_post(the_excerpt()); 
							    	printf( '<div class="aligncenter"><a class="aio-icon-read" href="%1$s">%2$s</a></div>',
										get_permalink( $postid ),
										__( 'READ MORE', 'larch' )
									);
						    	?>
						    </div>
				    		<?php
			    		}
			    		
			    		if ($media && !$upper_fullwidth_media){
				    		$article_output .= ob_get_clean() . '[/vc_column][/vc_row]';
							echo wp_kses_no_null( do_shortcode( $article_output, false ) );
			    		}
			    		
					?>
		    		
				    <div class="divider-posts"></div>
			    </article>
	    	<?php }
		    ?>
		    </div>
		    <?php
		    } else if (get_option("larch_blog_archive_style") === "masonry"){
			    ?>
			    <div class="post-listing journal isotope <?php if ($larch_animate_posts) echo ' larch-animate-posts not-ready'; ?>" data-columns="3" data-gutter-space="0.25">
	    	
		    	<?php
			    	
			    	$larch_import_fonts[] = get_option('larch_blog_masonry_title_font');
					$titlefont = explode("|",get_option('larch_blog_masonry_title_font'));
					$titlefont[0] = $titlefont[0]."', 'Arial', 'sans-serif";
					if (!isset($titlefont[1])) $titlefont[1] = "";
					$titlecolor = intval(get_option('larch_blog_masonry_title_color'),10);
					$titlesize = get_option('larch_blog_masonry_title_size');
			    
				    while (have_posts()){
					    the_post();
					    $postid = get_the_ID();
					    $post_classes = array("post", "journal-post", "isotope-item", get_post_meta(get_the_ID(), 'posttype_value', true));
					    if ($larch_animate_posts) $post_classes[] = "not-in-view";
					    ?>
					    <article id="post-<?php esc_attr(the_ID()); ?>" <?php post_class($post_classes); ?>>
						
						<div class="blog-default-bg-masonry">
							
								<div class="post-content fadeInUpBig">
							    <?php
								    $posttype = get_post_meta($postid, 'posttype_value', true);
								    switch($posttype){
							    		case "image":
							    		
							    			if (wp_get_attachment_url( get_post_thumbnail_id($postid))){
											?>
												<div class="featured-image">
													<?php
													if (get_option("larch_display_metas") == "on"){
														?>
														<div class="metas">
															<div class="align-metas-center">
															<?php
												    			$metas = explode(",", get_option("larch_metas_to_display"));
												    			if (!empty($metas) && get_post_meta($postid, 'larch_display_featured_image_only_value', 'no') == 'yes'){
													    			$firstMeta = true;
													    			foreach ($metas as $meta){
														    			switch ($meta){
															    			case "date": 
															    				?>
															    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
															    				<?php
															    			break;
															    			case "author":

															    				?>
															    				<p data-rel="metas-author">
																    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
															    				<?php
															    			break;
															    			case "tags":
															    				$posttags = get_the_tags();
																				if ($posttags) {

 																					$first = true;
																					echo '<p data-rel="metas-tags">';

																					foreach($posttags as $tag) {
																						if ($tag->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																							}
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "categories":
															    				$postcats = get_the_category();
																				if ($postcats) {

 																					$first = true;
																					echo '<p data-rel="metas-categories">';

																					foreach($postcats as $cat) {
																						if ($cat->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																							}	
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			
															    			case "customtext":
															    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
// 																	    			echo '<p>|</p>';
																	    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																    			} else {
																	    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																		    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																		    			$firstMeta = false;
																	    			}
																    			}
															    			break;
														    			}
													    			}
												    			}
											    			?>
											    			</div>
											    		</div>
														<?php
													}
											    ?>
													
													<a href="<?php esc_url(the_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
														<img alt="<?php echo esc_attr(get_the_title()); ?>" src="<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id($postid))); ?>" title="<?php echo esc_attr(get_the_title()); ?>"/>
														<span class="post_overlay">
															<i class="fa fa-plus" aria-hidden="true"></i>
														</span>
													</a>
												</div>
												<?php
													if (get_post_meta($postid, 'larch_display_featured_image_only_value', 'no') != 'yes'){
														?>
														<div class="padding-box-masonry">
															<div class="the_title">
																<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" ><a href="<?php esc_attr(the_permalink()); ?>"><?php wp_kses_post(the_title()); ?></a>
																	<div class="comments-masonry">
																		<i class="fa fa-comment-o" style="">
																			<span><?php echo wp_kses_post(get_comments_number()); ?></span>
																		</i>
																	</div>
																</h2>
															</div>
															<div class="post-summary"><?php wp_kses_post(the_excerpt()); ?></div>
															<a class="aio-icon-read" href="<?php echo esc_url(get_the_permalink($postid)); ?>">READ MORE &rarr;</a>
															
														</div>
														<?php
													}
											}
							    			
							    		break;
		
							    		case "slider": 
											$randClass = rand(0,1000);
											?>
												<div class="light">
													<div class="flexslider <?php echo esc_attr($posttype); ?>">
														
														<?php
													if (get_option("larch_display_metas") == "on"){
														?>
														<div class="metas">
															<div class="align-metas-center">
															<?php
												    			$metas = explode(",", get_option("larch_metas_to_display"));
												    			if (!empty($metas)){
													    			$firstMeta = true;
													    			foreach ($metas as $meta){
														    			switch ($meta){
															    			case "date": 

															    				?>
															    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
															    				<?php
															    			break;
															    			case "author":

															    				?>
															    				<p data-rel="metas-author">
																    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
															    				<?php
															    			break;
															    			case "tags":
															    				$posttags = get_the_tags();
																				if ($posttags) {

 																					$first = true;
																					echo '<p data-rel="metas-tags">';

																					foreach($posttags as $tag) {
																						if ($tag->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																							}
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "categories":
															    				$postcats = get_the_category();
																				if ($postcats) {

																					$first = true;
																					echo '<p data-rel="metas-categories">';

																					foreach($postcats as $cat) {
																						if ($cat->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																							}	
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "customtext":
															    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																	    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																    			} else {
																	    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																		    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																		    			$firstMeta = false;
																	    			}
																    			}
															    			break;
														    			}
													    			}
												    			}
											    			?>
											    			</div>
											    		</div>
														<?php
													}
													?>
														
														<ul class="slides">
															<?php
																$sliderData = get_post_meta($postid, "sliderImages_value", true);
																$slide = explode("|*|",$sliderData);
															    foreach ($slide as $s){
															    	if ($s != ""){
															    		$params = explode("|!|",$s);
															    		$attachment = get_post( $params[0] );
															    		$title = isset($attachment->post_excerpt) ? $attachment->post_excerpt : "";
															    		echo "<li>";
															    		echo "<img src='".esc_url($params[1])."' alt='".esc_url($title)."' title='".esc_url($title)."'>";
															    		echo "</li>";	
															    	}
															    }
															?>
														</ul>
													</div>
												</div>
												
												<div class="padding-box-masonry">
													<div class="the_title">
														<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" >
															<a href="<?php esc_url(the_permalink()); ?>"><?php wp_kses_post(the_title()); ?></a>
															<div class="comments-masonry">
																<i class="fa fa-comment-o" style="">
																	<span><?php echo wp_kses_post(get_comments_number()); ?></span>
																</i>
															</div>
														</h2>
													</div>
													<div class="post-summary"><?php wp_kses_post(the_excerpt()); ?></div>
													<a class="aio-icon-read" href="<?php echo esc_url(get_the_permalink($postid)); ?>">READ MORE &rarr;</a>
												</div>
											<?php
							    		break;
		
							    		case "audio":
											?>
											
											
											
											<?php
													if (get_option("larch_display_metas") == "on"){
														?>
														<div class="metas">
															<div class="align-metas-center">
															<?php
												    			$metas = explode(",", get_option("larch_metas_to_display"));
												    			if (!empty($metas)){
													    			$firstMeta = true;
													    			foreach ($metas as $meta){
														    			switch ($meta){
															    			case "date": 

															    				?>
															    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
															    				<?php
															    			break;
															    			case "author":

															    				?>
															    				<p data-rel="metas-author">
																    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') );?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
															    				<?php
															    			break;
															    			case "tags":
															    				$posttags = get_the_tags();
																				if ($posttags) {

 																					$first = true;
																					echo '<p data-rel="metas-tags">';

																					foreach($posttags as $tag) {
																						if ($tag->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																							}
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "categories":
															    				$postcats = get_the_category();
																				if ($postcats) {

																					$first = true;
																					echo '<p data-rel="metas-categories">';

																					foreach($postcats as $cat) {
																						if ($cat->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																							}	
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "customtext":
															    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																	    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																    			} else {
																	    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																		    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																		    			$firstMeta = false;
																	    			}
																    			}
															    			break;
														    			}
													    			}
												    			}
											    			?>
											    			</div>
											    		</div>
														<?php
													}
												?>
												
												
											<div class="audioContainer">
												<?php
													if (get_post_meta($postid, 'audioSource_value', true) == 'embed') echo get_post_meta($postid, 'audioCode_value', true); 
													else {
														$audio = explode("|!|",get_post_meta($postid, 'audioMediaLibrary_value', true));
														if (isset($audio[1])) {
															$ext = explode(".",$audio[1]);
															if (isset($ext)) $ext = $ext[count($ext)-1];
															?>
															<audio controls="controls"><source type="audio/<?php echo esc_attr($ext); ?>" src="<?php echo esc_url($audio[1]); ?>"></audio>
															<?php
														}
													}
												?>
											</div>
												
											<div class="padding-box-masonry">
													
												<div class="the_title">
													
												
													<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" >
														<a href="<?php the_permalink(); ?>"><?php wp_kses_post(the_title()); ?></a>
														<div class="comments-masonry">
															<i class="fa fa-comment-o" style="">
																<span><?php echo wp_kses_post(get_comments_number()); ?></span>
															</i>
														</div>
													</h2>
												</div>
												<div class="post-summary"><?php wp_kses_post(the_excerpt()); ?></div>
												
												<a class="aio-icon-read" href="<?php echo esc_url(get_the_permalink($postid)); ?>">READ MORE &rarr;</a>
												
												
											</div>
											<?php
							    		break;
							    		
							    		case "video":
							    			?>
							    			
							    			
								    			<div class="post-video <?php echo get_post_meta($postid, "videoSource_value", true); ?>">
													
													
													
													<div class="vendor">
														
														<?php
													if (get_option("larch_display_metas") == "on"){
														?>
														<div class="metas">
															<div class="align-metas-center">
															<?php
												    			$metas = explode(",", get_option("larch_metas_to_display"));
												    			if (!empty($metas)){
													    			$firstMeta = true;
													    			foreach ($metas as $meta){
														    			switch ($meta){
															    			case "date": 

															    				?>
															    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
															    				<?php
															    			break;
															    			case "author":

															    				?>
															    				<p data-rel="metas-author">
																    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"> <?php  esc_html(the_author_meta('nickname')); ?></a></p>
															    				<?php
															    			break;
															    			case "tags":
															    				$posttags = get_the_tags();
																				if ($posttags) {

																					$first = true;
																					echo '<p data-rel="metas-tags">';

																					foreach($posttags as $tag) {
																						if ($tag->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																							}
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "categories":
															    				$postcats = get_the_category();
																				if ($postcats) {

 																					$first = true;
																					echo '<p data-rel="metas-categories">';

																					foreach($postcats as $cat) {
																						if ($cat->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																							}	
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "customtext":
															    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																	    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																    			} else {
																	    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																		    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																		    			$firstMeta = false;
																	    			}
																    			}
															    			break;
														    			}
													    			}
												    			}
											    			?>
											    			</div>
											    		</div>
														<?php
													}
												?>
												
												
													<?php
														$videosType = get_post_meta($postid, "videoSource_value", true);
														if ($videosType != "embed"){
															$videos = get_post_meta($postid, "videoCode_value", true);
															$videos = preg_replace( '/\s+/', '', $videos );
															$vid = explode(",",$videos);
														}
														switch (get_post_meta($postid, "videoSource_value", true)){
															case "media":
																$video = explode("|!|",get_post_meta($postid, 'videoMediaLibrary_value', true));
																if (isset($video[1])) {
																	$ext = explode(".",$video[1]);
																	if (isset($ext)) $ext = $ext[count($ext)-1];
																	?>
																	<video controls="controls" class="video-controls"><source type="video/<?php echo esc_attr($ext); ?>" src="<?php echo esc_url($video[1]); ?>"></video>
																	<?php
																}
															break;
															case "youtube":
																if (isset($vid[0])) echo "<iframe src='//www.youtube.com/embed/".esc_attr($vid[0])."' frameborder='0' allowfullscreen></iframe>";
																break;
															case "vimeo":
																if (isset($vid[0])) echo '<iframe src="https://player.vimeo.com/video/'.esc_attr($vid[0]).'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
																break;
														}						
													?>
													</div>
								    			</div>
											
											
											<div class="padding-box-masonry">
											
												<div class="the_title">
													<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" ><a href="<?php the_permalink(); ?>"><?php wp_kses_post(the_title()); ?></a>
														<div class="comments-masonry">
															<i class="fa fa-comment-o" style="">
																<span><?php echo wp_kses_post(get_comments_number()); ?></span>
															</i>
														</div>
													</h2>
												</div>
												<div class="post-summary"><?php wp_kses_post(the_excerpt()); ?></div>
												<a class="aio-icon-read" href="<?php echo esc_url(get_the_permalink($postid)); ?>">READ MORE &rarr;</a>
											</div>
											<?php
							    		break;
							    		
							    		case "gallery":
											$slider = get_post_meta($postid,'gallery_slider_value',true);
											if ($slider != '-1'){
												if (substr($slider, 0, 10) === "revSlider_"){
													?>
													<div class="gallery_container">
														
														<?php
													if (get_option("larch_display_metas") == "on"){
														?>
														<div class="metas">
															<div class="align-metas-center">
															<?php
												    			$metas = explode(",", get_option("larch_metas_to_display"));
												    			if (!empty($metas)){
													    			$firstMeta = true;
													    			foreach ($metas as $meta){
														    			switch ($meta){
															    			case "date": 

															    				?>
															    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
															    				<?php
															    			break;
															    			case "author":

															    				?>
															    				<p data-rel="metas-author">
																    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') );?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
															    				<?php
															    			break;
															    			case "tags":
															    				$posttags = get_the_tags();
																				if ($posttags) {

 																					$first = true;
																					echo '<p data-rel="metas-tags">';

																					foreach($posttags as $tag) {
																						if ($tag->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																							}
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "categories":
															    				$postcats = get_the_category();
																				if ($postcats) {

																					$first = true;

																					echo '<p data-rel="metas-categories">';

																					foreach($postcats as $cat) {
																						if ($cat->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																							}	
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "customtext":
															    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																	    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																    			} else {
																	    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																		    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																		    			$firstMeta = false;
																	    			}
																    			}
															    			break;
														    			}
													    			}
												    			}
											    			?>
											    			</div>
											    		</div>
														<?php
													}
												?>
														
														<?php
															if (!function_exists('putRevSlider')){
																echo esc_html__('Please install the missing plugin - Revolution Slider.', 'larch');
															} else {
																putRevSlider(substr($slider, 10));
															}
														?>
													</div>
													<?php
												} else {
													?>
													<div class="gallery_container">
														
														<?php
													if (get_option("larch_display_metas") == "on"){
														?>
														<div class="metas">
															<div class="align-metas-center">
															<?php
												    			$metas = explode(",", get_option("larch_metas_to_display"));
												    			if (!empty($metas)){
													    			$firstMeta = true;
													    			foreach ($metas as $meta){
														    			switch ($meta){
															    			case "date": 

															    				?>
															    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
															    				<?php
															    			break;
															    			case "author":

															    				?>
															    				<p data-rel="metas-author">
																    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') );?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
															    				<?php
															    			break;
															    			case "tags":
															    				$posttags = get_the_tags();
																				if ($posttags) {

																					$first = true;

																					echo '<p data-rel="metas-tags">';

																					foreach($posttags as $tag) {
																						if ($tag->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																							}
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "categories":
															    				$postcats = get_the_category();
																				if ($postcats) {

																					$first = true;

																					echo '<p data-rel="metas-categories">';

																					foreach($postcats as $cat) {
																						if ($cat->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																							}	
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "customtext":
															    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																	    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																    			} else {
																	    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																		    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																		    			$firstMeta = false;
																	    			}
																    			}
															    			break;
														    			}
													    			}
												    			}
											    			?>
											    			</div>
											    		</div>
														<?php
													}
												?>
												
												
														<?php
															if (!function_exists('masterslider')){
																echo esc_html__('Please install the missing plugin - MasterSlider.', 'larch');
															} else {
																echo do_shortcode( '[masterslider alias="'.substr($slider, 13).'"]' );
															}
														?>
													</div>
													<?php
												}
											}
											?>
											<div class="padding-box-masonry">
												<div class="the_title">
													<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" ><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
													<div class="comments-masonry">
														<i class="fa fa-comment-o" style="">
															<span><?php echo wp_kses_post(get_comments_number()); ?></span>
														</i>
													</div>
													
													</h2>
												</div>
												<div class="post-summary"><?php wp_kses_post(the_excerpt()); ?></div>
												<a class="aio-icon-read" href="<?php echo esc_url(get_the_permalink($postid)); ?>">READ MORE &rarr;</a>
											</div>
											<?php
										break;
							    		
							    		case "quote":
							    			?>
							    			<a href="<?php the_permalink(); ?>">
								    			<div class="post-quote">
						                        	<blockquote><i class="fa fa-quote-left"></i> <?php echo wp_kses_post(get_post_meta($postid, 'quote_text_value', true)); ?><i class="fa fa-quote-right"></i></blockquote><span class="author-quote"><?php echo wp_kses_post(get_post_meta($postid, 'quote_author_value', true)); ?></span>
						                        </div>
					                        </a>

							    			<?php
							    		break;
		
										case "link":
											?>
											<div class="padding-box-masonry">
												<h2 class="post-title post-link" style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>">
													<?php
														$linkurl = get_post_meta($postid, 'link_url_value', true) != '' ? get_post_meta($postid, 'link_url_value', true) : get_permalink();
														$linktext = get_post_meta($postid, 'link_text_value', true) != '' ? get_post_meta($postid, 'link_text_value', true) : $linkurl;
													?>
													
													<div class="comments-masonry">
														<i class="fa fa-comment-o" style="">
															<span><?php echo wp_kses_post(get_comments_number()); ?></span>
														</i>
													</div>
													
													
													<a href="<?php echo esc_url($linkurl); ?>" target="_blank"><?php echo esc_html($linktext); ?></a>
						                        </h2>
						                        
											</div>
											<?php
										break;
							    		
							    		case "text": default:
							    		
							    			if ($posttype != "text"){
												if (wp_get_attachment_url( get_post_thumbnail_id($postid))){
												?>
													<div class="featured-image">
														<?php
														if (get_option("larch_display_metas") == "on"){
															?>
															<div class="metas">
																<div class="align-metas-center">
																<?php
													    			$metas = explode(",", get_option("larch_metas_to_display"));
													    			if (!empty($metas)){
														    			$firstMeta = true;
														    			foreach ($metas as $meta){
															    			switch ($meta){
																    			case "date": 
																    				?>
																    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
																    				<?php
																    			break;
																    			case "author":
	
																    				?>
																    				<p data-rel="metas-author">
																	    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
																    				<?php
																    			break;
																    			case "tags":
																    				$posttags = get_the_tags();
																					if ($posttags) {
	
	 																					$first = true;
																						echo '<p data-rel="metas-tags">';
	
																						foreach($posttags as $tag) {
																							if ($tag->name != "uncategorized"){
																								if ($first){
																									echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																									$first = false;
																								} else {
																									echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																								}
																							}
																					  	}
																					  	echo '</p>';
																					}
																    			break;
																    			case "categories":
																    				$postcats = get_the_category();
																					if ($postcats) {
	
	 																					$first = true;
																						echo '<p data-rel="metas-categories">';
	
																						foreach($postcats as $cat) {
																							if ($cat->name != "uncategorized"){
																								if ($first){
																									echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																									$first = false;
																								} else {
																									echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																								}	
																							}
																					  	}
																					  	echo '</p>';
																					}
																    			break;
																    			
																    			case "customtext":
																    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
	// 																	    			echo '<p>|</p>';
																		    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																	    			} else {
																		    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																			    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																			    			$firstMeta = false;
																		    			}
																	    			}
																    			break;
															    			}
														    			}
													    			}
												    			?>
												    			</div>
												    		</div>
															<?php
														}
												    ?>
														
														<a href="<?php esc_url(the_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
															<img alt="<?php echo esc_attr(get_the_title()); ?>" src="<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id($postid))); ?>" title="<?php echo esc_attr(get_the_title()); ?>"/>
															<span class="post_overlay">
																<i class="fa fa-plus" aria-hidden="true"></i>
															</span>
														</a>
													</div>
													<?php
												}
												
												if (get_post_meta($postid, 'larch_display_featured_image_only_value', 'no') != 'yes'){
													?>
													<div class="padding-box-masonry">
														<div class="the_title">
															<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" ><a href="<?php esc_attr(the_permalink()); ?>"><?php wp_kses_post(the_title()); ?></a>
																<div class="comments-masonry">
																	<i class="fa fa-comment-o" style="">
																		<span><?php echo wp_kses_post(get_comments_number()); ?></span>
																	</i>
																</div>
															</h2>
														</div>
														<div class="post-summary"><?php wp_kses_post(the_excerpt()); ?></div>
														<a class="aio-icon-read" href="<?php echo esc_url(get_the_permalink($postid)); ?>">READ MORE &rarr;</a>
														
													</div>
													<?php
												}
												break;
											}
							    		
							    			?>
							    			
							    			 <?php
													if (get_option("larch_display_metas") == "on"){
														?>
														<div class="metas">
															<div class="align-metas-center">
															<?php
												    			$metas = explode(",", get_option("larch_metas_to_display"));
												    			if (!empty($metas)){
													    			$firstMeta = true;
													    			foreach ($metas as $meta){
														    			switch ($meta){
															    			case "date": 

															    				?>
															    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
															    				<?php
															    			break;
															    			case "author":

															    				?>
															    				<p data-rel="metas-author">
																    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') );?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
															    				<?php
															    			break;
															    			case "tags":
															    				$posttags = get_the_tags();
																				if ($posttags) {

																					$first = true;

																					echo '<p data-rel="metas-tags">';

																					foreach($posttags as $tag) {
																						if ($tag->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																							}
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "categories":
															    				$postcats = get_the_category();
																				if ($postcats) {

																					$first = true;

																					echo '<p data-rel="metas-categories">';

																					foreach($postcats as $cat) {
																						if ($cat->name != "uncategorized"){
																							if ($first){
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																								$first = false;
																							} else {
																								echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																							}	
																						}
																				  	}
																				  	echo '</p>';
																				}
															    			break;
															    			case "customtext":
															    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																	    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																    			} else {
																	    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																		    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																		    			$firstMeta = false;
																	    			}
																    			}
															    			break;
														    			}
													    			}
												    			}
											    			?>
											    			</div>
											    		</div>
														<?php
													}
												?>
												
												
												
							    			<div class="padding-box-masonry">
							    				<div class="the_title no-feature">
								    				
							    			
								    				<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" >
									    				<div class="comments-masonry">
															<i class="fa fa-comment-o" style="">
																<span><?php echo wp_kses_post(get_comments_number()); ?></span>
															</i>
														</div>
									    				<a href="<?php the_permalink(); ?>"><?php wp_kses_post(the_title()); ?></a></h2></div>
												<div class="post-summary"><?php wp_kses_post(the_excerpt()); ?></div>
												<a class="aio-icon-read" href="<?php echo esc_url(get_the_permalink($postid)); ?>">READ MORE &rarr;</a>
							    			
							    			<?php
							    		break;
						    		}
								    ?>
							</div>
						</div>
				    </article>
					    <?php
				    }
				    
				    ?>
		    	</div>
		    	
			    <?php    
				    $larch_inline_script .= '
						jQuery(document).ready(function(){
							"use strict";
							var forceGutter = 50; // change to false to return to the normal behavior.
							(function(e){"use strict";e.Isotope.prototype._getMasonryGutterColumns=function(){var e=this.options.masonry&&this.options.masonry.gutterWidth||0;var t=this.element.width();this.masonry.columnWidth=this.options.masonry&&this.options.masonry.columnWidth||this.$filteredAtoms.outerWidth(true)||t;this.masonry.columnWidth+=e;this.masonry.cols=Math.floor((t+e)/this.masonry.columnWidth);this.masonry.cols=Math.max(this.masonry.cols,1)};e.Isotope.prototype._masonryReset=function(){this.masonry={};this._getMasonryGutterColumns();var e=this.masonry.cols;this.masonry.colYs=[];while(e--){this.masonry.colYs.push(0)}};e.Isotope.prototype._masonryResizeChanged=function(){var e=this.masonry.cols;this._getMasonryGutterColumns();return this.masonry.cols!==e};e(document).ready(function(){"use strict";var t=e(".journal");var n=0;var r=0;var i=function(){var e=parseInt(t.data("columns"));var i=t.data("gutterSpace");var s=t.width();var o=0;if(isNaN(i)){i=.02}else if(i>.05||i<0){i=.02}if(s<568){e=1}else if(s<768){e-=2}else if(s<991){e-=1;if(e<2){e=2}}if(e<1){e=1}r=forceGutter!=false ? forceGutter : Math.floor(s*i);var u=r*(e-1);var a=s-u;n=Math.floor(a/e);o=r;if(1==e){o=20}t.children(".journal-post").css({width:n+"px",marginBottom:o+"px"})};i();window.iso = t.isotope({itemSelector:".journal-post",resizable:false,masonry:{columnWidth:n,gutterWidth:r}});t.imagesLoaded(function(){i();t.isotope({itemSelector:".journal-post",resizable:true,masonry:{columnWidth:n,gutterWidth:r}})});e(window).smartresize(function(){i();t.isotope({masonry:{columnWidth:n,gutterWidth:r}})});var s=e(".wc-shortcodes-filtering .wc-shortcodes-term");s.on("click",function(i){i.preventDefault();s.removeClass("wc-shortcodes-term-active");e(this).addClass("wc-shortcodes-term-active");var o=e(this).attr("data-filter");t.isotope({filter:o,masonry:{columnWidth:n,gutterWidth:r}});return false})})})(jQuery);
							jQuery(".flexslider").flexslider({
								animation: "fade",
								slideshow: true,
								slideshowSpeed: 3500,
								animationDuration: 1000,
								directionNav: true,
								controlNav: true,
								smootheHeight:false,
								start: function(slider) {
								  slider.removeClass("loading").css("overflow","");
								}
							});
						});
						jQuery(window).load(function(){ jQuery(window).resize(); jQuery(".gallery_container").css("opacity",1); jQuery(".post-listing.larch-animate-posts").removeClass("not-ready"); });
					';
		    	} else {
			    	// MASONRY GRID
			    	?>
			    	<div class="post-listing journal isotope <?php if ($larch_animate_posts) echo ' larch-animate-posts not-ready'; ?>" data-columns="3" >
		    	
			    	<?php
				    	
				    	$larch_import_fonts[] = get_option('larch_blog_masonry_title_font');
						$titlefont = explode("|",get_option('larch_blog_masonry_title_font'));
						$titlefont[0] = $titlefont[0]."', 'Arial', 'sans-serif";
						if (!isset($titlefont[1])) $titlefont[1] = "";
						$titlecolor = intval(get_option('larch_blog_masonry_title_color'),10);
						$titlesize = get_option('larch_blog_masonry_title_size');
				    	
					    while (have_posts()){
						    the_post();
						    $postid = get_the_ID();
						    
						    $post_categories = wp_get_post_categories( $postid );
						    $post_categories_class = array();
						    foreach ($post_categories as $cat){
							    $thisCat = get_category($cat);
							    $post_categories_class[] = "cat-".$thisCat->slug;
						    }
						    
						    if ($larch_animate_posts) $post_categories_class[] = "not-in-view";
						    ?>
						    <article id="<?php echo esc_attr("post-".get_the_ID()); ?>" class="larch-masonry-grid post journal-post isotope-item <?php echo esc_attr(get_post_meta(get_the_ID(), 'posttype_value', true)); if (is_sticky()) echo esc_attr(" sticky"); echo " ".implode(" ", $post_categories_class); ?>">
								
								<div class="blog-default-bg-masonry">
									
										<div class="post-content fadeInUpBig">
									    <?php
										    $posttype = get_post_meta($postid, 'posttype_value', true);
										    switch($posttype){
									    		case "image": 
									    		
									    			if (wp_get_attachment_url( get_post_thumbnail_id($postid))){
													?>
														<div class="featured-image">
															<div class="info_wrapper">
																<?php
																if (get_option("larch_display_metas") == "on"){
																	?>
																	
																	<div class="metas">
																		<div class="align-metas-center">
																		<?php
															    			$metas = explode(",", get_option("larch_metas_to_display"));
															    			if (!empty($metas)){
																    			$firstMeta = true;
																    			foreach ($metas as $meta){
																	    			switch ($meta){
																		    			case "date": 
																		    				?>
																		    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
																		    				<?php
																		    			break;
																		    			case "author":
			
																		    				?>
																		    				<p data-rel="metas-author">
																			    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
																		    				<?php
																		    			break;
																		    			case "tags":
																		    				$posttags = get_the_tags();
																							if ($posttags) {
			
			 																					$first = true;
																								echo '<p data-rel="metas-tags">';
			
																								foreach($posttags as $tag) {
																									if ($tag->name != "uncategorized"){
																										if ($first){
																											echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																											$first = false;
																										} else {
																											echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																										}
																									}
																							  	}
																							  	echo '</p>';
																							}
																		    			break;
																		    			case "categories":
																		    				$postcats = get_the_category();
																							if ($postcats) {
			
			 																					$first = true;
																								echo '<p data-rel="metas-categories">';
			
																								foreach($postcats as $cat) {
																									if ($cat->name != "uncategorized"){
																										if ($first){
																											echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																											$first = false;
																										} else {
																											echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																										}	
																									}
																							  	}
																							  	echo '</p>';
																							}
																		    			break;
																		    			
																		    			case "customtext":
																		    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																				    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																			    			} else {
																				    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																					    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																					    			$firstMeta = false;
																				    			}
																			    			}
																		    			break;
																	    			}
																    			}
															    			}
														    			?>
														    			</div>
														    		</div>
																	<?php
																	}
															    ?>
															    <div class="the_title">
																	<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" ><a href="<?php esc_attr(the_permalink()); ?>"><?php wp_kses_post(the_title()); ?></a>
																	</h2>
																</div>
															</div>
															<a href="<?php esc_url(the_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
																<img alt="<?php echo esc_attr(get_the_title()); ?>" src="<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id($postid))); ?>" title="<?php echo esc_attr(get_the_title()); ?>"/>
																<span class="post_overlay"></span>
															</a>
														</div>
														<?php
													}
									    			
									    		break;
				
									    		case "slider": 
													$randClass = rand(0,1000);
													?>
														<div class="light">
															<div class="flexslider <?php echo esc_attr($posttype); ?>">
																<div class="info_wrapper">
																	<?php
																	if (get_option("larch_display_metas") == "on"){
																		?>
																			<div class="metas">
																				<div class="align-metas-center">
																				<?php
																	    			$metas = explode(",", get_option("larch_metas_to_display"));
																	    			if (!empty($metas)){
																		    			$firstMeta = true;
																		    			foreach ($metas as $meta){
																			    			switch ($meta){
																				    			case "date": 
					
																				    				?>
																				    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
																				    				<?php
																				    			break;
																				    			case "author":
					
																				    				?>
																				    				<p data-rel="metas-author">
																					    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
																				    				<?php
																				    			break;
																				    			case "tags":
																				    				$posttags = get_the_tags();
																									if ($posttags) {
					
					 																					$first = true;
																										echo '<p data-rel="metas-tags">';
					
																										foreach($posttags as $tag) {
																											if ($tag->name != "uncategorized"){
																												if ($first){
																													echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																													$first = false;
																												} else {
																													echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																												}
																											}
																									  	}
																									  	echo '</p>';
																									}
																				    			break;
																				    			case "categories":
																				    				$postcats = get_the_category();
																									if ($postcats) {
					
																										$first = true;
																										echo '<p data-rel="metas-categories">';
					
																										foreach($postcats as $cat) {
																											if ($cat->name != "uncategorized"){
																												if ($first){
																													echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																													$first = false;
																												} else {
																													echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																												}	
																											}
																									  	}
																									  	echo '</p>';
																									}
																				    			break;
																				    			case "customtext":
																				    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																						    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																					    			} else {
																						    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																							    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																							    			$firstMeta = false;
																						    			}
																					    			}
																				    			break;
																			    			}
																		    			}
																	    			}
																    			?>
																    			</div>
																    		</div>
																		<?php
																	}
																?>
																	<div class="the_title">
																		<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" ><a href="<?php esc_attr(the_permalink()); ?>"><?php wp_kses_post(the_title()); ?></a>
																		</h2>
																	</div>
																</div>
																
																<ul class="slides">
																	<?php
																		$sliderData = get_post_meta($postid, "sliderImages_value", true);
																		$slide = explode("|*|",$sliderData);
																	    foreach ($slide as $s){
																	    	if ($s != ""){
																	    		$params = explode("|!|",$s);
																	    		$attachment = get_post( $params[0] );
																	    		$title = isset($attachment->post_excerpt) ? $attachment->post_excerpt : "";
																	    		echo "<li>";
																	    		echo "<img src='".esc_url($params[1])."' alt='".esc_url($title)."' title='".esc_url($title)."'>";
																	    		echo "</li>";	
																	    	}
																	    }
																	?>
																</ul>
															</div>
														</div>
													<?php
									    		break;
				
									    		case "audio":
													?>
													<div class="info_wrapper">
														<?php
														if (get_option("larch_display_metas") == "on"){
															?>
															<div class="metas">
																<div class="align-metas-center">
																<?php
													    			$metas = explode(",", get_option("larch_metas_to_display"));
													    			if (!empty($metas)){
														    			$firstMeta = true;
														    			foreach ($metas as $meta){
															    			switch ($meta){
																    			case "date": 
			
																    				?>
																    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
																    				<?php
																    			break;
																    			case "author":
			
																    				?>
																    				<p data-rel="metas-author">
																	    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') );?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
																    				<?php
																    			break;
																    			case "tags":
																    				$posttags = get_the_tags();
																					if ($posttags) {
			
																							$first = true;
																						echo '<p data-rel="metas-tags">';
			
																						foreach($posttags as $tag) {
																							if ($tag->name != "uncategorized"){
																								if ($first){
																									echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																									$first = false;
																								} else {
																									echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																								}
																							}
																					  	}
																					  	echo '</p>';
																					}
																    			break;
																    			case "categories":
																    				$postcats = get_the_category();
																					if ($postcats) {
			
																						$first = true;
																						echo '<p data-rel="metas-categories">';
			
																						foreach($postcats as $cat) {
																							if ($cat->name != "uncategorized"){
																								if ($first){
																									echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																									$first = false;
																								} else {
																									echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																								}	
																							}
																					  	}
																					  	echo '</p>';
																					}
																    			break;
																    			case "customtext":
																    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																		    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																	    			} else {
																		    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																			    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																			    			$firstMeta = false;
																		    			}
																	    			}
																    			break;
															    			}
														    			}
													    			}
												    			?>
												    			</div>
												    		</div>
															<?php
														}
													?>
														<div class="the_title">
															<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" >
																<a href="<?php the_permalink(); ?>"><?php wp_kses_post(the_title()); ?></a>
															</h2>
														</div>
													</div>
														
													<div class="audioContainer">
														<?php
															if (get_post_meta($postid, 'audioSource_value', true) == 'embed') echo get_post_meta($postid, 'audioCode_value', true); 
															else {
																$audio = explode("|!|",get_post_meta($postid, 'audioMediaLibrary_value', true));
																if (isset($audio[1])) {
																	$ext = explode(".",$audio[1]);
																	if (isset($ext)) $ext = $ext[count($ext)-1];
																	?>
																	<audio controls="controls"><source type="audio/<?php echo esc_attr($ext); ?>" src="<?php echo esc_url($audio[1]); ?>"></audio>
																	<?php
																}
															}
														?>
													</div>
													<?php
									    		break;
									    		
									    		case "video":
									    			?>
									    			<div class="post-video <?php echo get_post_meta($postid, "videoSource_value", true); ?>">
														
														
														
														<div class="vendor">
															<div class="info_wrapper">
																<?php
																	if (get_option("larch_display_metas") == "on"){
																		?>
																		<div class="metas">
																			<div class="align-metas-center">
																			<?php
																    			$metas = explode(",", get_option("larch_metas_to_display"));
																    			if (!empty($metas)){
																	    			$firstMeta = true;
																	    			foreach ($metas as $meta){
																		    			switch ($meta){
																			    			case "date": 
					
																			    				?>
																			    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
																			    				<?php
																			    			break;
																			    			case "author":
					
																			    				?>
																			    				<p data-rel="metas-author">
																				    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"> <?php  esc_html(the_author_meta('nickname')); ?></a></p>
																			    				<?php
																			    			break;
																			    			case "tags":
																			    				$posttags = get_the_tags();
																								if ($posttags) {
					
																									$first = true;
																									echo '<p data-rel="metas-tags">';
					
																									foreach($posttags as $tag) {
																										if ($tag->name != "uncategorized"){
																											if ($first){
																												echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																												$first = false;
																											} else {
																												echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																											}
																										}
																								  	}
																								  	echo '</p>';
																								}
																			    			break;
																			    			case "categories":
																			    				$postcats = get_the_category();
																								if ($postcats) {
					
																										$first = true;
																									echo '<p data-rel="metas-categories">';
					
																									foreach($postcats as $cat) {
																										if ($cat->name != "uncategorized"){
																											if ($first){
																												echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																												$first = false;
																											} else {
																												echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																											}	
																										}
																								  	}
																								  	echo '</p>';
																								}
																			    			break;
																			    			case "customtext":
																			    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																					    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																				    			} else {
																					    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																						    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																						    			$firstMeta = false;
																					    			}
																				    			}
																			    			break;
																		    			}
																	    			}
																    			}
															    			?>
															    			</div>
															    		</div>
																		<?php
																	}
																?>
																<div class="the_title">
																	<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" >
																		<a href="<?php the_permalink(); ?>"><?php wp_kses_post(the_title()); ?></a>
																	</h2>
																</div>
															</div>
														<?php
		
														$videosType = get_post_meta($postid, "videoSource_value", true);
														if ($videosType != "embed"){
															$videos = get_post_meta($postid, "videoCode_value", true);
															$videos = preg_replace( '/\s+/', '', $videos );
															$vid = explode(",",$videos);
														}
														switch (get_post_meta($postid, "videoSource_value", true)){
															case "media":
																$video = explode("|!|",get_post_meta($postid, 'videoMediaLibrary_value', true));
																if (isset($video[1])) {
																	$ext = explode(".",$video[1]);
																	if (isset($ext)) $ext = $ext[count($ext)-1];
																	?>
																	<video controls="controls" class="video-controls"><source type="video/<?php echo esc_attr($ext); ?>" src="<?php echo esc_url($video[1]); ?>"></video>
																	<?php
																}
															break;
															case "youtube":
																if (isset($vid[0])) echo "<iframe src='//www.youtube.com/embed/".esc_attr($vid[0])."' frameborder='0' allowfullscreen></iframe>";
																break;
															case "vimeo":
																if (isset($vid[0])) echo '<iframe src="https://player.vimeo.com/video/'.esc_attr($vid[0]).'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
																break;
														}
														?>
														</div>
									    			</div>
													<?php
									    		break;
									    		
									    		case "gallery":
													$slider = get_post_meta($postid,'gallery_slider_value',true);
													if ($slider != '-1'){
														if (substr($slider, 0, 10) === "revSlider_"){
															?>
															<div class="gallery_container">
																<div class="info_wrapper">
																		<?php
																	if (get_option("larch_display_metas") == "on"){
																		?>
																		<div class="metas">
																			<div class="align-metas-center">
																			<?php
																    			$metas = explode(",", get_option("larch_metas_to_display"));
																    			if (!empty($metas)){
																	    			$firstMeta = true;
																	    			foreach ($metas as $meta){
																		    			switch ($meta){
																			    			case "date": 
				
																			    				?>
																			    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
																			    				<?php
																			    			break;
																			    			case "author":
				
																			    				?>
																			    				<p data-rel="metas-author">
																				    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') );?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
																			    				<?php
																			    			break;
																			    			case "tags":
																			    				$posttags = get_the_tags();
																								if ($posttags) {
				
				 																					$first = true;
																									echo '<p data-rel="metas-tags">';
				
																									foreach($posttags as $tag) {
																										if ($tag->name != "uncategorized"){
																											if ($first){
																												echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																												$first = false;
																											} else {
																												echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																											}
																										}
																								  	}
																								  	echo '</p>';
																								}
																			    			break;
																			    			case "categories":
																			    				$postcats = get_the_category();
																								if ($postcats) {
				
																									$first = true;
				
																									echo '<p data-rel="metas-categories">';
				
																									foreach($postcats as $cat) {
																										if ($cat->name != "uncategorized"){
																											if ($first){
																												echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																												$first = false;
																											} else {
																												echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																											}	
																										}
																								  	}
																								  	echo '</p>';
																								}
																			    			break;
																			    			case "customtext":
																			    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																					    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																				    			} else {
																					    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																						    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																						    			$firstMeta = false;
																					    			}
																				    			}
																			    			break;
																		    			}
																	    			}
																    			}
															    			?>
															    			</div>
															    		</div>
																		<?php
																	}
																?>
																	<div class="the_title">
																		<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" >
																			<a href="<?php the_permalink(); ?>"><?php wp_kses_post(the_title()); ?></a>
																		</h2>
																	</div>
																</div>
																
																<?php
																	if (!function_exists('putRevSlider')){
																		echo esc_html__('Please install the missing plugin - Revolution Slider.', 'larch');
																	} else {
																		putRevSlider(substr($slider, 10));
																	}
																?>
															</div>
															<?php
														} else {
															?>
															<div class="gallery_container">
																<div class="info_wrapper">
																		<?php
																	if (get_option("larch_display_metas") == "on"){
																		?>
																		<div class="metas">
																			<div class="align-metas-center">
																			<?php
																    			$metas = explode(",", get_option("larch_metas_to_display"));
																    			if (!empty($metas)){
																	    			$firstMeta = true;
																	    			foreach ($metas as $meta){
																		    			switch ($meta){
																			    			case "date": 
				
																			    				?>
																			    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
																			    				<?php
																			    			break;
																			    			case "author":
				
																			    				?>
																			    				<p data-rel="metas-author">
																				    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') );?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
																			    				<?php
																			    			break;
																			    			case "tags":
																			    				$posttags = get_the_tags();
																								if ($posttags) {
				
																									$first = true;
				
																									echo '<p data-rel="metas-tags">';
				
																									foreach($posttags as $tag) {
																										if ($tag->name != "uncategorized"){
																											if ($first){
																												echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																												$first = false;
																											} else {
																												echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																											}
																										}
																								  	}
																								  	echo '</p>';
																								}
																			    			break;
																			    			case "categories":
																			    				$postcats = get_the_category();
																								if ($postcats) {
				
																									$first = true;
				
																									echo '<p data-rel="metas-categories">';
				
																									foreach($postcats as $cat) {
																										if ($cat->name != "uncategorized"){
																											if ($first){
																												echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																												$first = false;
																											} else {
																												echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																											}	
																										}
																								  	}
																								  	echo '</p>';
																								}
																			    			break;
																			    			case "customtext":
																			    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																					    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																				    			} else {
																					    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																						    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																						    			$firstMeta = false;
																					    			}
																				    			}
																			    			break;
																		    			}
																	    			}
																    			}
															    			?>
															    			</div>
															    		</div>
																		<?php
																	}
																?>
																	<div class="the_title">
																		<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" >
																			<a href="<?php the_permalink(); ?>"><?php wp_kses_post(the_title()); ?></a>
																		</h2>
																	</div>
																</div>
														
																<?php
																	if (!function_exists('masterslider')){
																		echo esc_html__('Please install the missing plugin - MasterSlider.', 'larch');
																	} else {
																		echo do_shortcode( '[masterslider alias="'.substr($slider, 13).'"]' );
																	}
																?>
															</div>
															<?php
														}
													}
												break;
									    		
									    		case "quote":
									    			?>
									    			<a href="<?php the_permalink(); ?>">
										    			<div class="post-quote">
								                        	<blockquote><i class="fa fa-quote-left"></i> <?php echo wp_kses_post(get_post_meta($postid, 'quote_text_value', true)); ?><i class="fa fa-quote-right"></i></blockquote><span class="author-quote"><?php echo wp_kses_post(get_post_meta($postid, 'quote_author_value', true)); ?></span>
								                        </div>
							                        </a>
		
									    			<?php
									    		break;
				
												case "link":
													?>
													<div class="padding-box-masonry">
														<h2 class="post-title post-link" style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>">
															<?php
																$linkurl = get_post_meta($postid, 'link_url_value', true) != '' ? get_post_meta($postid, 'link_url_value', true) : get_permalink();
																$linktext = get_post_meta($postid, 'link_text_value', true) != '' ? get_post_meta($postid, 'link_text_value', true) : $linkurl;
															?>
															<a href="<?php echo esc_url($linkurl); ?>" target="_blank"><?php echo esc_html($linktext); ?></a>
								                        </h2>
								                        
													</div>
													<?php
												break;
									    		
									    		case "text": case "": default:
													if ($posttype != "text"){
														if (wp_get_attachment_url( get_post_thumbnail_id($postid))){
														?>
															<div class="featured-image">
																<div class="info_wrapper">
																	<?php
																	if (get_option("larch_display_metas") == "on"){
																		?>
																		<div class="metas">
																			<div class="align-metas-center">
																			<?php
																    			$metas = explode(",", get_option("larch_metas_to_display"));
																    			if (!empty($metas)){
																	    			$firstMeta = true;
																	    			foreach ($metas as $meta){
																		    			switch ($meta){
																			    			case "date": 
																			    				?>
																			    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
																			    				<?php
																			    			break;
																			    			case "author":
				
																			    				?>
																			    				<p data-rel="metas-author">
																				    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
																			    				<?php
																			    			break;
																			    			case "tags":
																			    				$posttags = get_the_tags();
																								if ($posttags) {
				
				 																					$first = true;
																									echo '<p data-rel="metas-tags">';
				
																									foreach($posttags as $tag) {
																										if ($tag->name != "uncategorized"){
																											if ($first){
																												echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																												$first = false;
																											} else {
																												echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																											}
																										}
																								  	}
																								  	echo '</p>';
																								}
																			    			break;
																			    			case "categories":
																			    				$postcats = get_the_category();
																								if ($postcats) {
				
				 																					$first = true;
																									echo '<p data-rel="metas-categories">';
				
																									foreach($postcats as $cat) {
																										if ($cat->name != "uncategorized"){
																											if ($first){
																												echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																												$first = false;
																											} else {
																												echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																											}	
																										}
																								  	}
																								  	echo '</p>';
																								}
																			    			break;
																			    			
																			    			case "customtext":
																			    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																					    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																				    			} else {
																					    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																						    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																						    			$firstMeta = false;
																					    			}
																				    			}
																			    			break;
																		    			}
																	    			}
																    			}
															    			?>
															    			</div>
															    		</div>
																		<?php
																	}
															    ?>
															    	<div class="the_title">
																		<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" >
																			<a href="<?php the_permalink(); ?>"><?php wp_kses_post(the_title()); ?></a>
																		</h2>
																	</div>
															    </div>
																
																<a href="<?php esc_url(the_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
																	<img alt="<?php echo esc_attr(get_the_title()); ?>" src="<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id($postid))); ?>" title="<?php echo esc_attr(get_the_title()); ?>"/>
																	<span class="post_overlay">
																		<i class="fa fa-plus" aria-hidden="true"></i>
																	</span>
																</a>
															</div>
															<?php
														}
														break;
													}
									    			?>
									    			<div class="info_wrapper">
										    			<?php
														if (get_option("larch_display_metas") == "on"){
															?>
															<div class="metas">
																<div class="align-metas-center">
																<?php
													    			$metas = explode(",", get_option("larch_metas_to_display"));
													    			if (!empty($metas)){
														    			$firstMeta = true;
														    			foreach ($metas as $meta){
															    			switch ($meta){
																    			case "date": 
			
																    				?>
																    				<p class="blog-date"><?php echo get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y"); ?></p>
																    				<?php
																    			break;
																    			case "author":
			
																    				?>
																    				<p data-rel="metas-author">
																	    				<a class="the_author" href="<?php echo get_author_posts_url( get_the_author_meta('ID') );?>"><?php  esc_html(the_author_meta('nickname'));?></a></p>
																    				<?php
																    			break;
																    			case "tags":
																    				$posttags = get_the_tags();
																					if ($posttags) {
			
																						$first = true;
			
																						echo '<p data-rel="metas-tags">';
			
																						foreach($posttags as $tag) {
																							if ($tag->name != "uncategorized"){
																								if ($first){
																									echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>"; 
																									$first = false;
																								} else {
																									echo "<a href='".esc_url( home_url( '/' ) )."tag/".esc_attr($tag->slug)."'>".esc_html($tag->name)."</a>";
																								}
																							}
																					  	}
																					  	echo '</p>';
																					}
																    			break;
																    			case "categories":
																    				$postcats = get_the_category();
																					if ($postcats) {
			
																						$first = true;
			
																						echo '<p data-rel="metas-categories">';
			
																						foreach($postcats as $cat) {
																							if ($cat->name != "uncategorized"){
																								if ($first){
																									echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																									$first = false;
																								} else {
																									echo "<a href='".esc_url( home_url( '/' ) )."?cat=".esc_attr($cat->term_id)."'>".esc_html($cat->name)."</a>"; 
																								}	
																							}
																					  	}
																					  	echo '</p>';
																					}
																    			break;
																    			case "customtext":
																    				if (!$firstMeta && strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0 ){
																		    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																	    			} else {
																		    			if (strlen(wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) )) > 0){
																			    			echo '<p data-rel="metas-customtext">'.wp_kses_post( get_post_meta( $postid , 'upper_single_meta_custom_text_value' , true ) ).'</p>';
																			    			$firstMeta = false;
																		    			}
																	    			}
																    			break;
															    			}
														    			}
													    			}
												    			?>
												    			</div>
												    		</div>
															<?php
														}
													?>
														<div class="the_title">
															<h2 style="<?php echo esc_attr("color: #$titlecolor; font-size: $titlesize; font-family: '{$titlefont[0]}'; font-weight: {$titlefont[1]};");?>" >
																<a href="<?php the_permalink(); ?>"><?php wp_kses_post(the_title()); ?></a>
															</h2>
														</div>
													</div>
													<?php
									    		break;
								    		}
										    ?>
									</div>
								</div>
						    </article>
						    <?php
					    }
					    
					    ?>
			    	</div>
			    	<?php
				    	
				    $larch_inline_script = '
						jQuery(document).ready(function(){
							"use strict";
							var forceGutter = 10; // change to false to return to the normal behavior.
							(function(e){"use strict";e.Isotope.prototype._getMasonryGutterColumns=function(){var e=this.options.masonry&&this.options.masonry.gutterWidth||0;var t=this.element.width();this.masonry.columnWidth=this.options.masonry&&this.options.masonry.columnWidth||this.$filteredAtoms.outerWidth(true)||t;this.masonry.columnWidth+=e;this.masonry.cols=Math.floor((t+e)/this.masonry.columnWidth);this.masonry.cols=Math.max(this.masonry.cols,1)};e.Isotope.prototype._masonryReset=function(){this.masonry={};this._getMasonryGutterColumns();var e=this.masonry.cols;this.masonry.colYs=[];while(e--){this.masonry.colYs.push(0)}};e.Isotope.prototype._masonryResizeChanged=function(){var e=this.masonry.cols;this._getMasonryGutterColumns();return this.masonry.cols!==e};e(document).ready(function(){"use strict";var t=e(".journal");var n=0;var r=0;var i=function(){var e=parseInt(t.data("columns"));var i=t.data("gutterSpace");var s=t.width();var o=0;if(isNaN(i)){i=.02}else if(i>.05||i<0){i=.02}if(s<568){e=1}else if(s<768){e-=2}else if(s<991){e-=1;if(e<2){e=2}}if(e<1){e=1}r=forceGutter!=false ? forceGutter : Math.floor(s*i);var u=r*(e-1);var a=s-u;n=Math.floor(a/e);o=r;if(1==e){o=20}t.children(".journal-post").css({width:n+"px",marginBottom:o+"px"})};i();window.iso = t.isotope({itemSelector:".journal-post",resizable:false,masonry:{columnWidth:n,gutterWidth:r}});    t.imagesLoaded(function(){i(); t.isotope({itemSelector:".journal-post",resizable:true,masonry:{columnWidth:n,gutterWidth:r}})});e(window).smartresize(function(){i();t.isotope({masonry:{columnWidth:n,gutterWidth:r}})});var s=e(".wc-shortcodes-filtering .wc-shortcodes-term");s.on("click",function(i){i.preventDefault();s.removeClass("wc-shortcodes-term-active");e(this).addClass("wc-shortcodes-term-active");var o=e(this).attr("data-filter"); t.isotope({filter:o,masonry:{columnWidth:n,gutterWidth:r} });return false})})})(jQuery);
							
							jQuery(".flexslider").flexslider({
								animation: "'. esc_js( get_option('larch_posts_flex_transition_masonry', 'fade') ) .'",
								slideshow: '. esc_js( (get_option('larch_posts_flex_autoplay_masonry','on') == 'on') ? "true" : "false" ) .',
								slideshowSpeed: '. esc_js( get_option('larch_posts_flex_slide_duration_masonry','5500') ) .',
								animationDuration: '. esc_js( get_option('larch_posts_flex_transition_duration_masonry','500') ) .',
								directionNav: '. esc_js( (get_option('larch_posts_flex_navigation_masonry','on') == 'on') ? "true" : "false" ) .',
								controlNav: '. esc_js( (get_option('larch_posts_flex_controls_masonry','on') == 'on') ? "true" : "false" ) .',
								smootheHeight:false,
								start: function(slider) {
								  slider.removeClass("loading").css("overflow","").find(".slides").css("max-height", "'.esc_js( intval(get_option('larch_posts_flex_height_masonry','400')) ).'px" );
								}
							});
							
							jQuery(".gallery_container").css("opacity",0);
							
							jQuery(".not-in-view").waypoint({
								handler: function(event, direction){
									var element = this.element ? this.element : this;
									jQuery(element).removeClass("not-in-view");
								},
								offset: "80%"
							});
						});
						jQuery(window).load(function(){ jQuery(window).resize(); jQuery(".gallery_container").css("opacity",1); jQuery(".post-listing.larch-animate-posts").removeClass("not-ready"); });
					';
					wp_add_inline_script('larch-global', $larch_inline_script, 'after');
		    	}
	    	?> 
					
			<div class="navigation">
				<?php
					$larch_reading_option = get_option('larch_blog_reading_type');
					if ($larch_reading_option != "paged" && $larch_reading_option != "dropdown"){ 
						$larch_the_query = new WP_Query();
						if (function_exists('icl_t')){
							next_posts_link('<div class="next-posts">&laquo; ' . sprintf(esc_html__("%s", "larch"), icl_t( 'larch', 'Previous results', get_option('larch_previous_results'))).'</div>', $larch_the_query->max_num_pages);
							previous_posts_link('<div class="prev-posts">'.sprintf(esc_html__("%s", "larch"), icl_t( 'larch', 'Next results', get_option('larch_next_results'))) . ' &raquo;</div>', $larch_the_query->max_num_pages);
						} else {
							next_posts_link('<div class="next-posts">&laquo; ' . sprintf(esc_html__("%s", "larch"), get_option("larch_previous_results")).'</div>', $larch_the_query->max_num_pages);
							previous_posts_link('<div class="prev-posts">'.sprintf(esc_html__("%s", "larch"), get_option("larch_next_results")) . ' &raquo;</div>', $larch_the_query->max_num_pages);
						}
					} else { 
						larch_wp_pagenavi();
					}
				?>
			</div>

		<?php
			
			if ($larch_animate_posts){
			    $larch_inline_script .= '
			    	jQuery(".not-in-view").waypoint({
						handler: function(event, direction){
							var element = this.element ? this.element : this;
							jQuery(element).removeClass("not-in-view");
						},
						offset: "80%"
					});
			    ';
		    }
			
			if ($larch_inline_script != "") wp_add_inline_script( 'larch-global', $larch_inline_script, 'after' );
			
		} else echo "<br/><br/><h2>There are no posts in this archive.</h2><br/><br/>";
	}
		
	?>

	<div class="clear"></div>
			
<?php get_footer(); ?>