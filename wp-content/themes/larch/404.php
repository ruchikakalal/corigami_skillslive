<?php get_header(); larch_print_menu(); $larch_color_code = get_option("larch_style_color"); ?>
	<div class="notfounderrorbg">
	<div class="container">
		<div class="entry-header">
			<div class="error-c">
				<img src="<?php echo esc_url(get_template_directory_uri() . "/images/404errorimg.png");?>" title="404"/>
				<br>
				<h1 class="heading-error"><?php
					if (function_exists('icl_t')){
						wp_kses_post(printf(esc_html__( "%s", "larch" ), stripslashes_from_strings_only(icl_t( 'larch', 'Oops! There is nothing here...', get_option('larch_404_heading')))));
					} else {
						wp_kses_post(printf(esc_html__( "%s", "larch" ), stripslashes_from_strings_only(get_option('larch_404_heading'))));
					}
				?></h1>
							
				<p class="text-error"><?php
					if (function_exists('icl_t')){
						wp_kses_post(printf(esc_html__( "%s", "larch" ), stripslashes_from_strings_only(icl_t( 'larch', "It seems we can't find what you're looking for. Perhaps searching one of the links in the above menu, can help.", get_option('larch_404_text')))));
					} else {
						wp_kses_post(printf(esc_html__( "%s", "larch" ), stripslashes_from_strings_only(get_option('larch_404_text'))));
					}
				?></p>
				
				<a href="<?php echo esc_url(home_url("/")); ?>" class="errorbutton"><?php
					if (function_exists('icl_t')){
						printf(esc_html__("%s","larch"), icl_t( 'larch', 'GO TO HOMEPAGE', get_option('larch_404_button_text')));
					} else {
						printf(esc_html__("%s","larch"), get_option('larch_404_button_text'));
					}
				?></a>
			</div>
			
		</div>
	</div>
	</div>
</div><!-- fechar div main -->
<?php
$larch_styleColor = "#".esc_html(get_option("larch_style_color"));
$larch_bodyLayoutType = get_option("larch_body_layout_type");
$larch_headerType = get_option("larch_header_type");
?>
<div id="bodyLayoutType" class="larch_helper_div"><?php echo esc_html($larch_bodyLayoutType); ?></div>
<div id="headerType" class="larch_helper_div"><?php echo esc_html($larch_headerType); ?></div>
<?php 
	if (get_option("larch_body_shadow") == "on"){
		?>
			<div id="bodyShadowColor" class="larch_helper_div"><?php echo "#".esc_html(get_option("larch_body_shadow_color")); ?></div>
		<?php
	}
	$larch_headerStyleType = get_option("larch_header_style_type");
?>
<div id="templatepath" class="larch_helper_div"><?php echo esc_url(get_template_directory_uri())."/"; ?></div>
<div id="homeURL" class="larch_helper_div"><?php echo esc_url(home_url("/")); ?>/</div>
<div id="styleColor" class="larch_helper_div"><?php echo "#".esc_html(get_option("larch_style_color")); ?></div>	
<div id="headerStyleType" class="larch_helper_div"><?php echo esc_html($larch_headerStyleType); ?></div>
<div class="larch_helper_div" id="larch_no_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'No more posts to load.', get_option('larch_no_more_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_no_more_posts_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_load_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'Load More Posts', get_option('larch_load_more_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_load_more_posts_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_loading_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'Loading posts.', get_option('larch_loading_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_loading_posts_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_links_color_hover"><?php echo esc_html(str_replace("__USE_THEME_MAIN_COLOR__", $larch_color_code, get_option("larch_links_color_hover"))); ?></div>
<div class="larch_helper_div" id="larch_enable_images_magnifier"><?php echo esc_html(get_option('larch_enable_images_magnifier')); ?></div>
<div class="larch_helper_div" id="larch_thumbnails_hover_option"><?php echo esc_html(get_option('larch_thumbnails_hover_option')); ?></div>
<div id="homePATH" class="larch_helper_div"><?php echo ABSPATH; ?></div>
<div class="larch_helper_div" id="larch_menu_color">#<?php echo esc_html(get_option("larch_menu_color")); ?></div>
<div class="larch_helper_div" id="larch_fixed_menu"><?php echo esc_html(get_option("larch_fixed_menu")); ?></div>
<div class="larch_helper_div" id="larch_thumbnails_effect"><?php if (get_option("larch_animate_thumbnails") == "on") echo esc_html(get_option("larch_thumbnails_effect")); else echo "none"; ?></div>
<div class="larch_helper_div loadinger">
	<img alt="<?php esc_attr_e('loading','larch'); ?>" src="<?php echo esc_url(get_template_directory_uri()). '/images/ajx_loading.gif' ?>">
</div>
<div class="larch_helper_div" id="permalink_structure"><?php echo esc_html(get_option('permalink_structure')); ?></div>
<div class="larch_helper_div" id="headerstyle3_menucolor">#<?php echo esc_html(get_option("larch_menu_color")); ?></div>
<div class="larch_helper_div" id="disable_responsive_layout"><?php echo esc_html(get_option('larch_disable_responsive')); ?></div>
<div class="larch_helper_div" id="filters-dropdown-sort"><?php esc_html_e('Sort Gallery','larch'); ?></div>
<div class="larch_helper_div" id="templatepath"><?php echo esc_url(get_template_directory_uri()); ?></div>
<div class="larch_helper_div" id="searcheverything"><?php echo esc_html(get_option("larch_enable_search_everything")); ?></div>
<div class="larch_helper_div" id="larch_header_shrink"><?php if (get_option('larch_fixed_menu') == 'on'){if (get_option('larch_header_after_scroll') == 'on'){if (get_option('larch_header_shrink_effect') == 'on'){echo "yes";} else echo "no";}} ?></div>
<div class="larch_helper_div" id="larch_header_after_scroll"><?php if (get_option('larch_fixed_menu') == 'on'){if (get_option('larch_header_after_scroll') == 'on'){echo "yes";} else echo "no";} ?></div>
<div class="larch_helper_div" id="larch_grayscale_effect"><?php echo esc_html(get_option("larch_enable_grayscale")); ?></div>
<div class="larch_helper_div" id="larch_enable_ajax_search"><?php echo esc_html(get_option("larch_enable_ajax_search")); ?></div>
<div class="larch_helper_div" id="larch_menu_add_border"><?php echo esc_html(get_option("larch_menu_add_border")); ?></div>
<div class="larch_helper_div" id="larch_newsletter_input_text"><?php
	if (function_exists('icl_t')){
		echo esc_html(icl_t( 'larch', 'Enter your email here', get_option('larch_newsletter_input_text')));
	} else {
		echo esc_html(get_option('larch_newsletter_input_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_content_to_the_top">
	<?php 
		if (is_singular() && get_post_meta(get_the_ID(), 'larch_enable_custom_header_options_value', true)=='yes') echo esc_html(get_post_meta(get_the_ID(), 'larch_content_to_the_top_value', true));
		else echo esc_html(get_option('larch_content_to_the_top')); 
	?>
</div>
<div class="larch_helper_div" id="larch_update_section_titles"><?php echo esc_html(get_option('larch_update_section_titles')); ?></div>
<?php
	if (function_exists('icl_t')){
		?>
		<div class="larch_helper_div" id="larch_wpml_current_lang"><?php echo ICL_LANGUAGE_CODE; ?></div>
		<?php
	}
?>
<?php 
	$standardfonts = array('Arial','Arial Black','Helvetica','Helvetica Neue','Courier New','Georgia','Impact','Lucida Sans Unicode','Times New Roman', 'Trebuchet MS','Verdana','');
	$importfonts = "";
	$larch_import_fonts = larch_get_import_fonts();

	foreach ($larch_import_fonts as $font){
		if (!in_array($font,$standardfonts)){
			$font = str_replace(" ","+",str_replace("|",":",$font));
			if ($importfonts=="") $importfonts .= $font;
			else {
				if (strpos($importfonts, $font) === false)
					$importfonts .= "|{$font}";
			}
		}
	}

	if ($importfonts!="") {
		$larch_import_fonts = $importfonts;
		larch_set_import_fonts($larch_import_fonts);
		larch_google_fonts_scripts();
	}
?>

<?php get_footer(); ?>