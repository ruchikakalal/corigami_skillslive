<?php

add_action ('wp_ajax_call_upper_search_ajax', 'upper_search_ajax') ;
add_action ('wp_ajax_nopriv_call_upper_search_ajax', 'upper_search_ajax') ;

function upper_search_ajax(){
	
	if (!isset($_POST)) wp_send_json_error('no direct access');

	//WPML force current language
	if (isset($_POST['wpml_lang']) && $_POST['wpml_lang']!="" && isset($sitepress)){
		$sitepress->switch_lang($_POST['wpml_lang'], true);
	}
	
	$results = "";
	
	if ($_POST['se'] == "on"){
		$args = array(
			'showposts' => -1,
			'post_status' => 'publish',
			's' => $_POST['query']
		);
	} else {
		$args = array(
			'showposts' => -1,
			'post_status' => 'publish',
			'post_type' => 'post',
			's' => $_POST['query']
		);
	}
    
    $larch_the_query = new WP_Query( $args );
    
	if ( $larch_the_query->have_posts() ) {
		$first = true;
		$selected = "";
		while ( $larch_the_query->have_posts() ) {
			$larch_the_query->the_post();

			if ($first)	{
				$first = false;
				$selected = "selected";
			} else {
				$selected = "";
			}
			$results .= "<li class='".esc_attr($selected)."'><a href='".esc_url(get_permalink())."'><strong>".wp_kses_post(get_the_title())."</strong><span>";
			if (get_option("larch_search_show_author") == "on") {
				if (function_exists('icl_t')){
					$results .=", ".sprintf(esc_html__("%s","larch"), icl_t( 'larch', 'by', get_option('larch_by_text')))." ".get_the_author();
				} else {
					$results .=", ".sprintf(esc_html__("%s","larch"), get_option("larch_by_text"))." ".get_the_author();
				}
			}
			if (get_option("larch_search_show_date") == "on")
			$results .= ", ".get_the_date("M")." ".get_the_date("d").", ".get_the_date("Y");
			if (get_option("larch_search_show_categories") == "on"){
				$categories = get_the_category();
				$firstcat = true;
				if ($categories){
					foreach($categories as $category) {
						if ($category->term_id != 1){
							if ($firstcat){
								if (function_exists('icl_t')){
									$results .= ", ".sprintf(esc_html__("%s","larch"), icl_t( 'larch', 'in', get_option('larch_in_text')))." <i>";
								} else {
									$results .= ", ".sprintf(esc_html__("%s","larch"), get_option("larch_in_text"))." <i>";
								}
								$firstcat = false;
								$results .= $category->cat_name;
							} else {
								$results .= ", ".$category->cat_name;
							}	
						}
					}
				}
				if (!$firstcat) $results .= "</i>";
			}
			if (get_option("larch_search_show_tags") == "on"){
				$tags = get_the_tags();
				$firsttag = true;
				if ($tags){
					foreach($tags as $tag) {
						if ($firsttag){
							if (function_exists('icl_t')){
								$results .= ", ".sprintf(esc_html__("%s","larch"), icl_t( 'larch', 'in', get_option('larch_in_text')))." <i>";
							} else {
								$results .= ", ".sprintf(esc_html__("%s","larch"), get_option("larch_in_text"))." <i>";
							}
							$firsttag = false;
							$results .= $tag->name;
						} else {
							$results .= ", ".$tag->name;
						}
					}
				}
				if (!$firsttag) $results .= "</i>";
			}
			$results .= "</span></a></li>";
		}
	} else {
		if (function_exists('icl_t')){
			$results .= "<li><a>".sprintf(esc_html__("%s","larch"), icl_t( 'larch', 'No results found.', get_option('larch_no_results_text')))."</a></li>";
		} else {
			$results .= "<li><a>".sprintf(esc_html__("%s","larch"), get_option("larch_no_results_text"))."</a></li>";
		}
	}

	echo wp_send_json_success($results);
	
	wp_die();
}

?>