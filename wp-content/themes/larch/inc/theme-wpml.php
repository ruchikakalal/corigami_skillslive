<?php

//LANGUAGES TAB
icl_register_string( 'larch', 'Home', get_option('larch_breadcrumbs_home_text'));
icl_register_string( 'larch', 'You are here:', get_option('larch_you_are_here'));
icl_register_string( 'larch', 'Latest from <span class=text_color>Twitter</span>', get_option('larch_latest_for_twitter'));
icl_register_string( 'larch', 'Open Menu', get_option('larch_open_menu'));

icl_register_string( 'larch', 'Next Project', get_option('larch_next_single_proj'));
icl_register_string( 'larch', 'Previous Project', get_option('larch_prev_single_proj'));
icl_register_string( 'larch', 'SHARE THIS PROJECT', get_option('larch_share_proj_text'));

icl_register_string( 'larch', 'read more', get_option('larch_read_more'));
icl_register_string( 'larch', 'Previous posts', get_option('larch_previous_text'));
icl_register_string( 'larch', 'Next posts', get_option('larch_next_text'));
icl_register_string( 'larch', 'Previous post', get_option('larch_single_previous_text'));
icl_register_string( 'larch', 'Next post', get_option('larch_single_next_text'));
icl_register_string( 'larch', 'by', get_option('larch_by_text'));
icl_register_string( 'larch', 'in', get_option('larch_in_text'));
icl_register_string( 'larch', 'Tags', get_option('larch_tags_text'));
icl_register_string( 'larch', 'Load More Posts', get_option('larch_load_more_posts_text'));
icl_register_string( 'larch', 'No more posts to load.', get_option('larch_no_more_posts_text'));
icl_register_string( 'larch', 'Loading posts.', get_option('larch_loading_posts_text'));
icl_register_string( 'larch', 'SHARE THIS', get_option('larch_share_post_text'));

icl_register_string( 'larch', 'No comments', get_option('larch_no_comments_text'));
icl_register_string( 'larch', 'comment', get_option('larch_comment_text'));
icl_register_string( 'larch', 'comments', get_option('larch_comments_text'));

icl_register_string( 'larch', 'Type your search and hit enter...', get_option('larch_search_box_text'));
icl_register_string( 'larch', 'Search results for', get_option('larch_search_results_text'));
icl_register_string( 'larch', 'No results found.', get_option('larch_no_results_text'));
icl_register_string( 'larch', 'Next results', get_option('larch_next_results'));
icl_register_string( 'larch', 'Previous results', get_option('larch_previous_results'));

icl_register_string( 'larch', 'Name', get_option('larch_cf_name'));
icl_register_string( 'larch', 'Email', get_option('larch_cf_email'));
icl_register_string( 'larch', 'Message', get_option('larch_cf_message'));
icl_register_string( 'larch', 'Send', get_option('larch_cf_send'));

icl_register_string( 'larch', 'Follow us on Twitter', get_option('larch_twitter_follow_us'));
icl_register_string( 'larch', 'I was looking at ', get_option('larch_twitter_pre_tweet'));

icl_register_string( 'larch', 'Oops! There is nothing here...', get_option('larch_404_heading'));
icl_register_string( 'larch', "It seems we can't find what you're looking for. Perhaps searching one of the links in the above menu, can help.", get_option('larch_404_text'));
icl_register_string( 'larch', 'GO TO HOMEPAGE', get_option('larch_404_button_text'));

//NEWSLETTER TAB
icl_register_string( 'larch', 'Subscribe our <span class=text_color>Newsletter</span>', get_option('larch_newsletter_text'));
icl_register_string( 'larch', 'Subscribe to our newsletter to receive news, cool free stuff updates and new released products (no spam!)', get_option('larch_newsletter_stext'));
icl_register_string( 'larch', 'Enter your email here', get_option('larch_newsletter_input_text'));

?>
