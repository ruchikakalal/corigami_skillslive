<?php

function larch_print_woocommerce_button(){
	global $woocommerce;
	if (isset($woocommerce) && get_option("larch_woocommerce_cart") == "on"){ ?>
		<div class="larch_dynamic_shopping_bag">
			<div class="larch_little_shopping_bag_wrapper">
				<div class="larch_little_shopping_bag">
					<div class="title">
						<div class="icon dripicons-cart"></div>
					</div>
					<div class="overview">
						<span class="minicart_items"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'larch'), $woocommerce->cart->cart_contents_count); ?></span>
					</div>
				</div>
				<div class="larch_minicart_wrapper">
					<div class="larch_minicart">
					<?php
						if (sizeof($woocommerce->cart->cart_contents)>0){
							echo '<ul class="cart_list">';
							foreach ($woocommerce->cart->cart_contents as $cart_item_key => $cart_item){
								$_product = $cart_item['data'];
								if ($_product->exists() && $cart_item['quantity']>0){
									echo '<li class="cart_list_product">';
										echo '<a class="cart_list_product_img" href="'.esc_url(get_permalink($cart_item['product_id'])).'">' . $_product->get_image().'</a>';
										echo '<div class="cart_list_product_title">';
											$larch_product_title = $_product->get_title();
											$larch_short_product_title = (strlen($larch_product_title) > 28) ? substr($larch_product_title, 0, 25) . '...' : $larch_product_title;
											echo '<a href="'.esc_url(get_permalink($cart_item['product_id'])).'">' . apply_filters('woocommerce_cart_widget_product_title', $larch_short_product_title, $_product) . '</a>';
											echo '<div class="cart_list_product_quantity">'.$cart_item['quantity'].'x</div>';
										echo '</div>';
										echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s">x</a>', esc_url( $woocommerce->cart->get_remove_url( $cart_item_key ) ), esc_html__('Remove this item', 'larch') ), $cart_item_key );
										echo '<div class="cart_list_product_price">'.woocommerce_price($_product->get_price()).'</div>';
										echo '<div class="clr"></div>';
									echo '</li>';
								}
							}
							echo '</ul>';
							?>
							<div class="minicart_total_checkout">
							<?php esc_html_e('Cart subtotal', 'larch'); ?><span><?php echo wp_kses_post($woocommerce->cart->get_cart_total()); ?></span>
							</div>
							<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" class="button larch_minicart_cart_but"><?php esc_html_e('View Bag', 'larch'); ?></a>
							<a href="<?php echo esc_url( $woocommerce->cart->get_checkout_url() ); ?>" class="button larch_minicart_checkout_but"><?php esc_html_e('Checkout', 'larch'); ?></a>
							<?php
						} else {
							echo '<ul class="cart_list"><li class="empty">'.esc_html__('No products in the cart.','larch').'</li></ul>';
						}
						?>
					</div>
				</div>
			</div>
			<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" class="larch_little_shopping_bag_wrapper_mobiles"><span><?php echo wp_kses_post($woocommerce->cart->cart_contents_count); ?></span></a>
		</div>
	<?php
	}
}

function larch_woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;
	if (isset($woocommerce) && get_option("larch_woocommerce_cart") == "on"){
		$larch_woo_output = '
		<div class="larch_dynamic_shopping_bag" style="display:table-cell;">
			<div class="larch_little_shopping_bag_wrapper">
				<div class="larch_little_shopping_bag">
					<div class="title">
						<div class="icon dripicons-cart"></div>
					</div>
					<div class="overview">
						<span class="minicart_items">'.sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'larch'), $woocommerce->cart->cart_contents_count).'</span>
					</div>
				</div>
				<div class="larch_minicart_wrapper">
					<div class="larch_minicart">';
						if (sizeof($woocommerce->cart->cart_contents)>0){
							$larch_woo_output .= '<ul class="cart_list">';
							foreach ($woocommerce->cart->cart_contents as $cart_item_key => $cart_item){
								$_product = $cart_item['data'];
								if ($_product->exists() && $cart_item['quantity']>0){
									$larch_woo_output .= '<li class="cart_list_product">';
										$larch_woo_output .= '<a class="cart_list_product_img" href="'.esc_url(get_permalink($cart_item['product_id'])).'">' . $_product->get_image().'</a>';
										$larch_woo_output .= '<div class="cart_list_product_title">';
											$larch_product_title = $_product->get_title();
											$larch_short_product_title = (strlen($larch_product_title) > 28) ? substr($larch_product_title, 0, 25) . '...' : $larch_product_title;
											$larch_woo_output .= '<a href="'.esc_url(get_permalink($cart_item['product_id'])).'">' . apply_filters('woocommerce_cart_widget_product_title', $larch_short_product_title, $_product) . '</a>';
											$larch_woo_output .= '<div class="cart_list_product_quantity">'.$cart_item['quantity'].'x</div>';
										$larch_woo_output .= '</div>';
										$larch_woo_output .= apply_filters( 'woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s">x</a>', esc_url( $woocommerce->cart->get_remove_url( $cart_item_key ) ), esc_html__('Remove this item', 'larch') ), $cart_item_key );
										$larch_woo_output .= '<div class="cart_list_product_price">'.woocommerce_price($_product->get_price()).'</div>';
										$larch_woo_output .= '<div class="clr"></div>';
									$larch_woo_output .= '</li>';
								}
							}
							$larch_woo_output .= '</ul>';
							$larch_woo_output .= '
							<div class="minicart_total_checkout">
								'.esc_html__('Cart subtotal', 'larch').'<span>'.wp_kses_post($woocommerce->cart->get_cart_total()).'</span>
						</div>
						<a href="'.esc_url( $woocommerce->cart->get_cart_url() ).'" class="button larch_minicart_cart_but">'.esc_html__('View Bag', 'larch').'</a>
						<a href="'.esc_url( $woocommerce->cart->get_checkout_url() ).'" class="button larch_minicart_checkout_but">'. esc_html__('Checkout', 'larch').'</a>';
						} else {
							$larch_woo_output .= '<ul class="cart_list"><li class="empty">'.esc_html__('No products in the cart.','larch').'</li></ul>';
						}
						$larch_woo_output .= '
					</div>
				</div>
			</div>
			<a href="'.esc_url( $woocommerce->cart->get_cart_url() ).'" class="larch_little_shopping_bag_wrapper_mobiles"><span>'. wp_kses_post($woocommerce->cart->cart_contents_count).'</span></a>
		</div>';
		$fragments['div.larch_dynamic_shopping_bag'] = $larch_woo_output;
		return $fragments;
	} else return "";
}

?>