<?php
	
function larch_print_menu($ispagephp = true, $isfirstpage = false){
	global $larch_header_bgstyle_pre, $larch_header_bgstyle_after;
	$header_shrink = "";
	if (get_option('larch_fixed_menu') == 'on'){
		if (get_option('larch_header_after_scroll') == 'on'){
			if (get_option('larch_header_shrink_effect') == 'on'){
				$header_shrink = " header_shrink";
			}
		}
	}
	$header_after_scroll = false;
	if (get_option('larch_fixed_menu') == 'on'){
		if (get_option('larch_header_after_scroll') == 'on'){
			$header_after_scroll = true;
		}
	}
	$typeofheader = get_option("larch_header_style_type");
	$larch_underlining_effect = get_option("larch_menu_underline_".$larch_header_bgstyle_pre,"on") == "on" ? " larch-underlining " : "";

	$larch_content_at_top = (get_post_meta(get_the_ID(),'larch_enable_custom_header_options_value', true) == "yes") ? get_post_meta(get_the_ID(),'larch_content_to_the_top_value', true) : get_option('larch_content_to_the_top');
	$larch_content_at_top = ($larch_content_at_top == "on") ? false : true;
	
	$larch_header_hide_on_start = (get_post_meta(get_the_ID(),'larch_enable_custom_header_options_value', true) == "yes") ? get_post_meta(get_the_ID(),'larch_header_hide_on_start_value', true) : get_option('larch_header_hide_on_start');
	$larch_header_hide_on_start = ($larch_header_hide_on_start == "on") ? true : false;
	?>
	
	<header class="header-init navbar navbar-default navbar-fixed-top <?php echo esc_attr($typeofheader . $larch_underlining_effect); ?> <?php if (get_option('larch_fixed_menu') == 'off') echo " header_not_fixed"; else if ($larch_header_hide_on_start) echo " hide-on-start"; ?><?php if (get_option("larch_header_full_width") == "on") echo " header-full-width"; ?><?php if (get_option("larch_header_full_width") == "off") echo " header-with-container"; ?><?php if (get_option("larch_header_menu_itens_style") == "rounded") echo " menu-rounded"; ?><?php if (get_option("larch_header_menu_itens_style") == "simple") echo " menu-simple"; ?><?php if (get_option("larch_header_menu_itens_style") == "square") echo " menu-square"; ?><?php echo " ".esc_attr($larch_header_bgstyle_pre); ?><?php if ($larch_content_at_top) echo " header-init-force-fixed "; ?>" data-rel="<?php echo esc_attr($larch_header_bgstyle_pre."|".$larch_header_bgstyle_after); ?>">
		
		<?php
		if (get_option("larch_info_above_menu") == "on"){
			?>
			<div class="top-bar">
				<div class="top-bar-bg">
					<div class="<?php if (get_option("larch_header_full_width") == "off") echo "container"; ?> clearfix">
						<div class="slidedown">
						    <div class="col-xs-12 col-sm-12">
							<?php
								
								/* wpml menu */
								if (get_option("larch_wpml_menu_widget") == "on") {
									if (function_exists('icl_object_id')) { ?>
										<div class="menu_wpml_widget">	
											<?php if (function_exists('icl_language_selector')) do_action('icl_language_selector'); else if (function_exists('wpml_add_language_selector')) do_action('wpml_add_language_selector'); ?>
										</div>
									<?php 
									}
								}
								/* social icons */
								if (get_option("larch_enable_socials") == "on"){
									?>
										<div class="social-icons-fa">
									        <ul>
											<?php
												$icons = array(array("facebook","Facebook"),array("twitter","Twitter"),array("tumblr","Tumblr"),array("stumbleupon","Stumble Upon"),array("flickr","Flickr"),array("blogger","Blogger"),array("linkedin","LinkedIn"),array("delicious","Delicious"),array("skype","Skype"),array("digg","Digg"),array("google-plus","Google+"),array("vimeo-square","Vimeo"),array("deviantart","DeviantArt"),array("behance","Behance"),array("instagram","Instagram"),array("wordpress","WordPress"),array("youtube","Youtube"),array("reddit","Reddit"),array("rss","RSS"),array("soundcloud","SoundCloud"),array("pinterest","Pinterest"),array("dribbble","Dribbble"));
												foreach ($icons as $i){
													if (is_string(get_option("larch_icon-".$i[0])) && get_option("larch_icon-".$i[0]) != ""){
													?>
													<li>
														<a href="<?php echo esc_url(get_option("larch_icon-".$i[0])); ?>" target="_blank" class="<?php echo esc_attr(strtolower($i[0])); ?>" title="<?php echo esc_attr($i[1]); ?>"><i class="fa fa-<?php echo esc_attr(strtolower($i[0])); ?>"></i></a>
													</li>
													<?php
													}
												}
											?>
										    </ul>
										</div>
									<?php	
								}
								/* company infos */
								if ( get_option("larch_telephone_menu") != "" || get_option("larch_email_menu") != "" || get_option("larch_address_menu") != "" || get_option("larch_text_field_menu") != "" ){
									?>
									<ul class="phone-mail">
										<?php if ( is_string(get_option("larch_telephone_menu")) && get_option("larch_telephone_menu") != "" ){ ?>
											<li><div class="icon dripicons-phone"></div><?php printf(esc_html__("%s", "larch"), get_option("larch_telephone_menu")); ?></li>
										<?php } ?>
										<?php if ( is_string(get_option("larch_email_menu")) && get_option("larch_email_menu") != "" ){ ?>
											<li><div class="icon dripicons-message"></div><a href="mailto:<?php echo esc_attr(get_option("larch_email_menu")); ?>"><?php echo esc_html(get_option("larch_email_menu")); ?></a></li>
										<?php } ?>
										<?php if ( is_string(get_option("larch_address_menu")) && get_option("larch_address_menu") != "" ){ ?>
											<li><div class="icon dripicons-pin"></div><?php echo wp_kses_post(get_option("larch_address_menu")); ?></li>
										<?php } ?>
										<?php if ( is_string(get_option("larch_text_field_menu")) && get_option("larch_text_field_menu") != "" ){ ?>
											<li class="text_field"><?php echo wp_kses_post(get_option("larch_text_field_menu")); ?></li>
										<?php } ?>
									</ul>
									<?php
								}
								
								
								/* topbar menu */
								if (get_option("larch_top_bar_menu") == "on"){
									?>
									<div class="top-bar-menu">
										<?php wp_nav_menu( array( 'theme_location' => 'topbarnav', 'container' => false, 'menu_class' => 'sf-menu', 'menu_id' => 'menu_top_bar' )); ?>
									</div>
									<?php
								}
							?>
							</div>
						</div>
					</div>
				</div>
				<a href="#" class="down-button"><i class="fa fa-plus"></i></a><!-- this appear on small devices -->
			</div>
			<?php
		}
		
		
		?>
		
		<div class="nav-container <?php if (get_option("larch_header_full_width") == "off") echo " container"; ?>">
	    	<div class="navbar-header">
		    	
				<a class="navbar-brand nav-to" href="<?php echo esc_url(home_url("/")); ?>" tabindex="-1">
	        	<?php 
					$larch_header_style_pre = $larch_header_bgstyle_pre == 'dark' ? 'light' : 'dark';
					$larch_header_style_after = $larch_header_bgstyle_after == 'dark' ? 'light' : 'dark';
					
					$alone = true;
    				if (get_option("larch_logo_retina_image_url_".$larch_header_style_pre) != ""){
	    				$alone = false;
    				}
					?>
					<img class="logo_normal <?php if (!$alone) echo "notalone"; ?>" src="<?php echo esc_url(get_option("larch_logo_image_url_".$larch_header_style_pre)); ?>" alt="<?php esc_attr_e("", "larch"); ?>" title="<?php esc_html_e("", "larch"); ?>">
    					
    				<?php 
    				if (get_option("larch_logo_retina_image_url_".$larch_header_style_pre) != ""){
    				?>
	    				<img class="logo_retina" src="<?php echo esc_url(get_option("larch_logo_retina_image_url_".$larch_header_style_pre)); ?>" alt="<?php esc_attr_e("", "larch"); ?>" title="<?php esc_html_e("", "larch"); ?>">
    				<?php
					}
					/* larch_header_after_scroll option */
	    			if ($header_after_scroll || $larch_header_hide_on_start){
		    			$alone = true;
	    				if (get_option("larch_logo_retina_image_url_".$larch_header_style_after) != ""){
		    				$alone = false;
	    				}
    					?>
    					<img class="logo_normal logo_after_scroll <?php if (!$alone) echo "notalone"; ?>" alt="<?php esc_attr_e("", "larch"); ?>" title="<?php esc_html_e("", "larch"); ?>" src="<?php echo esc_url(get_option("larch_logo_image_url_".$larch_header_style_after)); ?>">
	    					
	    				<?php 
	    				if (get_option("larch_logo_retina_image_url_".$larch_header_style_after) != ""){
	    				?>
		    				<img class="logo_retina logo_after_scroll" src="<?php echo esc_url(get_option("larch_logo_retina_image_url_".$larch_header_style_after)); ?>" alt="<?php esc_attr_e("", "larch"); ?>" title="<?php esc_html_e("", "larch"); ?>">
	    				<?php
    					}
	    			}
	    		?>
		        </a>
			</div>
			
			<div class="larch_right_header_icons <?php
				if (class_exists( 'WooCommerce' ) && get_option("larch_woocommerce_cart") == "on") echo "with-woocommerce-cart";
			?>">
			
					<div class="header_social_icons <?php if (get_option("larch_social_icons_menu") == "on") echo "with-social-icons"; ?>">
						<?php
							if (get_option("larch_social_icons_menu") == "on" ){
								?>
								<div class="header_social_icons_wrapper">
								<?php
									$icons = array_reverse(array(array("facebook","Facebook"),array("twitter","Twitter"),array("tumblr","Tumblr"),array("stumbleupon","Stumble Upon"),array("flickr","Flickr"),array("linkedin","LinkedIn"),array("delicious","Delicious"),array("skype","Skype"),array("digg","Digg"),array("google-plus","Google+"),array("vimeo-square","Vimeo"),array("deviantart","DeviantArt"),array("behance","Behance"),array("instagram","Instagram"),array("wordpress","WordPress"),array("youtube","Youtube"),array("reddit","Reddit"),array("rss","RSS"),array("soundcloud","SoundCloud"),array("pinterest","Pinterest"),array("dribbble","Dribbble")));
									foreach ($icons as $i){
										if (is_string(get_option("larch_icon-".$i[0])) && get_option("larch_icon-".$i[0]) != ""){
										?>
										<div class="social_container <?php echo esc_attr(strtolower($i[0])); ?>_container" onclick="window.open('<?php echo esc_js(get_option("larch_icon-".$i[0])); ?>', '_blank');">
											<i class="fa fa-<?php echo esc_attr(strtolower($i[0])); ?>"></i>
					                    </div>
								<?php
								}
							}
						?>	
						</div>
						<?php
					}
					

				?>
			</div>
				
				<?php
				
				//search trigger
				if (get_option("larch_enable_search") == "on"){
					?>
					<div class="search_trigger"><div class="icon dripicons-search"></div></div>
					<?php
				}
				?>
				
				<?php larch_print_woocommerce_button();?>
				
				<?php
				if (get_option("larch_sliding_panel") == "on"){
					?>					
						<div class="menu-controls sliderbar-menu-controller" title="Sidebar Menu Controller">
                            <div class="font-icon custom-font-icon">
	                            <div data-icon="c" class="icon"></div>
	                            <div class="icon dripicons-cross"></div>
                            </div>
                        </div>
					<?php
				}
				
			?>
			</div>
					
			
			
			<?php
				if (!$isfirstpage){
					?>
					<div id="dl-menu" class="dl-menuwrapper">
						<div class="dl-trigger-wrapper">
							<button class="dl-trigger"></button>
						</div>
						<?php 
							if ($ispagephp){
								wp_nav_menu( array( 'theme_location' => 'PrimaryNavigation', 'container' => false, 'menu_class' => 'dl-menu', 'walker' => new larch_walker_nav_menu_outsider_mobile, 'fallback_cb' => esc_html__('You need to assign a Menu to the Main Navigation Location.','larch') ) );
							} 
							else {
								global $homes;
								$homes = 0;
								wp_nav_menu( array( 'theme_location' => 'PrimaryNavigation', 'container' => false, 'menu_class' => 'dl-menu', 'walker' => new larch_walker_nav_menu_mobile, 'fallback_cb' => esc_html__('You need to assign a Menu to the Main Navigation Location.','larch') ) );
							} 
						?>
					</div>
					<?php
				}
			?>
			
				<div class="navbar-collapse collapse">
					<?php 
						if (!$isfirstpage){
							if ($ispagephp){
								wp_nav_menu( array( 'theme_location' => 'PrimaryNavigation', 'container' => false, 'menu_class' => 'nav navbar-nav navbar-right', 'walker' => new larch_walker_nav_menu_outsider, 'fallback_cb' => esc_html__('You need to assign a Menu to the Main Navigation Location.','larch') ) );
							} 
							else {
								global $homes;
								$homes = 0;
								wp_nav_menu( array( 'theme_location' => 'PrimaryNavigation', 'container' => false, 'menu_class' => 'nav navbar-nav navbar-right', 'walker' => new larch_walker_nav_menu, 'fallback_cb' => esc_html__('You need to assign a Menu to the Main Navigation Location.','larch') ) );
							}
						}
						if (get_option('larch_header_button') == 'on'){
							?>
							<div class="larch-header-button">
								<a href="<?php echo esc_url(get_option('larch_header_button_link')); ?>" class="nav-to"><?php 
									if (function_exists('icl_t')){
										printf(esc_html__("%s","larch"), icl_t( 'larch', 'Header Button', get_option('larch_header_button_text')));
									} else {
										printf(esc_html__("%s","larch"), get_option("larch_header_button_text"));
									} ?>
								</a>
							</div>
							<?php
						}
					?>
					
				</div>
			
			</div>
		
	</header>
	<?php
}
	
?>