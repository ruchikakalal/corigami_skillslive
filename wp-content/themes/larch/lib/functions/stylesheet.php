<?php
function larch_styles(){

	 if (!is_admin()){
		
		wp_enqueue_style('larch-blog', LARCH_CSS_PATH .'blog.css'); 
	 	wp_enqueue_style('larch-bootstrap', LARCH_CSS_PATH .'bootstrap.css');
		wp_enqueue_style('larch-icons-font', LARCH_CSS_PATH .'icons-font.css');
		wp_enqueue_style('larch-component', LARCH_CSS_PATH .'component.css');
		
		wp_enqueue_style('larch-IE', LARCH_CSS_PATH .'IE.css');	
		wp_style_add_data('larch-IE','conditional','lt IE 9');
		
		wp_enqueue_style('larch-shortcodes', get_template_directory_uri().'/functions/css/shortcodes.css');
		wp_enqueue_style('larch-woo-layout', LARCH_CSS_PATH .'larch-woo-layout.css');
		wp_enqueue_style('larch-woocommerce', LARCH_CSS_PATH .'larch-woocommerce.css');
		wp_enqueue_style('larch-mb-ytplayer', LARCH_CSS_PATH .'mb.YTPlayer.css');
		wp_enqueue_style('larch-retina', LARCH_CSS_PATH .'retina.css');
		
		
	}
}
add_action('wp_enqueue_scripts', 'larch_styles', 1);

?>