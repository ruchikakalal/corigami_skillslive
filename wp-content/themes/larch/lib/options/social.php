<?php
	
	$larch_general_options= array( array(
		"name" => "Social Networks",
		"type" => "title",
		"img" => LARCH_IMAGES_URL."icon_general.png"
	),
	
	( defined('LARCH_PLG_ACTIVE') === true ? 
		array(
			"type" => "open",
			"subtitles"=>array(array("id"=>"social", "name"=>"Social Icons"), array("id"=>"twitter", "name" => "Twitter"), array("id"=>"instagram", "name" => "Instagram"))
		)
		:
		array(
			"type" => "open",
			"subtitles"=>array(array("id"=>"social", "name"=>"Social Icons"))
		)
	),
	
	array(
		"type" => "subtitle",
		"id"=>'instagram'
	),
	
	array(
		"name" => "Display Instagram on Footer?",
		"id" => "larch_display_instagram_footer",
		"type" => "checkbox",
		"std" => 'off',
		"desc" => "Displays your latest Instagram photos on the footer."
	),
	
	array(
		"name" => "Title",
		"id" => "larch_instagram_title",
		"type" => "text",
		"std" => "INSTAGRAM @UPPER"
	),
	
	array(
		"name" => "Username",
		"id" => "larch_instagram_username",
		"type" => "text",
		"std" => "@unsplash"
	),
	
	array(
		"name" => "Number of photos",
		"id" => "larch_instagram_limit",
		"type" => "text",
		"std" => "12"
	),
	
	array(
		"name" => "Open Links in:",
		"id" => "larch_instagram_target",
		"type" => "select",
		"options" => array(array('id'=>'_self', 'name'=>'Current Window (self)'), array('id'=>'_blank','name'=>'New Window (blank)')),
		"std" => '_self'
	),

	array(
		"name" => "Link text",
		"id" => "larch_instagram_link",
		"type" => "text",
		"std" => "Follow me!"
	),
	
	array(
		"type" => "close"
	),
	
	
	/* ------------------------------------------------------------------------*
	 * Top Panel
	 * ------------------------------------------------------------------------*/
	
	array(
		"type" => "subtitle",
		"id"=>'social'
	),
	
	array(
		"type" => "documentation",
		"text" => "<h3>Social Icons</h3>"
	),
	
	array(
		"name" => "Facebook Icon",
		"id" => "larch_icon-facebook",
		"type" => "text",
		"desc" => "Enter full url   ex: http://facebook.com/UpperThemes",
		"std" => ""
	),
	array(
		"name" => "Twitter Icon",
		"id" => "larch_icon-twitter",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Tumblr Icon",
		"id" => "larch_icon-tumblr",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Stumble Upon Icon",
		"id" => "larch_icon-stumbleupon",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Flickr Icon",
		"id" => "larch_icon-flickr",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "LinkedIn Icon",
		"id" => "larch_icon-linkedin",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Delicious Icon",
		"id" => "larch_icon-delicious",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Skype Icon",
		"id" => "larch_icon-skype",
		"type" => "text",
		"desc" => "For a directly call to your Skype, add the following code.  skype:username?call",
		"std" => ""
	),
	array(
		"name" => "Digg Icon",
		"id" => "larch_icon-digg",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Google Icon",
		"id" => "larch_icon-google-plus",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Vimeo Icon",
		"id" => "larch_icon-vimeo-square",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "DeviantArt Icon",
		"id" => "larch_icon-deviantart",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Behance Icon",
		"id" => "larch_icon-behance",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Instagram Icon",
		"id" => "larch_icon-instagram",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Blogger Icon",
		"id" => "larch_icon-blogger",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "WordPress Icon",
		"id" => "larch_icon-wordpress",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Youtube Icon",
		"id" => "larch_icon-youtube",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Reddit Icon",
		"id" => "larch_icon-reddit",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "RSS Icon",
		"id" => "larch_icon-rss",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "SoundCloud Icon",
		"id" => "larch_icon-soundcloud",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Pinterest Icon",
		"id" => "larch_icon-pinterest",
		"type" => "text",
		"std" => ""
	),
	array(
		"name" => "Dribbble Icon",
		"id" => "larch_icon-dribbble",
		"type" => "text",
		"std" => ""
	),

	array(
		"type" => "close"
	),
	
	/* twitter options */ 
	array(
		"type" => "subtitle",
		"id"=>'twitter'
	),
	
	array(
		"type" => "documentation",
		"text" => "<h3>Twitter Scroller</h3>"
	),
	
	array(
		"name" => "Twitter Username",
		"id" => "larch_twitter_username",
		"type" => "text",
		"std" => ''
	),
	
	array(
		"name" => "Twitter App Consumer Key",
		"id" => "twitter_consumer_key",
		"type" => "text"
	),
	
	array(
		"name" => "Twitter App Consumer Secret",
		"id" => "twitter_consumer_secret",
		"type" => "text"
	),
	
	array(
		"name" => "Twitter App Access Token",
		"id" => "twitter_user_token",
		"type" => "text"
	),
	
	array(
		"name" => "Twitter App Access Token Secret",
		"id" => "twitter_user_secret",
		"type" => "text"
	),
	
	array(
		"name" => "Number Tweets",
		"id" => "larch_twitter_number_tweets",
		"type" => "text",
		"std" => '5'
	),
	
	array( "type" => "close" ),
	
		
	/*close array*/
	
	array(
		"type" => "close"
	));
	
	larch_add_options($larch_general_options);
	
?>