<?php
	
	/**
	 * Load the patterns into arrays.
	 */
	$patterns=array();
	$patterns[0]='none';
	for($i=1; $i<=22; $i++){
		$patterns[]='pattern'.$i.'.jpg';
	}
	
	$larch_fonts_array = larch_fonts_array_builder();
	
	$larch_style_general_options= array( array(
		"name" => "Footer",
		"type" => "title",
	),
	
	array(
		"type" => "open",
		"subtitles"=>array(array("id"=>"style-footer", "name"=>"Footer"))
	),
	
	/* ------------------------------------------------------------------------*
	 * FOOTER
	 * ------------------------------------------------------------------------*/
	
	array(
		"type" => "subtitle",
		"id" => 'style-footer'
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Primary Footer</h3>'
	),
	
	array(
		"name" => "Show Primary Footer?",
		"id" => "larch_show_primary_footer",
		"type" => "checkbox",
		"std" => 'off'
	),
	
	array(
		"name" => "Background Type",
		"id" => "larch_footerbg_type",
		"type" => "select",
		"options" => array(array('id'=>'color','name'=>'Color'), array('id'=>'image','name'=>'Image'), array('id'=>'pattern','name'=>'Pattern'), array('id'=>'custom_pattern','name'=>'Custom Pattern')),
		"std" => 'color'
	),
	
	array(
		"name" => "Image",
		"id" => "larch_footerbg_image",
		"type" => "upload_from_media",
		"desc" => 'Here you can choose the image for your footer.'
	),
	
	array(
		"name" => "Color",
		"id" => "larch_footerbg_color",
		"type" => "color",
		"std" => '1a1c21'
	),
	
	array(
		"name" => "Background Opacity",
		"id" => "larch_footerbg_color_opacity",
		"type" => "opacity_slider",
		"std" => "100"
	),
	
	array(
		"name" => "Pattern",
		"id" => "larch_footerbg_pattern",
		"type" => "pattern",
		"options" => $patterns,
		"desc" => 'Here you can choose the pattern for your footer.'
	),
	
	array(
		"name" => "Custom Pattern",
		"id" => "larch_footerbg_custom_pattern",
		"type" => "upload_from_media",
		"desc" => 'Here you can choose the custom pattern for your footer. It will replace the pattern you choose above.'
	),
	
	array(
		"name" => "Borders Color",
		"id" => "larch_footerbg_borderscolor",
		"type" => "color",
		"std" => '0f1114'
	),
	
	array(
		"name" => "Padding Top",
		"id" => "larch_primary_footer_padding_top",
		"type" => "text",
		"std" => "80px"
	),
	
	array(
		"name" => "Padding Bottom",
		"id" => "larch_primary_footer_padding_bottom",
		"type" => "text",
		"std" => "80px"
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Primary Footer - Text Colors</h3>'
	),
	
	array(
		"name" => "Links Color",
		"id" => "larch_footerbg_linkscolor",
		"type" => "color",
		"std" => '4f5258'
	),
	
	array(
		"name" => "Paragraphs Color",
		"id" => "larch_footerbg_paragraphscolor",
		"type" => "color",
		"std" => '4f5258'
	),
	
	array(
		"name" => "Headings Color",
		"id" => "larch_footerbg_headingscolor",
		"type" => "color",
		"std" => '9b9fa9'
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Secondary Footer</h3>'
	),
	
	array(
		"name" => "Show Secondary Footer?",
		"id" => "larch_show_sec_footer",
		"type" => "checkbox",
		"std" => 'on'
	),
	
	array(
		"name" => "Background Type",
		"id" => "larch_sec_footerbg_type",
		"type" => "select",
		"options" => array(array('id'=>'color','name'=>'Color'), array('id'=>'image','name'=>'Image'), array('id'=>'pattern','name'=>'Pattern'), array('id'=>'custom_pattern','name'=>'Custom Pattern')),
		"std" => 'color'
	),
	
	array(
		"name" => "Image",
		"id" => "larch_sec_footerbg_image",
		"type" => "upload_from_media",
		"desc" => 'Here you can choose the image for your footer.'
	),
	
	array(
		"name" => "Color",
		"id" => "larch_sec_footerbg_color",
		"type" => "color",
		"std" => '151619'
	),
	
	array(
		"name" => "Background Opacity",
		"id" => "larch_sec_footerbg_color_opacity",
		"type" => "opacity_slider",
		"std" => "100"
	),
	
	array(
		"name" => "Pattern",
		"id" => "larch_sec_footerbg_pattern",
		"type" => "pattern",
		"options" => $patterns,
		"desc" => 'Here you can choose the pattern for your footer.'
	),
	
	array(
		"name" => "Custom Pattern",
		"id" => "larch_sec_footerbg_custom_pattern",
		"type" => "upload_from_media",
		"desc" => 'Here you can choose the custom pattern for your footer. It will replace the pattern you choose above.'
	),
	
	array(
		"name" => "Padding Top",
		"id" => "larch_secondary_footer_padding_top",
		"type" => "text",
		"std" => "20px"
	),
	
	array(
		"name" => "Padding Bottom",
		"id" => "larch_secondary_footer_padding_bottom",
		"type" => "text",
		"std" => "20px"
	),
	
	array(
		"name" => "Social Icons Size",
		"id" => "larch_sec_footer_social_icons_size",
		"type" => "text",
		"std" => "14px"
	),
	
	array(
		"name" => "Social Icons Color",
		"id" => "larch_sec_footer_social_icons_color",
		"type" => "color",
		"std" => "4b4e54"
	),
	
	array(
		"name" => "Social Icons Hover Color",
		"id" => "larch_sec_footer_social_icons_hover_color",
		"type" => "color",
		"std" => "d7dbe0"
	),
	
	array(
		"type" => "close"
	),

	
	/*close array*/
	
	array(
		"type" => "close"
	));
	
	larch_add_style_options($larch_style_general_options);
	
?>