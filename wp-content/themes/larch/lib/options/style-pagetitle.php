<?php
	
	$revsliders = larch_get_created_camera_sliders();

	/**
	 * Load the patterns into arrays.
	 */
	$patterns=array();
	$patterns[0]='none';
	for($i=1; $i<=22; $i++){
		$patterns[]='pattern'.$i.'.jpg';
	}
	
	$larch_fonts_array = larch_fonts_array_builder();
	
	$larch_style_general_options= array( array(
		"name" => "Page Title",
		"type" => "title",
	),
	
	array(
		"type" => "open",
		"subtitles"=>array(array("id"=>"pagetitle", "name"=>"Page Title"), array("id"=>"singlepost_pagetitle", "name"=>"Single Post Page Title"), array("id"=>"shop_pagetitle", "name"=>"WooCommerce Page Title"))
	),
	
	
	/* ------------------------------------------------------------------------*
	 * Page Title
	 * ------------------------------------------------------------------------*/
	
	array(
		"type" => "subtitle",
		"id" => 'pagetitle'
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Page Title Background</h3>'
	),
	
	array(
		"name" => "Background Type",
		"id" => "larch_header_type",
		"type" => "select",
		"options" => array(array('id'=>'without', 'name'=>'Without Page Title'), array('id'=>'color','name'=>'Color'), array('id'=>'image','name'=>'Image'), array('id'=>'pattern','name'=>'Pattern'), array('id'=>'custom_pattern','name'=>'Custom Pattern'), array('id' => 'banner', 'name' => 'Banner Slider')),
		"std" => 'image'
	),
	
	array(
		"name" => "Image",
		"id" => "larch_header_image",
		"type" => "upload_from_media",
		"desc" => 'Here you can choose the image for your header.',
		"std" => "http://demos.upperthemes.com/larch/wp-content/uploads/2018/10/page-title.jpg"
	),
	
	array(
		"name" => "Background Position",
		"id" => "larch_pagetitle_background_position",
		"type" => "select",
		"options" => array(array("id"=>"top","name"=>"Name"), array("id"=>"center","name"=>"Center"),array("id"=>"bottom","name"=>"Bottom")),
		"std" => "center",
		"desc" => "Only available if Parallax is not active.",
	),
	
	array(
		"name" => "Parallax ?",
		"id" => "larch_pagetitle_image_parallax",
		"type" => "checkbox",
		"std" => "off",
	),
	
	array(
		"name" => "Overlay ?",
		"id" => "larch_pagetitle_image_overlay",
		"type" => "checkbox",
		"std" => "on"
	),
	
	array(
		"name" => "Overlay Type",
		"id" => "larch_pagetitle_overlay_type",
		"type" => "select",
		"options" => array(array('id'=>'color', 'name'=>'Color'), array('id'=>'pattern','name'=>'Pattern')),
		"std" => 'color',
	),
	
	array(
		"name" => "Overlay Color",
		"id" => "larch_pagetitle_overlay_color",
		"type" => "color",
		"std" => "000000"
	),
	
	array(
		"name" => "Overlay Pattern",
		"id" => "larch_pagetitle_overlay_pattern",
		"type" => "pattern",
		"options" => $patterns,
	),
	
	array(
		"name" => "Overlay Opacity",
		"id" => "larch_pagetitle_overlay_opacity",
		"type" => "opacity_slider",
		"std" => "85"
	),
	
	array(
		"name" => "Color",
		"id" => "larch_header_color",
		"type" => "color",
		"std" => "f2f2f2"
	),
	
	array(
		"name" => "Background Opacity",
		"id" => "larch_header_opacity",
		"type" => "opacity_slider",
		"std" => "100"
	),
	
	array(
		"name" => "Pattern",
		"id" => "larch_header_pattern",
		"type" => "pattern",
		"options" => $patterns,
		"desc" => 'Here you can choose the pattern for your header.',
		"std" => "pattern5.jpg"
	),
	
	array(
		"name" => "Custom Pattern",
		"id" => "larch_header_custom_pattern",
		"type" => "upload_from_media",
		"desc" => 'Here you can choose the custom pattern for your header. It will replace the pattern you choose above.'
	),
	
	array(
		"name" => "Banner Slider",
		"id" => "larch_banner_slider",
		"type" => "select",
		"options" => $revsliders
	),
	
	array(
		"name" => "Page Title Padding",
		"id"=> "larch_page_title_padding",
		"type" => "text",
		"std" => "150px",
		"desc" => 'Value for the padding must be set in pixels'
	),
	
	array(
		"name" => "Elements Alignment",
		"id"=> "larch_header_text_alignment",
		"type" => "select",
		"std" => "center",
		"options" => array(array("id"=>"left", "name"=>"Left"), array("id"=>"center", "name"=>"Center"), array("id"=>"right", "name"=>"Right"), array("id"=>"titlesleftcrumbsright", "name"=>"Left: Titles, Right: Breadcrumbs"), array("id"=>"titlesrightcrumbsleft", "name"=>"Left: Breadcrumbs, Right: Titles"))
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Primary Title</h3>'
	),
	
	array(
		"name" => "Display Title",
		"id" => "larch_hide_pagetitle",
		"type" => "checkbox",
		"std" => "on"
	),
	
	array(
		"name" => "Primary Title Font",
		"id" => "larch_header_text_font",
		"type" => "select",
		"options" => $larch_fonts_array,
		"desc" => 'You can select one of the fonts that the theme goes with or you can add google fonts (Style Options > Fonts).',
		"std" => 'Helvetica Neue'
	),
	
	array(
		"name" => "Primary Title Color",
		"id" => "larch_header_text_color",
		"type" => "color",
		"std" => "f2f2f2"
	),
	
	array(
		"name" => "Primary Title Size",
		"id" => "larch_header_text_size",
		"type" => "slider",
		"std" => "50px"
	),
	
	array(
		"name" => "Primary Title Margin",
		"id" => "larch_header_text_margin_top",
		"type" => "text",
		"std" => "0px"
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Secondary Title</h3>'
	),
	
	array(
		"name" => "Display Title",
		"id" => "larch_hide_sec_pagetitle",
		"type" => "checkbox",
		"std" => "off"
	),
	
	array(
		"name" => "Secondary Title Font",
		"id" => "larch_secondary_title_font",
		"type" => "select",
		"options" => $larch_fonts_array,
		"desc" => 'You can select one of the fonts that the theme goes with or you can add google fonts (Style Options > Fonts).',
		"std" => 'Helvetica Neue'
	),
	
	array(
		"name" => "Secondary Title Text Color",
		"id" => "larch_secondary_title_text_color",
		"type" => "color",
		"std" => "666666"
	),
	
	array(
		"name" => "Secondary Title Text Size",
		"id" => "larch_secondary_title_text_size",
		"type" => "slider",
		"std" => "20px"
	),
	
	array(
		"name" => "Secondary Title Margin",
		"id" => "larch_header_sec_text_margin_top",
		"type" => "text",
		"std" => "15px"
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Breadcrumbs</h3>'
	),
	
	array(
		"name" => "Breadcrumbs",
		"id" => "larch_breadcrumbs",
		"type" => "checkbox",
		"std" => "off"
	),
	
	array(
		"name" => "Breadcrumbs Font",
		"id" => "larch_breadcrumbs_font",
		"type" => "select",
		"options" => $larch_fonts_array,
		"desc" => 'You can select one of the fonts that the theme goes with or you can add google fonts (Style Options > Fonts).',
		"std" => 'Helvetica Neue'
	),
	
	array(
		"name" => "Breadcrumbs Text Color",
		"id" => "larch_breadcrumbs_color",
		"type" => "color",
		"std" => "ffffff"
	),
	
	array(
		"name" => "Breadcrumbs Text Size",
		"id" => "larch_breadcrumbs_size",
		"type" => "slider",
		"std" => "12px"
	),
	
	array(
		"name" => "Breadcrumbs Margin Top",
		"id" => "larch_breadcrumbs_text_margin_top",
		"type" => "text",
		"std" => "0px"
	),
	
	array(
		"type" => "close"
	),
	
	
	/* ------------------------------------------------------------------------*
	 * SINGLE POST Page Title - NEW LARCH
	 * ------------------------------------------------------------------------*/
	
	array(
		"type" => "subtitle",
		"id" => 'singlepost_pagetitle'
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Page Title Background</h3>'
	),
	
	array(
		"name" => "Background Type",
		"id" => "larch_header_type_single_post",
		"type" => "select",
		"options" => array(array('id'=>'without', 'name'=>'Without Page Title'), array('id'=>'featured_image','name'=>'Featured Image'), array('id'=>'color','name'=>'Color'), array('id'=>'image','name'=>'Image'), array('id'=>'pattern','name'=>'Pattern'), array('id'=>'custom_pattern','name'=>'Custom Pattern'), array('id' => 'banner', 'name' => 'Banner Slider')),
		"std" => 'image'
	),
	
	array(
		"name" => "Image",
		"id" => "larch_header_image_single_post",
		"type" => "upload_from_media",
		"desc" => 'Here you can choose the image for your header.',
		"std" => "http://demos.upperthemes.com/larch/wp-content/uploads/2018/10/page-title.jpg"
	),
	
	array(
		"name" => "Background Position",
		"id" => "larch_pagetitle_background_position_single_post",
		"type" => "select",
		"options" => array(array("id"=>"top","name"=>"Name"), array("id"=>"center","name"=>"Center"),array("id"=>"bottom","name"=>"Bottom")),
		"std" => "center",
		"desc" => "Only available if Parallax is not active.",
	),
	
	array(
		"name" => "Parallax ?",
		"id" => "larch_pagetitle_image_parallax_single_post",
		"type" => "checkbox",
		"std" => "off",
	),
	
	array(
		"name" => "Overlay ?",
		"id" => "larch_pagetitle_image_overlay_single_post",
		"type" => "checkbox",
		"std" => "on"
	),
	
	array(
		"name" => "Overlay Type",
		"id" => "larch_pagetitle_overlay_type_single_post",
		"type" => "select",
		"options" => array(array('id'=>'color', 'name'=>'Color'), array('id'=>'pattern','name'=>'Pattern')),
		"std" => 'color',
	),
	
	array(
		"name" => "Overlay Color",
		"id" => "larch_pagetitle_overlay_color_single_post",
		"type" => "color",
		"std" => "000000"
	),
	
	array(
		"name" => "Overlay Pattern",
		"id" => "larch_pagetitle_overlay_pattern_single_post",
		"type" => "pattern",
		"options" => $patterns,
	),
	
	array(
		"name" => "Overlay Opacity",
		"id" => "larch_pagetitle_overlay_opacity_single_post",
		"type" => "opacity_slider",
		"std" => "85"
	),
	
	array(
		"name" => "Color",
		"id" => "larch_header_color_single_post",
		"type" => "color",
		"std" => "000000"
	),
	
	array(
		"name" => "Background Opacity",
		"id" => "larch_header_opacity_single_post",
		"type" => "opacity_slider",
		"std" => "85"
	),
	
	array(
		"name" => "Pattern",
		"id" => "larch_header_pattern_single_post",
		"type" => "pattern",
		"options" => $patterns,
		"desc" => 'Here you can choose the pattern for your header.',
		"std" => "pattern5.jpg"
	),
	
	array(
		"name" => "Custom Pattern",
		"id" => "larch_header_custom_pattern_single_post",
		"type" => "upload_from_media",
		"desc" => 'Here you can choose the custom pattern for your header. It will replace the pattern you choose above.'
	),
	
	array(
		"name" => "Banner Slider",
		"id" => "larch_banner_slider_single_post",
		"type" => "select",
		"options" => $revsliders
	),
	
	array(
		"name" => "Page Title Padding",
		"id"=> "larch_page_title_padding_single_post",
		"type" => "text",
		"std" => "150px",
		"desc" => 'Value for the padding must be set in pixels'
	),
	
	array(
		"name" => "Elements Alignment",
		"id"=> "larch_header_text_alignment_single_post",
		"type" => "select",
		"std" => "center",
		"options" => array(array("id"=>"left", "name"=>"Left"), array("id"=>"center", "name"=>"Center"), array("id"=>"right", "name"=>"Right"), array("id"=>"titlesleftcrumbsright", "name"=>"Left: Titles, Right: Breadcrumbs"), array("id"=>"titlesrightcrumbsleft", "name"=>"Left: Breadcrumbs, Right: Titles"))
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Primary Title</h3>'
	),
	
	array(
		"name" => "Display Title",
		"id" => "larch_hide_pagetitle_single_post",
		"type" => "checkbox",
		"std" => "on"
	),
	
	array(
		"name" => "Primary Title Font",
		"id" => "larch_header_text_font_single_post",
		"type" => "select",
		"options" => $larch_fonts_array,
		"desc" => 'You can select one of the fonts that the theme goes with or you can add google fonts (Style Options > Fonts).',
		"std" => 'Helvetica Neue'
	),
	
	array(
		"name" => "Primary Title Color",
		"id" => "larch_header_text_color_single_post",
		"type" => "color",
		"std" => "f2f2f2"
	),
	
	array(
		"name" => "Primary Title Size",
		"id" => "larch_header_text_size_single_post",
		"type" => "slider",
		"std" => "50px"
	),
	
	array(
		"name" => "Primary Title Margin",
		"id" => "larch_header_text_margin_top_single_post",
		"type" => "text",
		"std" => "0px"
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Secondary Title</h3>'
	),
	
	array(
		"name" => "Display Title",
		"id" => "larch_hide_sec_pagetitle_single_post",
		"type" => "checkbox",
		"std" => "off"
	),
	
	array(
		"name" => "Secondary Title Font",
		"id" => "larch_secondary_title_font_single_post",
		"type" => "select",
		"options" => $larch_fonts_array,
		"desc" => 'You can select one of the fonts that the theme goes with or you can add google fonts (Style Options > Fonts).',
		"std" => 'Helvetica Neue'
	),
	
	array(
		"name" => "Secondary Title Text Color",
		"id" => "larch_secondary_title_text_color_single_post",
		"type" => "color",
		"std" => "cccccc"
	),
	
	array(
		"name" => "Secondary Title Text Size",
		"id" => "larch_secondary_title_text_size_single_post",
		"type" => "slider",
		"std" => "17px"
	),
	
	array(
		"name" => "Secondary Title Margin",
		"id" => "larch_header_sec_text_margin_top_single_post",
		"type" => "text",
		"std" => "15px"
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Breadcrumbs</h3>'
	),
	
	array(
		"name" => "Breadcrumbs",
		"id" => "larch_breadcrumbs_single_post",
		"type" => "checkbox",
		"std" => "off"
	),
	
	array(
		"name" => "Breadcrumbs Font",
		"id" => "larch_breadcrumbs_font_single_post",
		"type" => "select",
		"options" => $larch_fonts_array,
		"desc" => 'You can select one of the fonts that the theme goes with or you can add google fonts (Style Options > Fonts).',
		"std" => 'Helvetica Neue'
	),
	
	array(
		"name" => "Breadcrumbs Text Color",
		"id" => "larch_breadcrumbs_color_single_post",
		"type" => "color",
		"std" => "666"
	),
	
	array(
		"name" => "Breadcrumbs Text Size",
		"id" => "larch_breadcrumbs_size_single_post",
		"type" => "slider",
		"std" => "16px"
	),
	
	array(
		"name" => "Breadcrumbs Margin Top",
		"id" => "larch_breadcrumbs_text_margin_top_single_post",
		"type" => "text",
		"std" => "15px"
	),
	
	array(
		"type" => "close"
	),
	
	
	/* ------------------------------------------------------------------------*
	 * WOO COMMERCE Page Title
	 * ------------------------------------------------------------------------*/
	
	array(
		"type" => "subtitle",
		"id" => 'shop_pagetitle'
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>WooCommerce Page Title Background</h3>'
	),
	
	array(
		"name" => "Background Type",
		"id" => "larch_header_type_shop",
		"type" => "select",
		"options" => array(array('id'=>'without', 'name'=>'Without Page Title'), array('id'=>'color','name'=>'Color'), array('id'=>'image','name'=>'Image'), array('id'=>'pattern','name'=>'Pattern'), array('id'=>'custom_pattern','name'=>'Custom Pattern'), array('id' => 'banner', 'name' => 'Banner Slider')),
		"std" => 'image'
	),
	
	array(
		"name" => "Image",
		"id" => "larch_header_image_shop",
		"type" => "upload_from_media",
		"desc" => 'Here you can choose the image for your header.',
		"std" => "http://demos.upperthemes.com/larch/wp-content/uploads/2018/10/page-title.jpg"
	),
	
	array(
		"name" => "Background Position",
		"id" => "larch_header_background_shop_position",
		"type" => "select",
		"options" => array(array("id"=>"top","name"=>"Name"), array("id"=>"center","name"=>"Center"),array("id"=>"bottom","name"=>"Bottom")),
		"std" => "center",
		"desc" => "Only available if Parallax is not active.",
	),
	
	array(
		"name" => "Parallax ?",
		"id" => "larch_pagetitle_image_parallax_shop",
		"type" => "checkbox",
		"std" => "off",
	),
	
	array(
		"name" => "Overlay ?",
		"id" => "larch_pagetitle_image_overlay_shop",
		"type" => "checkbox",
		"std" => "on"
	),
	
	array(
		"name" => "Overlay Type",
		"id" => "larch_pagetitle_overlay_type_shop",
		"type" => "select",
		"options" => array(array('id'=>'color', 'name'=>'Color'), array('id'=>'pattern','name'=>'Pattern')),
		"std" => 'color',
	),
	
	array(
		"name" => "Overlay Color",
		"id" => "larch_pagetitle_overlay_color_shop",
		"type" => "color",
		"std" => "000000"
	),
	
	array(
		"name" => "Overlay Pattern",
		"id" => "larch_pagetitle_overlay_pattern_shop",
		"type" => "pattern",
		"options" => $patterns,
	),
	
	array(
		"name" => "Overlay Opacity",
		"id" => "larch_pagetitle_overlay_opacity_shop",
		"type" => "opacity_slider",
		"std" => "85"
	),
	
	array(
		"name" => "Color",
		"id" => "larch_header_color_shop",
		"type" => "color",
		"std" => "f5f5f5"
	),
	
	array(
		"name" => "Background Opacity",
		"id" => "larch_header_opacity_shop",
		"type" => "opacity_slider",
		"std" => "100"
	),
	
	array(
		"name" => "Pattern",
		"id" => "larch_header_pattern_shop",
		"type" => "pattern",
		"options" => $patterns,
		"desc" => 'Here you can choose the pattern for your header.'
	),
	
	array(
		"name" => "Custom Pattern",
		"id" => "larch_header_custom_pattern_shop",
		"type" => "upload_from_media",
		"desc" => 'Here you can choose the custom pattern for your header. It will replace the pattern you choose above.'
	),
	
	array(
		"name" => "Banner Slider",
		"id" => "larch_banner_slider_shop",
		"type" => "select",
		"options" => $revsliders
	),
	
	array(
		"name" => "Page Title Padding",
		"id"=> "larch_page_title_padding_shop",
		"type" => "text",
		"std" => "150px",
		"desc" => 'Value for the padding must be set in pixels'
	),
	
	array(
		"name" => "Elements Alignment",
		"id"=> "larch_header_text_alignment_shop",
		"type" => "select",
		"std" => "center",
		"options" => array(array("id"=>"left", "name"=>"Left"), array("id"=>"center", "name"=>"Center"), array("id"=>"right", "name"=>"Right"), array("id"=>"titlesleftcrumbsright", "name"=>"Left: Titles, Right: Breadcrumbs"), array("id"=>"titlesrightcrumbsleft", "name"=>"Left: Breadcrumbs, Right: Titles"))
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Primary Title</h3>'
	),
	
	array(
		"name" => "Display Title",
		"id" => "larch_hide_pagetitle_shop",
		"type" => "checkbox",
		"std" => "on"
	),
	
	array(
		"name" => "Primary Title Font",
		"id" => "larch_header_text_font_shop",
		"type" => "select",
		"options" => $larch_fonts_array,
		"desc" => 'You can select one of the fonts that the theme goes with or you can add google fonts (Style Options > Fonts).',
		"std" => 'Helvetica Neue'
	),
	
	array(
		"name" => "Primary Title Color",
		"id" => "larch_header_text_color_shop",
		"type" => "color",
		"std" => "f2f2f2"
	),
	
	array(
		"name" => "Primary Title Size",
		"id" => "larch_header_text_size_shop",
		"type" => "slider",
		"std" => "50px"
	),
	
	array(
		"name" => "Primary Title Margin",
		"id" => "larch_header_text_margin_top_shop",
		"type" => "text",
		"std" => "0px"
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Secondary Title</h3>'
	),
	
	array(
		"name" => "Display Title",
		"id" => "larch_hide_sec_pagetitle_shop",
		"type" => "checkbox",
		"std" => "off"
	),
	
	array(
		"name" => "Secondary Title Font",
		"id" => "larch_secondary_title_font_shop",
		"type" => "select",
		"options" => $larch_fonts_array,
		"desc" => 'You can select one of the fonts that the theme goes with or you can add google fonts (Style Options > Fonts).',
		"std" => 'Helvetica Neue'
	),
	
	array(
		"name" => "Secondary Title Text Color",
		"id" => "larch_secondary_title_text_color_shop",
		"type" => "color",
		"std" => "A0A0A2"
	),
	
	array(
		"name" => "Secondary Title Text Size",
		"id" => "larch_secondary_title_text_size_shop",
		"type" => "slider",
		"std" => "17px"
	),
	
	array(
		"name" => "Secondary Title Margin",
		"id" => "larch_header_sec_text_margin_top_shop",
		"type" => "text",
		"std" => "25px"
	),
	
	array(
		"type" => "documentation",
		"text" => '<h3>Breadcrumbs</h3>'
	),
	
	array(
		"name" => "Breadcrumbs",
		"id" => "larch_breadcrumbs_shop",
		"type" => "checkbox",
		"std" => "off"
	),
	
	array(
		"name" => "Breadcrumbs Font",
		"id" => "larch_breadcrumbs_font_shop",
		"type" => "select",
		"options" => $larch_fonts_array,
		"desc" => 'You can select one of the fonts that the theme goes with or you can add google fonts (Style Options > Fonts).',
		"std" => 'Helvetica Neue'
	),
	
	array(
		"name" => "Breadcrumbs Text Color",
		"id" => "larch_breadcrumbs_color_shop",
		"type" => "color",
		"std" => "ffffff"
	),
	
	array(
		"name" => "Breadcrumbs Text Size",
		"id" => "larch_breadcrumbs_size_shop",
		"type" => "slider",
		"std" => "12px"
	),
	
	array(
		"name" => "Breadcrumbs Margin Top",
		"id" => "larch_breadcrumbs_text_margin_top_shop",
		"type" => "text",
		"std" => "30px"
	),
	
	array(
		"type" => "close"
	),
		
	/*close array*/
	
	array(
		"type" => "close"
	));
	
	larch_add_style_options($larch_style_general_options);
	
?>