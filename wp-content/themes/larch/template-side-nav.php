<?php
/**
 * Template Name: Page with Side Navigation
 * @package WordPress
 * @subpackage Larch
**/

get_header(); larch_print_menu();
wp_enqueue_script('upper-template-side-nav',get_template_directory_uri().'/js/utils/upper-template-page-side-nav.js', array('jquery'), '1', true);
$larch_thisPostID = get_the_ID(); $larch_color_code = get_option("larch_style_color");
?>
	
	<div class="master_container" style="width: 100%;float: left;background-color: white;">
		<section class="page_content" id="section-<?php echo esc_attr(get_the_ID()); ?>">
			<div class="container">
				<?php
					if ((function_exists('vc_is_inline') && vc_is_inline()) || is_preview()){
						wp_reset_postdata();
						the_content();
					} else {
						$content_post = get_post($larch_thisPostID);
						$content = $content_post->post_content;
						$upper_theme_main_color = "#".get_option('larch_style_color');
						$content = str_replace( '__USE_THEME_MAIN_COLOR__', $upper_theme_main_color, $content );
						
						// it's necessary to add these parameters to the sections otherwise all hell breaks loose x)
						$content = str_replace( '[vc_section ', '[vc_section full_width="stretch_row" full_height="yes" content_placement="middle" ', $content );
						
						larch_content_shortcoder($content);
						$content = apply_filters('the_content', $content);
						if (function_exists('wpb_js_remove_wpautop') == true)
							echo wpb_js_remove_wpautop($content);
						else echo wp_kses_post( shortcode_unautop( $content )); 
						$shortcodes_custom_css = get_post_meta( $larch_thisPostID, '_wpb_shortcodes_custom_css', true );
						if ( isset($shortcodes_custom_css) && ! empty( $shortcodes_custom_css ) ) {
							larch_set_custom_inline_css($shortcodes_custom_css);
						}
					}
				?>
			</div>
		</section>
	</div>
	
<?php get_footer(); ?>