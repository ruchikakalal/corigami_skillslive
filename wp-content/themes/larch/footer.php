<?php
/**
 * @package WordPress
 * @subpackage Larch
 */
?>
	</div>
	<div id="big_footer" class="<?php if (get_option("larch_footer_full_width") == "on") echo "footer-full-width"; if (get_option("larch_footer_reveal_footer") == "on") echo "reveal-footer"; ?>">

		<?php
		if (get_option("larch_show_primary_footer") == "on"){
			?>
			<div id="primary_footer">
			    	<?php
			    	/* Show Newsletter in Footer */
					if (get_option("larch_newsletter_enabled") == "on"){
						$code = get_option("larch_mailchimp_code");
						if (!empty($code)){
						    $output = '<div class="footer_newsletter"><div class="mail-box"><div class="mail-news container"><div class="news-l"><div class="opacity-icon"><i class="fa fa-paper-plane-o"></i></div><div class="banner"><h3>'.sprintf(esc_html("%s", "larch"), get_option("larch_newsletter_text")).'</h3><p>'.sprintf(esc_html("%s", "larch"), get_option("larch_newsletter_stext")).'</p></div><div class="form">';
						    if (function_exists('icl_t')){
							    $output = '<div class=""><div class="mail-box"><div class="mail-news"><div class="news-l"><div class="banner"><h3>'.wp_kses_post(icl_t( 'larch', 'Subscribe our <span>Newsletter</span>', get_option('larch_newsletter_text'))).'</h3><p>'.wp_kses_post(icl_t( 'larch', 'Subscribe to our newsletter to receive news, cool free stuff updates and new released products (no spam!)', get_option('larch_newsletter_stext'))).'</p></div><div class="form">';
						    }
							$output .= stripslashes($code);
							$output .= '</div></div></div></div></div>';
						} else {
							$output = '<div class="">'.esc_html__('You need to fill the inputs on the Appearance > Larch Options > Newsletter panel in order to work.','larch').'</div>';
						}			
						$output = wp_kses_no_null( $output, array( 'slash_zero' => 'keep' ) );
						$output = wp_kses_normalize_entities($output);
						echo wp_kses_hook($output, 'post', array()); // WP changed the order of these funcs and added args to wp_kses_hook		
					
					}?>
			    	<div class="<?php if (get_option("larch_footer_full_width") == "off") echo "container"; ?> no-fcontainer">
			    		
	    		<?php
	    		
					if(get_option("larch_footer_number_cols") == "one"){ ?>
						<div class="footer_sidebar col-xs-12 col-md-12"><?php larch_print_sidebar('footer-one-column'); ?></div>
					<?php }
					if(get_option("larch_footer_number_cols") == "two"){ ?>
						<div class="footer_sidebar col-xs-12 col-md-6"><?php larch_print_sidebar('footer-two-column-left'); ?></div>
						<div class="footer_sidebar col-xs-12 col-md-6"><?php larch_print_sidebar('footer-two-column-right'); ?></div>
					<?php }
					if(get_option("larch_footer_number_cols") == "three"){
						if(get_option("larch_footer_columns_order") == "one_three"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php larch_print_sidebar('footer-three-column-left'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php larch_print_sidebar('footer-three-column-center'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php larch_print_sidebar('footer-three-column-right'); ?></div>
						<?php }
						if(get_option("larch_footer_columns_order") == "one_two_three"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php larch_print_sidebar('footer-three-column-left-1_3'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-8"><?php larch_print_sidebar('footer-three-column-right-2_3'); ?></div>
						<?php }
						if(get_option("larch_footer_columns_order") == "two_one_three"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-8"><?php larch_print_sidebar('footer-three-column-left-2_3'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php larch_print_sidebar('footer-three-column-right-1_3'); ?></div>
						<?php }
					}
					if(get_option("larch_footer_number_cols") == "four"){
						if(get_option("larch_footer_columns_order_four") == "one_four"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php larch_print_sidebar('footer-four-column-left'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php larch_print_sidebar('footer-four-column-center-left'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php larch_print_sidebar('footer-four-column-center-right'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php larch_print_sidebar('footer-four-column-right'); ?></div>
						<?php }
						if(get_option("larch_footer_columns_order_four") == "two_one_two_four"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php larch_print_sidebar('footer-four-column-left-1_2_4'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-6"><?php larch_print_sidebar('footer-four-column-center-2_2_4'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-3"><?php larch_print_sidebar('footer-four-column-right-1_2_4'); ?></div>
						<?php }
						if(get_option("larch_footer_columns_order_four") == "three_one_four"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-8"><?php larch_print_sidebar('footer-four-column-left-3_4'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php larch_print_sidebar('footer-four-column-right-1_4'); ?></div>
						<?php }
						if(get_option("larch_footer_columns_order_four") == "one_three_four"){ ?>
							<div class="footer_sidebar col-xs-12 col-md-4"><?php larch_print_sidebar('footer-four-column-left-1_4'); ?></div>
							<div class="footer_sidebar col-xs-12 col-md-8"><?php larch_print_sidebar('footer-four-column-right-3_4'); ?></div>
						<?php }
					}
				?>
				</div>
		    </div>
			<?php
				
			if ( get_option('larch_display_instagram_footer') == 'on' && defined('LARCH_PLG_ACTIVE') === true ){
				?>
				<div id="footer-instagram">
					<?php
					if (get_option('larch_instagram_title') != "" || get_option('larch_instagram_title')!=" ") echo "<h4>" . wp_kses_post( get_option('larch_instagram_title') ) . "</h4>";
			
					if (get_option('larch_instagram_username') != '') {
			
						$media_array = upper_scrape_instagram( get_option('larch_instagram_username') );
			
						if ( is_wp_error( $media_array ) ) {
							echo wp_kses_post( $media_array->get_error_message() );
						} else {
			
							// filter for images only?
							if ( $images_only = apply_filters( 'upper_insta_images_only', FALSE ) ) {
								$media_array = array_filter( $media_array, array( $this, 'upper_images_only' ) );
							}
			
							// slice list down to required limit
							$media_array = array_slice( $media_array, 0, intval(get_option('larch_instagram_limit')) );
			
							// filters for custom classes
							$ulclass = apply_filters( 'upper_insta_list_class', 'instagram-pics' );
							$liclass = apply_filters( 'upper_insta_item_class', '' );
							$aclass = apply_filters( 'upper_insta_a_class', '' );
							$imgclass = apply_filters( 'upper_insta_img_class', '' );
							if (get_option('larch_enable_instagram_grayscale') == "on") $imgclass .= ' larch_grayscale ';
			
							?><ul class="<?php echo esc_attr( $ulclass ); ?>"><?php
							foreach ( $media_array as $item ) {
								echo '<li style="width:'. (100/intval(get_option('larch_instagram_limit'))) .'%;" class="'. esc_attr( $liclass ) .'"><a href="'. esc_url( $item['link'] ) .'" target="'. esc_attr( get_option('larch_instagram_target') ) .'"  class="'. esc_attr( $aclass ) .'"><img src="'. esc_url( $item['thumbnail'] ) .'"  alt="'. esc_attr( $item['description'] ) .'" title="'. esc_attr( $item['description'] ).'"  class="'. esc_attr( $imgclass ) .'"/></a></li>';
							}
							?></ul><?php
						}
					}
					$linkclass = apply_filters( 'upper_insta_link_class', 'clear' );
					if ( get_option('larch_instagram_link') != '' ) {
						?><p class="<?php echo esc_attr( $linkclass ); ?>"><a href="<?php echo trailingslashit( '//instagram.com/' . esc_attr( trim( get_option('larch_instagram_username') ) ) ); ?>" rel="me" target="<?php echo esc_attr( get_option('larch_instagram_target') ); ?>"><?php echo esc_html( get_option('larch_instagram_link') ); ?></a></p><?php
					}
					?>
				</div>
				<?php
		    }
		}
	?>
    
    <?php
	    if (get_option("larch_show_sec_footer") == "on"){
		    $larch_is_only_custom_text = (get_option("larch_footer_display_logo") != 'on' && get_option("larch_footer_display_social_icons") != "on") ? true : false;
		    ?>
		    <div id="secondary_footer">
				<div class="container <?php if ( $larch_is_only_custom_text ) echo "only_custom_text"; ?>">
					
					<?php
						/* FOOTER LOGOTYPE */
						if (get_option("larch_footer_display_logo") == 'on'){
						?>
						<a class="footer_logo align-<?php echo esc_attr(get_option("larch_footer_logo_alignment")); ?>" href="<?php echo esc_url(home_url("/")); ?>" tabindex="-1">
				        	<?php
				    			$alone = true;
			    				if (get_option("larch_footer_logo_retina_image_url") != ""){
				    				$alone = false;
			    				}
		    					?>
		    					<img class="footer_logo_normal <?php if (!$alone) echo "notalone"; ?>" style="position: relative;" src="<?php echo esc_url(get_option("larch_footer_logo_image_url")); ?>" alt="<?php esc_attr_e("", "larch"); ?>" title="<?php esc_attr_e("", "larch"); ?>">
			    					
			    				<?php 
			    				if (get_option("larch_footer_logo_retina_image_url") != ""){
			    				?>
				    				<img class="footer_logo_retina" style="display:none; position: relative;" src="<?php echo esc_url(get_option("larch_footer_logo_retina_image_url")); ?>" alt="<?php esc_attr_e("", "larch"); ?>" title="<?php esc_attr_e("", "larch"); ?>">
			    				<?php
		    					}
			    			?>
				        </a>
						<?php
						}
						
						
						
						/* FOOTER SOCIAL ICONS */
						if (get_option("larch_footer_display_social_icons") == "on"){
						?>
						<div class="social-icons-fa align-<?php echo esc_attr(get_option("larch_footer_social_icons_alignment")); ?>">
					        <ul>
							<?php
								/*$icons = array(array("facebook","Facebook"),array("twitter","Twitter"),array("tumblr","Tumblr"),array("stumbleupon","Stumble Upon"),array("flickr","Flickr"),array("linkedin","LinkedIn"),array("delicious","Delicious"),array("skype","Skype"),array("digg","Digg"),array("google-plus","Google+"),array("vimeo-square","Vimeo"),array("blogger","Blogger"),array("deviantart","DeviantArt"),array("behance","Behance"),array("instagram","Instagram"),array("wordpress","WordPress"),array("youtube","Youtube"),array("reddit","Reddit"),array("rss","RSS"),array("soundcloud","SoundCloud"),array("pinterest","Pinterest"),array("dribbble","Dribbble"));

								foreach ($icons as $i){
									if (is_string(get_option("larch_icon-".$i[0])) && get_option("larch_icon-".$i[0]) != ""){
									?>
									<li>
										<a href="<?php echo esc_url(get_option("larch_icon-".$i[0])); ?>" target="_blank" class="<?php echo esc_attr(strtolower($i[0])); ?>" title="<?php echo esc_attr($i[1]); ?>"><i class="fa fa-<?php echo esc_attr(strtolower($i[0])); ?>"></i></a>
									</li>
									<?php
									}
								}
							*/?>
							<ul>

								<li>
								<a href="mailto:evan@skillslive.com.au" target="_blank" class="mail" title="mail"><i class="fa fa-envelope-o"></i></a>
								</li>
								<li>
								<a href="https://www.linkedin.com/company/myskillslive" target="_blank" class="linkedin" title="linkedin"><i class="fa fa-linkedin"></i></a>
								</li>
								<li>
								<a href="https://www.youtube.com/channel/UCzPwBN40F7OySSsKlUkfDtQ" target="_blank" class="youtube" title="youtube"><i class="fa fa-caret-square-o-right"></i></a>
								</li>
								<li>
								<a href="https://twitter.com/SkillsliveLMS" target="_blank" class="twitter" title="Twitter"><i class="fa fa-twitter"></i></a>
								</li>

						    </ul>
						</div>
						<?php
						}
						/* FOOTER CUSTOM TEXT */
						if (get_option("larch_footer_display_custom_text") == "on"){
						?>
						<div class="footer_custom_text <?php if ( $larch_is_only_custom_text ) echo "wide"; else echo esc_attr(get_option("larch_footer_custom_text_alignment")); ?>"><?php echo do_shortcode(stripslashes(get_option("larch_footer_custom_text"))); ?></div>
						<?php
						}
						
					?>
				</div>
			</div>
		    <?php
	    }
    ?>
	

<?php

/* sets the type of pagination [scroll, autoscroll, paged, default] */
wp_reset_postdata();
$larch_reading_option = get_option('larch_blog_reading_type');

if (is_archive() || is_single() || is_search() || is_page_template('blog-template.php') || is_page_template('blog-masonry-template.php') || is_page_template('blog-masonry-grid-template.php') || is_front_page()) {

	$nposts = get_option('posts_per_page');

	$larch_more = 0;
	$larch_pag = 0;

	$orderby="";
	$category="";
	$nposts = "";
	$order = "";

	$larch_pag = $wp_query->query_vars['paged'];
	if (!is_numeric($larch_pag)) $larch_pag = 1;
	$max = 0;

	switch ($larch_reading_option){
		case "scrollauto": 

				// Add code to index pages.
				if( !is_singular() ) {	

					if (is_search()){

						$larch_reading_option = get_option('larch_blog_reading_type');
						$se = get_option("larch_enable_search_everything");

						$nposts = get_option('posts_per_page');

						$larch_pag = $wp_query->query_vars['paged'];
						if (!is_numeric($larch_pag)) $larch_pag = 1;

						if ($se == "on"){
							$args = array(
								'showposts' => get_option('posts_per_page'),
								'post_status' => 'publish',
								'paged' => $larch_pag,
								's' => esc_html($_GET['s'])
							);

						    $larch_the_query = new WP_Query( $args );

						    $args2 = array(
								'showposts' => -1,
								'post_status' => 'publish',
								'paged' => $larch_pag,
								's' => esc_html($_GET['s'])
							);

							$counter = new WP_Query($args2);

						} else {
							$args = array(
								'showposts' => get_option('posts_per_page'),
								'post_status' => 'publish',
								'paged' => $larch_pag,
								'post_type' => 'post',
								's' => esc_html($_GET['s'])
							);

						    $larch_the_query = new WP_Query( $args );

						    $args2 = array(
								'showposts' => -1,
								'post_status' => 'publish',
								'paged' => $larch_pag,
								'post_type' => 'post',
								's' => esc_html($_GET['s'])
							);

							$counter = new WP_Query($args2);
						}

						$max = ceil($counter->post_count / $nposts);
						$larch_paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;

					} else {

						$max = $wp_query->max_num_pages;
						$larch_paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;

					}
					
					$larch_inline_script = '
						jQuery(document).ready(function($){
							"use strict";
							if (jQuery("#reading_option").html() === "scrollauto" && !jQuery("body").hasClass("single")){ 
								window.larch_loadingPoint = 0;
								//monitor page scroll to fire up more posts loader
								window.clearInterval(window.larch_interval);
								window.larch_interval = setInterval("larch_monitorScrollTop()", 1000 );
							}
						});
					';
					wp_add_inline_script('larch-global', $larch_inline_script, 'after');

				} else {

				    $args = array(
	    				'showposts' => $nposts,
	    				'orderby' => $orderby,
	    				'order' => $order,
	    				'cat' => $category,
	    				'paged' => $larch_pag,
	    				'post_status' => 'publish'
	    			);

	    		    $larch_the_query = new WP_Query( $args );

		    		$max = $larch_the_query->max_num_pages;
		    		$larch_paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;

		    		$larch_inline_script = '
						jQuery(document).ready(function($){
							"use strict";
							if (jQuery("#reading_option").html() === "scrollauto" && !jQuery("body").hasClass("single")){ 
								window.larch_loadingPoint = 0;
								//monitor page scroll to fire up more posts loader
								window.clearInterval(window.larch_interval);
								window.larch_interval = setInterval("larch_monitorScrollTop()", 1000 );
							}
						});
					';
					wp_add_inline_script('larch-global', $larch_inline_script, 'after');

	    		}
			break;
		case "scroll": 

				// Add code to index pages.
				if( !is_singular() ) {	

					if (is_search()){

						$nposts = get_option('posts_per_page');

						$se = get_option("larch_enable_search_everything");

						if ($se == "on"){
							$args = array(
								'showposts' => get_option('posts_per_page'),
								'post_status' => 'publish',
								'paged' => $larch_pag,
								's' => esc_html($_GET['s'])
							);

						    $larch_the_query = new WP_Query( $args );

						    $args2 = array(
								'showposts' => -1,
								'post_status' => 'publish',
								'paged' => $larch_pag,
								's' => esc_html($_GET['s'])
							);

							$counter = new WP_Query($args2);

						} else {
							$args = array(
								'showposts' => get_option('posts_per_page'),
								'post_status' => 'publish',
								'paged' => $larch_pag,
								'post_type' => 'post',
								's' => esc_html($_GET['s'])
							);

						    $larch_the_query = new WP_Query( $args );

						    $args2 = array(
								'showposts' => -1,
								'post_status' => 'publish',
								'paged' => $larch_pag,
								'post_type' => 'post',
								's' => esc_html($_GET['s'])
							);

							$counter = new WP_Query($args2);
						}

						$max = ceil($counter->post_count / $nposts);
						$larch_pag = 1;

						$larch_pag = $wp_query->query_vars['paged'];
						if (!is_numeric($larch_pag)) $larch_pag = 1;

					} else {
						$max = $wp_query->max_num_pages;
						$larch_paged = $larch_pag;

					}


				} else {

					$orderby = "";
					$category = "";

				    $args = array(
	    				'showposts' => $nposts,
	    				'orderby' => $orderby,
	    				'order' => $order,
	    				'cat' => $category,
	    				'post_status' => 'publish'
	    			);

	    		    $larch_the_query = new WP_Query( $args );


		    		$max = $larch_the_query->max_num_pages;
		    		$larch_pag = 1;

					$larch_pag = $wp_query->query_vars['paged'];
					if (!is_numeric($larch_pag)) $larch_pag = 1;		    			    				
	    		}

			break;
	}
	?>
	<div class="larch_helper_div" id="loader-startPage"><?php echo esc_html($larch_pag); ?></div>
	<div class="larch_helper_div" id="loader-maxPages"><?php echo esc_html($max); ?></div>
	<div class="larch_helper_div" id="loader-nextLink"><?php echo next_posts($max, false); ?></div>
	<div class="larch_helper_div" id="loader-prevLink"><?php echo previous_posts($max, false); ?></div>
	<?php
} 

$larch_styleColor = "#".esc_html(get_option("larch_style_color"));
$larch_bodyLayoutType = get_option("larch_body_layout_type");
$larch_headerType = get_option("larch_header_type");
?>
</div> <!-- END OF MAIN -->
<div id="bodyLayoutType" class="larch_helper_div"><?php echo esc_html($larch_bodyLayoutType); ?></div>
<div id="headerType" class="larch_helper_div"><?php echo esc_html($larch_headerType); ?></div>
<?php 
	if (get_option("larch_body_shadow") == "on"){
		?>
			<div id="bodyShadowColor" class="larch_helper_div"><?php echo "#".esc_html(get_option("larch_body_shadow_color")); ?></div>
		<?php
	}
	$larch_headerStyleType = get_option("larch_header_style_type");
?>
<div id="templatepath" class="larch_helper_div"><?php echo esc_url(get_template_directory_uri())."/"; ?></div>
<div id="homeURL" class="larch_helper_div"><?php echo esc_url(home_url("/")); ?></div>
<div id="styleColor" class="larch_helper_div"><?php echo "#".esc_html(get_option("larch_style_color")); ?></div>	
<div id="headerStyleType" class="larch_helper_div"><?php echo esc_html($larch_headerStyleType); ?></div>
<div class="larch_helper_div" id="reading_option"><?php 
	if ($larch_reading_option == "scrollauto"){
		if (wp_is_mobile())
			$larch_reading_option = "scroll";
	}
	echo esc_html($larch_reading_option); 
?></div>
<?php
	$larch_color_code = get_option("larch_style_color");
?>
<div class="larch_helper_div" id="larch_no_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'No more posts to load.', get_option('larch_no_more_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_no_more_posts_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_load_more_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'Load More Posts', get_option('larch_load_more_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_load_more_posts_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_loading_posts_text"><?php
	if (function_exists('icl_t')){
		printf(esc_html__("%s", "larch"), icl_t( 'larch', 'Loading posts.', get_option('larch_loading_posts_text')));
	} else {
		printf(esc_html__("%s", "larch"), get_option('larch_loading_posts_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_links_color_hover"><?php echo esc_html(str_replace("__USE_THEME_MAIN_COLOR__", $larch_color_code, get_option("larch_links_color_hover"))); ?></div>
<div class="larch_helper_div" id="larch_enable_images_magnifier"><?php echo esc_html(get_option('larch_enable_images_magnifier')); ?></div>
<div class="larch_helper_div" id="larch_thumbnails_hover_option"><?php echo esc_html(get_option('larch_thumbnails_hover_option')); ?></div>
<div id="homePATH" class="larch_helper_div"><?php echo ABSPATH; ?></div>
<div class="larch_helper_div" id="larch_menu_color">#<?php echo esc_html(get_option("larch_menu_color")); ?></div>
<div class="larch_helper_div" id="larch_fixed_menu"><?php echo esc_html(get_option("larch_fixed_menu")); ?></div>
<div class="larch_helper_div" id="larch_thumbnails_effect"><?php if (get_option("larch_animate_thumbnails") == "on") echo esc_html(get_option("larch_thumbnails_effect")); else echo "none"; ?></div>
<div class="larch_helper_div loadinger">
	<img alt="<?php esc_attr_e('loading','larch'); ?>" src="<?php echo esc_url(get_template_directory_uri()). '/images/ajx_loading.gif' ?>">
</div>
<div class="larch_helper_div" id="permalink_structure"><?php echo esc_html(get_option('permalink_structure')); ?></div>
<div class="larch_helper_div" id="headerstyle3_menucolor">#<?php echo esc_html(get_option("larch_menu_color")); ?></div>
<div class="larch_helper_div" id="disable_responsive_layout"><?php echo esc_html(get_option('larch_disable_responsive')); ?></div>
<div class="larch_helper_div" id="filters-dropdown-sort"><?php esc_html_e('Sort Gallery','larch'); ?></div>
<div class="larch_helper_div" id="searcheverything"><?php echo esc_html(get_option("larch_enable_search_everything")); ?></div>
<div class="larch_helper_div" id="larch_header_shrink"><?php if (get_option('larch_fixed_menu') == 'on'){if (get_option('larch_header_after_scroll') == 'on'){if (get_option('larch_header_shrink_effect') == 'on'){echo "yes";} else echo "no";}} ?></div>
<div class="larch_helper_div" id="larch_header_after_scroll"><?php if (get_option('larch_fixed_menu') == 'on'){if (get_option('larch_header_after_scroll') == 'on'){echo "yes";} else echo "no";} ?></div>
<div class="larch_helper_div" id="larch__portfolio_grayscale_effect"><?php echo esc_html(get_option("larch_enable_portfolio_grayscale")); ?></div>
<div class="larch_helper_div" id="larch__instagram_grayscale_effect"><?php echo esc_html(get_option("larch_enable_instagram_grayscale")); ?></div>
<div class="larch_helper_div" id="larch_grayscale_effect"><?php echo esc_html(get_option("larch_enable_grayscale")); ?></div>
<div class="larch_helper_div" id="larch_enable_ajax_search"><?php echo esc_html(get_option("larch_enable_ajax_search")); ?></div>
<div class="larch_helper_div" id="larch_menu_add_border"><?php echo esc_html(get_option("larch_menu_add_border")); ?></div>
<div class="larch_helper_div" id="larch_newsletter_input_text"><?php
	if (function_exists('icl_t')){
		echo esc_html(icl_t( 'larch', 'Enter your email here', get_option('larch_newsletter_input_text')));
	} else {
		echo esc_html(get_option('larch_newsletter_input_text'));
	}
?></div>
<div class="larch_helper_div" id="larch_content_to_the_top">
	<?php 
		if (is_singular() && get_post_meta(get_the_ID(), 'larch_enable_custom_header_options_value', true)=='yes') echo esc_html(get_post_meta(get_the_ID(), 'larch_content_to_the_top_value', true));
		else echo esc_html(get_option('larch_content_to_the_top')); 
	?>
</div>
<div class="larch_helper_div" id="larch_update_section_titles"><?php echo esc_html(get_option('larch_update_section_titles')); ?></div>
<?php
	if (function_exists('icl_t')){
		?>
		<div class="larch_helper_div" id="larch_wpml_current_lang"><?php echo ICL_LANGUAGE_CODE; ?></div>
		<?php
	}
?>
<?php 
	$standardfonts = array('Arial','Arial Black','Helvetica','Helvetica Neue','Courier New','Georgia','Impact','Lucida Sans Unicode','Times New Roman', 'Trebuchet MS','Verdana','');
	$importfonts = "";
	$larch_import_fonts = larch_get_import_fonts();

	if (is_string($larch_import_fonts)) $larch_import_fonts = explode( "|", $larch_import_fonts);
	foreach ($larch_import_fonts as $font){
		if (!in_array($font,$standardfonts)){
			$font = str_replace(" ","+",str_replace("|",":",$font));
			if ($importfonts=="") $importfonts .= $font;
			else {
				if (strpos($importfonts, $font) === false)
					$importfonts .= "|{$font}";
			}
		}
	}

	if ($importfonts!="") {
		$larch_import_fonts = $importfonts;
		larch_set_import_fonts($larch_import_fonts);
		larch_google_fonts_scripts();
	}

	if (get_option("larch_enable_gotop") == "on"){
		?>
		<p id="back-top"><a href="#home"><i class="fa fa-angle-up"></i></a></p>
		<?php
	}
	
	larch_get_team_profiles_content();
	larch_get_custom_inline_css();

	if (get_option("larch_body_type") == "body_boxed"){
		?>
		</div>
		<?php
	}
	
	wp_footer(); 
?>
    	
</body>
</html>
